#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <glm/glm.hpp>
#include <memory>
#include <sstream>

#include "Camera.h"
#include "Render.h"
#include "Fonts.h"
#include "Text.h"
#include "ObjParser.h"

#define BOLD(TEXT) "\033[1m" #TEXT "\033[0m"
#define BOLD_RED(TEXT) "\033[1;31m" #TEXT "\033[0m"
#define BOLD_MAG(TEXT) "\033[1;35m" #TEXT "\033[0m"

using namespace std;

shared_ptr<Camera> camera;

void man();
void ScrollCallback(double x);
std::string vec2string(glm::vec3);

int main(int argc, char** argv)
{
    // Parse input data
    if(argc == 1){
        man();
    }

    int height=768, width=1024;

    bool any_obj_file = false;
    string file_name;
    string texture_name = "";
    string texture_ext;

    for(int i = 1; i < argc; ++i){
        std::string argument(argv[i]);
        unsigned long len = argument.length();

        if( argument == "--help"){
            man();
            return 0;
        }

        if( len > 4 ) {
            std::string ext = argument.substr(len - 4);

            if (ext == ".obj" || ext == ".OBJ") {
                file_name = argument;
                any_obj_file = true;
            }
            else if( ext == ".dss" || ext == ".DDS" || ext == ".bmp" || ext == ".BMP" )
            {
                texture_name = argument;
                texture_ext = ext;
            }
        }
        else{
            cout << BOLD_MAG(Warning:) << " Unknown argument " + argument << endl;
        }
    }

    if( not any_obj_file ){
        cout << BOLD_RED(You have to pass .obj file to this program!) << endl;
        return 1;
    }

    ObjParser objFile(file_name);
//    if( not objFile.SimpleParser() ){
//        return 2;
//    }
    if( not objFile.loadUsingAssImp() ){
        return 2;
    }



    //objFile.box();

    camera = make_shared<Camera>();

    cout << "Initiating Renderer..."<< endl;
    int n = Render::Init(height, width);
    if(n) {
        return n;
    }
    Render::scroll_callback = ScrollCallback;
    cout << "...Done" << endl;

    cout << "Sending data to graphic card ..." << endl;
    objFile.sendData2GraphicCard();
    cout << "...Done" << endl;

    cout << "Loading texture ..." << endl;
    if(texture_name != "") {
        if( not objFile.loadTexture(texture_name, texture_ext) ){
            cout << BOLD_MAG(Warning:) << " Fail to load texture form " << texture_name << "\n";
        }
    }
    cout << "...Done" << endl;

    cout << "Initiating Fonts..."<< endl;
    n = init_font();
    if(n) {
        return n;
    }
    cout << "...Done" << endl;


    auto file_name_text  = std::make_shared<Text>("File name: " + file_name, glm::vec2(10,20));
    auto color_text   = std::make_shared<Text>("Model color: " + vec2string(objFile.getModelColor()), glm::vec2(10,40));
    auto info_about_color_change = std::make_shared<Text>("", glm::vec2(Render::getWidth()/2 - 80, 20), 18, glm::vec3(0.6f, 0.0f, 0.1f));
    auto info_about_light_pos = std::make_shared<Text>("Light position: " + vec2string(Render::getLightPos()), glm::vec2(10,60), 16);

    #define MARGIN_UL 80
    auto fps_text   = std::make_shared<Text>("FPS: 60", glm::vec2(Render::getWidth()-MARGIN_UL, 20));

    double lastTimeForFps = Render::GetTime();
    double lastTime = Render::GetTime();
    int nbFrames = 0;

    Render::hackForMouseCenter();
    camera->reset();

    bool R_key_down = false, G_key_down = false, B_key_down = false, UP_key_down = false, DOWN_key_down = false;

    enum {
        NONE,
        R,
        G,
        B
    }choosen_color = NONE;

    do
    {
        double newTime = Render::GetTime();
        double deltaTime = newTime - lastTime;
        lastTime = newTime;

        glm::vec2 mpos = Render::updateMouse();
        if( not Render::IsKeyPressed(GLFW_KEY_LEFT_CONTROL)) {
            camera->movePitch(mpos.x);
            camera->moveYaw(mpos.y);
        }
        else{
            Render::changeLightPos(mpos);
            info_about_light_pos->SetText("Light position: " + vec2string(Render::getLightPos()));
        }

        //FPS counter
        nbFrames++;
        if ( newTime - lastTimeForFps >= 1.0 ) {
            //cout << 1000.0/double(nbFrames) << " ms/frame\n";

            fps_text->SetText("FPS: " + std::to_string(nbFrames));
            nbFrames = 0;
            lastTimeForFps += 1.0;
        }

        if( not Render::IsKeyPressed(GLFW_KEY_R)) R_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_R)  == GLFW_PRESS && not R_key_down) {
            R_key_down = true;
            if(choosen_color == NONE) {
                choosen_color = R;
                info_about_color_change->SetText("Change RED color [UP/DOWN]");
            }else if( choosen_color == R){
                choosen_color = NONE;
                info_about_color_change->SetText("");
            }
        }

        if( not Render::IsKeyPressed(GLFW_KEY_G)) G_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_G)  == GLFW_PRESS && not G_key_down) {
            G_key_down = true;
            if(choosen_color == NONE) {
                choosen_color = G;
                info_about_color_change->SetText("Change GREEN color [UP/DOWN]");
            }else if( choosen_color == G){
                choosen_color = NONE;
                info_about_color_change->SetText("");
            }
        }

        if( not Render::IsKeyPressed(GLFW_KEY_B)) B_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_B)  == GLFW_PRESS && not B_key_down) {
            B_key_down = true;
            if(choosen_color == NONE) {
                choosen_color = B;
                info_about_color_change->SetText("Change BLUE color [UP/DOWN]");
            }else if( choosen_color == B){
                choosen_color = NONE;
                info_about_color_change->SetText("");
            }
        }

        if( not Render::IsKeyPressed(GLFW_KEY_UP)) UP_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_UP)  == GLFW_PRESS && not UP_key_down) {
            UP_key_down = true;
            glm::vec3 color = objFile.getModelColor();

            switch (choosen_color){
                case NONE:
                    break;
                case R: {
                    float r = color.r;
                    if(r<1.0f)
                        r+=0.1f;
                    objFile.setModelColor(glm::vec3(r, color.g, color.b));
                }break;
                case G:{
                    float g = color.g;
                    if(g<1.0f)
                        g+=0.1f;
                    objFile.setModelColor(glm::vec3(color.r, g, color.b));
                }break;
                case B:{
                    float b = color.b;
                    if(b<1.0f)
                        b+=0.1f;
                    objFile.setModelColor(glm::vec3(color.r, color.g, b));
                }break;
            }
            color_text->SetText("Model color: " + vec2string(objFile.getModelColor()));
        }

        if( not Render::IsKeyPressed(GLFW_KEY_DOWN)) DOWN_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_DOWN)  == GLFW_PRESS && not DOWN_key_down) {
            DOWN_key_down = true;
            glm::vec3 color = objFile.getModelColor();

            switch (choosen_color){
                case NONE:
                    break;
                case R: {
                    float r = color.r;
                    if(r>0.001f)
                        r-=0.1f;
                    objFile.setModelColor(glm::vec3(r, color.g, color.b));
                }break;
                case G:{
                    float g = color.g;
                    if(g>0.001f)
                        g-=0.1f;
                    objFile.setModelColor(glm::vec3(color.r, g, color.b));
                }break;
                case B:{
                    float b = color.b;
                    if(b>0.001f)
                        b-=0.1f;
                    objFile.setModelColor(glm::vec3(color.r, color.g, b));
                }break;
            }
            color_text->SetText("Model color: " + vec2string(objFile.getModelColor()));
        }


        // Draw a single frame
        Render::DrawFrame(&objFile);

    }while( not ( Render::IsKeyPressed(GLFW_KEY_ESCAPE) || Render::IsWindowClosed() ) );


    Render::CleanUp();
    return 0;
}

void man()
{
    cout << "\033[1;36mOBJViewer\033[0m - program to browse obj files" << endl;
    cout << BOLD(Usage:) << endl;
    cout << "OBJViewer OBJ-FILE [TEXTURE-FILE] | --help" << endl;
    cout << endl;
    cout << BOLD(Arguments:) << endl;
    std::cout << "  OBJ-FILE - *.obj file to load" << std::endl;
    std::cout << "  TEXTURE-FILE - *.bmp or dds file to load" << std::endl;
//    std::cout << "  "<<BOLD(-h)<<" HEIGHT - set window height. Default: 768." << std::endl;
//    std::cout << "      HEIGHT - must be bigger then 400 and smaller then 1080." << std::endl;
//    std::cout << "  "<<BOLD(-w)<<" WIDTH - set window height. Default: 1024." << std::endl;
//    std::cout << "      WIDTH - must be bigger then 400 and smaller then 1920." << std::endl;
//    std::cout << "  "<<BOLD(-q)<<" quiet mode - don't write info on console." << std::endl;
//    std::cout << "      This doesn't apply for error messages." << std::endl;
    cout << endl;
}

void ScrollCallback(double x){
    if( not Render::IsKeyPressed(GLFW_KEY_LEFT_CONTROL)) {
        //TODO: Fix it
        float dst = glm::min(glm::max(camera->getDistance() - 0.25f * (float) x, 0.5f), 20.0f);
        camera->setDistance(dst);
    }
    else{
        float z = 0.1f*(float)x;
        Render::changeLightZPos(z);
    }
}

std::string vec2string(glm::vec3 vec){
    string out = "(";

    std::ostringstream ss;
    ss << vec.r << ", " << vec.g << ", " << vec.b;
    out += ss.str();

    return out + ")\0";
}