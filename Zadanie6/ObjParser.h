#ifndef PGK_OBJPARSER_H
#define PGK_OBJPARSER_H

#include <string>
#include <glm/glm.hpp>
#include <vector>
#include <fstream>
#include <GL/glew.h>


class ObjParser {
    std::string filePath;
    std::fstream fileStream;

    std::vector<glm::vec3>      vertices;
    std::vector<glm::vec2>      uvs;
    std::vector<glm::vec3>      normals;
    std::vector<unsigned short> indices;

    GLuint      vertexbuffer;
    GLuint      uvbuffer;
    GLuint      normalbuffer;
    GLuint      elementbuffer;

    GLuint      texture;

    glm::vec3   color;

    bool        no_texture_cords;
public:
    ObjParser(std::string path);

    //bool SimpleParser();
    bool loadUsingAssImp();
    void sendData2GraphicCard();
    bool loadTexture(std::string path, std::string ext);

    void Render();

    void setModelColor(const glm::vec3 value);
    glm::vec3 getModelColor();

    void box();

    bool isAnyTextureSet() {  return texture != 0;  }

    GLuint getTexture();
};


#endif //PGK_OBJPARSER_H
