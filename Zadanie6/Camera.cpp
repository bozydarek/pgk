#include "Camera.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>

#define RANGE 0.9f // 1.0f is full

Camera::Camera(glm::vec3 pos)
    :position(pos)
{

}

glm::vec3 Camera::getDirection() {
    return glm::vec3(
            cos(yaw) * sin(pitch),
            sin(yaw),
            cos(yaw) * cos(pitch)
    );
}

glm::mat4 Camera::getProjectionMatrix()
{
    return glm::perspective(GetFOV(), 4.0f / 3.0f, 0.1f, 100.0f);
}

glm::mat4 Camera::getViewMatrix()
{
    return glm::lookAt(
            -getDirection() * distance,   // Camera is here
            glm::vec3(0,0,0),         // and looks here : at the same position, plus "direction"
            glm::vec3(0,1,0)          // Head is up (set to 0,-1,0 to look upside-down)
    );
}

void Camera::moveYaw(float delta)
{
    yaw = glm::clamp(yaw+delta, -PI/2.0f*RANGE, PI/2.0f*RANGE);
    //yaw = glm::clamp(yaw+delta, -PI/2.0f, PI/2.0f);
    //std::cout << "yaw:" << yaw << std::endl;
}

void Camera::movePitch(float delta)
{
    pitch += delta;
//    std::cout << pitch << std::endl;
}


glm::vec3 Camera::getPosition(){
    return -getDirection() * distance;
}

Camera::~Camera() {

}

float Camera::getDistance() const {
    return distance;
}

void Camera::changeDistance(float distance_delta) {
    distance += distance_delta;
}

void Camera::setDistance(float distance_) {
    distance = distance_;
}

void Camera::reset() {
    pitch = PI/2;
    yaw = 0.0f;
}
