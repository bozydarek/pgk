#ifndef RENDER_H
#define RENDER_H

#include <algorithm>
#include <functional>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/vec2.hpp>
#include <vector>
#include <glm/vec3.hpp>

class ObjParser;

class Render
{
private:
	Render() = delete; // static class

	// Used for scroll handling
	static void ScrollCallback(GLFWwindow*, double, double);

    static float 	pxsizex, pxsizey;
	static int      width, height;
	static GLuint   VertexArrayID;
	static float	window_ratio;

	static glm::vec3 lightPos;

public:
    static int 	    Init (int height_, int width_);

	static void     DrawFrame(ObjParser *file);
	static void 	CleanUp ();

	static double 	GetTime ();
	static bool 	IsKeyPressed (int key);
	static bool 	IsWindowClosed ();

	static void 		centerMouseOnStart();
	static int 			getWidth();
	static int 			getHeight();
	static float 		getWindowRatio();

    static void         centerMouse();
    static glm::vec2    updateMouse();
    static void         hackForMouseCenter();
	
	// Scroll callback
	static std::function<void(double)> scroll_callback;

	static void 		changeLightPos(glm::vec2 mouse_pos);
	static glm::vec3 	getLightPos();
	static void 		changeLightZPos(float zpos);
};


#endif //RENDER_H
