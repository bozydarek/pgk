#include "ObjParser.h"
#include "Texture.h"
#include "Shader.h"

#include <iostream>

// Include AssImp
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

ObjParser::ObjParser(std::string path)
    :filePath(path)
    ,texture(0)
    ,color(glm::vec3(0.9f, 0.0f, 0.1f))
    ,no_texture_cords(false)
{

}


bool ObjParser::loadUsingAssImp(){

    Assimp::Importer importer;

    const aiScene* scene = importer.ReadFile(filePath, aiProcess_GenNormals | aiProcess_Triangulate | aiProcess_GenUVCoords | 0 );
    if( !scene ) {
        std::cout << importer.GetErrorString() << "\n";
        return false;
    }
    const aiMesh* mesh = scene->mMeshes[0]; // In this simple example code we always use the 1rst mesh (in OBJ files there is often only one anyway)

    // Fill vertices positions
    if(mesh->mVertices != nullptr) {
        vertices.reserve(mesh->mNumVertices);
        for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
            aiVector3D pos = mesh->mVertices[i];
            vertices.push_back(glm::vec3(pos.x, pos.y, pos.z));
        }
    }
    else{
        std::cout << "!!! There is no vertices in this obj file\n";
        return false;
    }

    // Fill vertices texture coordinates
    if(mesh->mTextureCoords[0] != nullptr)
    {
        uvs.reserve(mesh->mNumVertices);
        for(unsigned int i=0; i<mesh->mNumVertices; i++){
            aiVector3D UVW = mesh->mTextureCoords[0][i]; // Assume only 1 set of UV coords; AssImp supports 8 UV sets.
            uvs.push_back(glm::vec2(UVW.x, UVW.y));
        }
    }
    else{
        std::cout << "! There is no texture coords in this obj file\n";
        std::cout << "... but it should be generated automatically ...\n";
        std::cout << " ok, never mind ;/\n";

        no_texture_cords = true;
    }

    // Fill vertices normals
    if(mesh->mNormals != nullptr)
    {
        normals.reserve(mesh->mNumVertices);
        for(unsigned int i=0; i<mesh->mNumVertices; i++){
            aiVector3D n = mesh->mNormals[i];
            normals.push_back(glm::vec3(n.x, n.y, n.z));
        }
    }
    else
    {
        std::cout << "! There is no normal vectors in this obj file\n";
        std::cout << "... but it should be generated automatically ...?!\n";
    }



    // Fill face indices
    indices.reserve(3*mesh->mNumFaces);
    for (unsigned int i=0; i<mesh->mNumFaces; i++){
        // Assume the model has only triangles.
        indices.push_back(mesh->mFaces[i].mIndices[0]);
        indices.push_back(mesh->mFaces[i].mIndices[1]);
        indices.push_back(mesh->mFaces[i].mIndices[2]);
    }

    return true;
    // The "scene" pointer will be deleted automatically by "importer"
}

void ObjParser::sendData2GraphicCard() {
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), vertices.data(), GL_STATIC_DRAW);

    glGenBuffers(1, &uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), uvs.data(), GL_STATIC_DRAW);

    glGenBuffers(1, &normalbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), normals.data(), GL_STATIC_DRAW);

    glGenBuffers(1, &elementbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), indices.data() , GL_STATIC_DRAW);
}

void ObjParser::Render() {
    if(not no_texture_cords) {
        // Bind our texture in Texture Unit 1
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture);
    }

    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

    // 2nd attribute buffer : UVs
    if (uvs.size() != 0) {
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);
    }

    // 3rd attribute buffer : normals

    if (normals.size() != 0) {
        glEnableVertexAttribArray(2);
        glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
        glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0 );
    }

    // Index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

    // Draw the triangles !
    glDrawElements( GL_TRIANGLES, (GLsizei) indices.size(), GL_UNSIGNED_SHORT, (void*)0 );
}

bool ObjParser::loadTexture(std::string path, std::string ext) {

    if( no_texture_cords ) {
        std::cout << "Sorry, but your model hasn't got texture cords. \n";
        return false;
    }

    if( ext == ".bmp" || ext == ".BMP" ) {
        texture = loadBMP(path.c_str());
        return texture != 0;
    }
    else if( ext == ".dss" || ext == ".DDS" ){
        texture = loadDDS(path.c_str());
        return texture != 0;
    }else{
        std::cout << "Unable to load " << path << " - unsupported type!" << std::endl;
    }

    return false;
}

void ObjParser::setModelColor(const glm::vec3 value) {
    color = value;
}

glm::vec3 ObjParser::getModelColor() {
    return color;
}

GLuint ObjParser::getTexture() {
    return texture;
}





// only for debug:
static const GLfloat g_vertex_buffer_data[] = {

        -1.0f,-1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f,
        1.0f, 1.0f,-1.0f,

        -1.0f,-1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f
};


static const GLfloat g_normals_buffer_data[] = {
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
};

static const unsigned short g_ind[] = {
        0, 2, 3,
        1, 0, 3,

        1, 3, 7,
        1, 7, 5,

        2, 7, 3,
        2, 6, 7,

        6, 5, 7,
        6, 4, 5,

        6, 2, 0,
        6, 0, 4,

        0, 1, 5,
        0, 5, 4
};

void ObjParser::box(){
    for(int i=0; i<8; ++i)
    {
        vertices.push_back(glm::vec3(g_vertex_buffer_data[3*i],g_vertex_buffer_data[3*i+1],g_vertex_buffer_data[3*i+2]));
        normals.push_back(glm::vec3(g_normals_buffer_data[3*i],g_normals_buffer_data[3*i+1],g_normals_buffer_data[3*i+2]));
    }

    for(int i=0; i<36; ++i)
    {
        indices.push_back(g_ind[i]);
    }
}