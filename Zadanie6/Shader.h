#ifndef SHADER_H
#define SHADER_H

#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Shader{

protected:
    GLuint shader_id;
    bool load(const char * vertex_file_path, const char * fragment_file_path);

public:
    Shader(const char * vertex_file_path, const char * fragment_file_path);
    virtual ~Shader();

    void use();
};


class BasicShader : public Shader {
    // uniform locations
    GLint MVP;
    GLint view;
    GLint model;
    GLint texture;
    GLint light_pos;
    GLint modelColor;
    GLint hasTexture;

public:
    BasicShader(const char *vertex_file_path, const char *fragment_file_path);

    void set_model(const GLfloat *value);
    void set_view(const GLfloat *value);
    void set_model_view_projection(const GLfloat *value);
    void set_light_pos(const GLfloat *value);
    void set_texture(unsigned int value);
    void set_has_texture(bool value);
    void set_model_color(const GLfloat *value);

};

class FontShader : public Shader{
private:
    GLint uniform_tex;
    GLint uniform_color;
public:
    FontShader(const char * vertex_file_path, const char * fragment_file_path);

    void set_font_tex(int value);
    void set_font_color(const GLfloat *value);
};


#endif
