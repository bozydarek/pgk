#version 330

in vec2 texcoord;
uniform sampler2D tex;
uniform vec4 color;
out vec4 color_out;

//https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Text_Rendering_01#Shaders

void main(void) {
    // Map texture's r channel to color's alpha.
    color_out = vec4(1, 1, 1, texture(tex, texcoord).r) * color;
}
