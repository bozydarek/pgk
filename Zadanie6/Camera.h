#ifndef PGK_CAMERA_H
#define PGK_CAMERA_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include <math.h>

#define PI 3.14159265f

class Camera {
protected:
    float       pitch = PI/2.0f;
    float       yaw = 0.0f;
    glm::vec3   position;

    float       speed = 3.0f;
    float       FoV = 0.785f; //45 deg
    float       distance = 6.0f;
public:

    Camera( glm::vec3 pos = glm::vec3(0) );
    virtual ~Camera();

    void        movePitch(float delta);
    void        moveYaw(float delta);

    glm::vec3   getDirection();
    glm::mat4   getProjectionMatrix();

    glm::mat4   getViewMatrix();
    glm::vec3   getPosition();

    void        SetFOV(float f) { FoV = f; }
    void        SetFOVdg(float f) { FoV = f*2.0f*3.1415926f/360.0f; }
    float       GetFOV() const { return FoV; }
    float       GetFOVdg() const { return FoV*360.0f/(2.0f*3.1415926f); }

    float       getDistance() const;
    void        changeDistance(float distance_delta);
    void        setDistance(float distance_);

    void        reset();

};


#endif //PGK_CAMERA_H
