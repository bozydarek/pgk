#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;

// Ouput data
out vec4 colorA;

// Values that stay constant for the whole mesh.
uniform bool hasTexture;
uniform vec3 modelColor;
uniform sampler2D myTextureSampler;
uniform mat4 MV;
uniform vec3 LightPosition_worldspace;

float noise(){
    //return sin(Position_worldspace.x*13) + (sin(Position_worldspace.y));
    //return sin(Position_worldspace.x*13) + (sin(Position_worldspace.y*29));
    //return sin(Position_worldspace.x*13) + (sin(Position_worldspace.y*29)) + (sin(Position_worldspace.z*7));
    //return sin(Position_worldspace.x*3) + (sin(Position_worldspace.y*11)) + (sin(Position_worldspace.z*7)) * cos(Position_worldspace.y + 2* Position_worldspace.z);
    //return sin(Position_worldspace.x*3) + (sin(Position_worldspace.y*11)) + (sin(Position_worldspace.z*7)) * cos(Position_worldspace.y - 20* Position_worldspace.z);
    //return floor(sin(Position_worldspace.x*11) + (sin(Position_worldspace.y*11)));
    //return floor(sin(Position_worldspace.x*11) + (sin(Position_worldspace.y*11)) + (sin(Position_worldspace.z*11)));
    //return floor(sin(Position_worldspace.x*17) + (sin(Position_worldspace.y*14)) + (sin(Position_worldspace.z*4)));
    return floor(sin(Position_worldspace.x*11) + (sin(Position_worldspace.y*11)) + (sin(Position_worldspace.z*11)) + cos(Position_worldspace.y * 110));
}

void main(){

	// Light emission properties
	vec3 LightColor = vec3(1,1,1);
	float LightPower = 40.0f;
	
	// Material properties
	vec3 MaterialDiffuseColor = modelColor; //vec3(1.0f, 0.0f, 0.0f); //texture( myTextureSampler, UV ).rgb;
	if( hasTexture )
	{
	    MaterialDiffuseColor = texture( myTextureSampler, UV ).rgb;
	}
	vec3 MaterialAmbientColor = vec3(0.1,0.1,0.1) * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.3,0.3,0.3);

	// Distance to the light
	float distance = length( LightPosition_worldspace - Position_worldspace );

	// Normal of the computed fragment, in camera space
	vec3 n = normalize( Normal_cameraspace );
	// Direction of the light (from the fragment to the light)
	vec3 l = normalize( LightDirection_cameraspace );
	// Cosine of the angle between the normal and the light direction, 
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendicular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float cosTheta = clamp( dot( n,l ), 0,1 );
	
	// Eye vector (towards the camera)
	vec3 E = normalize(EyeDirection_cameraspace);
	// Direction in which the triangle reflects the light
	vec3 R = reflect(-l,n);
	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float cosAlpha = clamp( dot( E,R ), 0,1 );
	
	vec3 color = 
		// Ambient : simulates indirect lighting
		MaterialAmbientColor * noise() +
		// Diffuse : "color" of the object
		MaterialDiffuseColor * LightColor * LightPower * cosTheta / (distance*distance) +
		// Specular : reflective highlight, like a mirror
		MaterialSpecularColor * LightColor * LightPower * pow(cosAlpha,5) / (distance*distance);

	colorA = vec4(color , 1.0f);

}
