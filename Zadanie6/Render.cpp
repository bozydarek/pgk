#include "Render.h"
#include "Shader.h"
#include "Camera.h"
#include "Fonts.h"
#include "Text.h"
#include "ObjParser.h"

#include <iostream>
#include <zconf.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <memory>

#define RGB(r,g,b) r/255.0f, g/255.0f, b/255.0f

#define MOUSE_SPEED 0.005f

GLFWwindow* window;
extern std::shared_ptr<Camera>camera;

extern std::shared_ptr<BasicShader> global_shader;

// Static members of Render class
float Render::pxsizex, Render::pxsizey, Render::window_ratio;
int Render::width, Render::height;
GLuint Render::VertexArrayID;
glm::vec3 Render::lightPos = glm::vec3(4,4,4);
std::function<void(double)> Render::scroll_callback;

double Render::GetTime()
{
	return glfwGetTime();
}

bool Render::IsKeyPressed(int key)
{
	return glfwGetKey(window, key) == GLFW_PRESS;
}

bool Render::IsWindowClosed()
{
	return glfwWindowShouldClose(window) == 1;
}


// GLFW initialization
int Render::Init(int height_, int width_)
{
    height = height_;
    width = width_;

    window_ratio = (width*1.0f)/(height*1.0f);

	if( !glfwInit() )
	{
		std::cerr << "Failed to initialize GLFW." << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	window = glfwCreateWindow( width, height, "OBJ Viewer", NULL, NULL);
	pxsizex = 2.0f/width;
	pxsizey = 2.0f/height;

	if( window == NULL )
	{
		std::cerr << "Failed to open GLFW window." << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glewExperimental = (GLboolean) true;
	if (glewInit() != GLEW_OK)
	{
        std::cerr << "Failed to initialize GLEW\n";
		glfwTerminate();
		return -1;
	}

	centerMouse();

	glClearColor(RGB(250,250,250), 0.0f);

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited movement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwSetScrollCallback(window, ScrollCallback);

    //global_shader = std::make_shared<BasicShader>("StandardVertexShader.glsl", "StandardFragmentShader.glsl");
    global_shader = std::make_shared<BasicShader>("StandardVertexShader.glsl", "Noise1FragmentShader.glsl");

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    //glFrontFace(GL_CW);
    //glEnable(GL_CULL_FACE);

    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

	return 0;
}


void Render::DrawFrame(ObjParser *file)
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glEnable(GL_DEPTH_TEST);
    
	global_shader->use();

    glm::mat4 ProjectionMatrix = camera->getProjectionMatrix();
    glm::mat4 ViewMatrix = camera->getViewMatrix();
    glm::mat4 ModelMatrix = glm::mat4(1.0);
    glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

    global_shader->set_model_view_projection(&MVP[0][0]);
    global_shader->set_model(&ModelMatrix[0][0]);
    global_shader->set_view(&ViewMatrix[0][0]);

    if(file->isAnyTextureSet())
    {
        global_shader->set_has_texture(true);
        global_shader->set_texture(1);
    }
    else
    {
        global_shader->set_model_color(&(file->getModelColor())[0]);
    }

    global_shader->set_light_pos(&lightPos[0]);

	file->Render();

    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    glDisable(GL_DEPTH_TEST); // Force text to always stay on top
    for(auto t : Text::texts){
        if(!t->active) {
            continue;
        }

        glm::vec2 off = t->px_offset;
        glm::vec3 color = t->color;

        render_text(t->text.c_str(), -1.0f + off.x * pxsizex, 1.0f - off.y * pxsizey, color.r, color.g, color.b, t->size, pxsizex, pxsizey);
    }

	//glUseProgram(0);

	glfwSwapBuffers(window);
	glfwPollEvents();
}

void Render::CleanUp()
{
    //delete global_shader;
    glDeleteVertexArrays(1, &VertexArrayID);
	glfwTerminate();
}

void Render::centerMouse()
{
    //set mouse cursor in the center of window
    glfwSetCursorPos(window, width/2, height/2);
}

glm::vec2 Render::updateMouse() {

    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    centerMouse();
    return MOUSE_SPEED * glm::vec2(float( width/2 - xpos ), float( height/2 - ypos ));
}

void Render::ScrollCallback(GLFWwindow *, double, double x)
{
    if(scroll_callback)
        scroll_callback(x);
}

void Render::centerMouseOnStart() {
    glfwSwapBuffers(window);
    glfwPollEvents();
    centerMouse();
}


int Render::getWidth() {
    return width;
}

int Render::getHeight() {
    return height;
}

float Render::getWindowRatio() {
    return window_ratio;
}

void Render::hackForMouseCenter()
{
	glfwSwapBuffers(window);
	glfwPollEvents();
	centerMouse();
}

void Render::changeLightPos(glm::vec2 mouse_pos) {
    lightPos.x -= mouse_pos.x;
    lightPos.y += mouse_pos.y;
}

void Render::changeLightZPos(float zpos) {
    lightPos.z -= zpos;
    if(abs(lightPos.z) < 0.05)
        lightPos.z = 0.0f;
}


glm::vec3 Render::getLightPos() {
    return lightPos;
}

