Przeglądaraka plików OBJ
===

Obsługa
---
**Klawisze**:

*R,G,B* - aktywacja/dezaktywacja zmiany koloru modelu - efekt widoczny gdy nie mamy załadowanych tekstur

*Strzałki góra/dół* - zwiększenie/zminejszenie aktywnego koloru w modelu o 0.1f

**Mysz**: Poruszanie kamerą

**Scroll**: Przybliżanie/Oddalanie

Przykłady użycia
---
```
./OBJViewer example.obj
./OBJViewer example.obj example_texture.DDS
./OBJViewer example.obj example_texture.bmp
```

Zmiana szumu
---
Póki co ręcznie w shaderze - funkcja ```noise()```. 
Jest tam w komentarzach kilka prostych funkcji.