#include "Camera.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#define RANGE 0.7f // 1.0f is full
#define DISTANCE 18.0f

void Camera::movePitch(float delta)
{
    pitch += delta;
}

void Camera::moveYaw(float delta)
{
    yaw = glm::clamp(yaw+delta, -PI/2.0f*RANGE, PI/2.0f*RANGE);
}

Camera::Camera(glm::vec3 pos)
    :position(pos)
{

}

glm::vec3 Camera::getDirection() {
    return glm::vec3(
            cos(yaw) * sin(pitch),
            sin(yaw),
            cos(yaw) * cos(pitch)
    );
}

glm::mat4 Camera::getProjectionMatrix()
{
    return glm::perspective(GetFOV(), 4.0f / 3.0f, 0.1f, 100.0f);
}

glm::mat4 Camera::getViewMatrix()
{
    return glm::lookAt(
            -getDirection() * DISTANCE,   // Camera is here
            glm::vec3(0,0,0),         // and looks here : at the same position, plus "direction"
            glm::vec3(0,1,0)          // Head is up (set to 0,-1,0 to look upside-down)
    );
}

glm::vec3 Camera::getPosition(){
    return -getDirection() * DISTANCE;
}

glm::vec3 Camera::getCameraPosition() {
    return getPosition();
}

float Camera::getSpeed() const {
    return speed;
}

void Camera::setSpeed(float speed) {
    Camera::speed = speed;
}

Camera::~Camera() {

}
