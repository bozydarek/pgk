#ifndef PGK_BUBBLE_H
#define PGK_BUBBLE_H

#include <GL/glew.h>
#include "Camera.h"
#include "Shape.h"
#include <vector>

using namespace std;

class Sphere {
    vector<glm::vec3>   sphere_vertices;
    vector<glm::vec3>   sphere_normals;
    vector<unsigned short>   sphere_indices;
    vector<glm::vec3>   sphere_colores;

    GLuint              vertex_array_id;
    GLuint              normal_buffer;
    GLuint              vertex_buffer;
    GLuint              element_buffer;
    GLuint              color_buffer;

private:
    void create_bubble_vertices(unsigned short layers, unsigned short vertices_per_layer);

public:
    Sphere(unsigned short accuracy);
    ~Sphere();

    void prepare_for_drawing();

    void draw(Camera *view, Player *player, const glm::vec3 &position, float radius);
    void draw(Camera *pCamera, Player *pPlayer, Bubble *pBubble);

    void draw_single_bubble();

    void clean_after_drawing();

    void drawPlayer(Camera *pCamera, Player *pPlayer, Bubble *pBubble);
};


#endif //PGK_BUBBLE_H
