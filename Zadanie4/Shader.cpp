#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;

#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>

#include "Shader.h"

BasicShader *global_shader;

bool Shader::load(const char * vertex_file_path,const char * fragment_file_path){

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if(VertexShaderStream.is_open()){
		std::string Line = "";
		while(getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}else{
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if(FragmentShaderStream.is_open()){
		std::string Line = "";
		while(getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;


	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}



	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}



	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	
	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);
	
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	shader_id = ProgramID;

	return true;
}

int static uniformCheck(GLint uniform, std::string name) {

    if (uniform == -1)
	{
		std::cerr << "A " << name << " uniform is missing from the shader." << std::endl;
		return 1;
	}

	return 0;
}

Shader::Shader(const char *vertex_file_path, const char *fragment_file_path) {
	if (not load(vertex_file_path, fragment_file_path)) {
		exit(1);
	}
}
BasicShader::BasicShader(const char *vertex_file_path, const char *fragment_file_path)
	: Shader(vertex_file_path, fragment_file_path)
{
    model_view_projection = glGetUniformLocation(shader_id, "MVP");
    model_transparency =    glGetUniformLocation(shader_id, "alpha");
    model =                 glGetUniformLocation(shader_id, "model");
    ambient_strength =      glGetUniformLocation(shader_id, "ambient_strength");
    light_pos =             glGetUniformLocation(shader_id, "light_pos");
    light_color =           glGetUniformLocation(shader_id, "light_color");
	object_pigment =        glGetUniformLocation(shader_id, "object_pigment");
	view_pos = 				glGetUniformLocation(shader_id, "view_pos");
    external_camera = 		glGetUniformLocation(shader_id, "external_camera");
    more_light =            glGetUniformLocation(shader_id, "more_light");

    if( uniformCheck(model_view_projection, "MVP")
        || uniformCheck(model_transparency, "transparency")
        || uniformCheck(model, "model")
        || uniformCheck(ambient_strength, "ambient_strength")
        || uniformCheck(light_pos, "light_pos")
        || uniformCheck(light_color, "light_color")
        || uniformCheck(object_pigment, "object_pigment")
        || uniformCheck(view_pos, "view_pos")
        || uniformCheck(external_camera, "external_camera")
        ) {
            exit(1);
    }

}

void BasicShader::set_light_color(const GLfloat *value) {
    glUniform3fv(light_color, 1, value);
}

void BasicShader::set_object_pigment(const GLfloat *value) {
    glUniform3fv(object_pigment, 1, value);
}

void BasicShader::set_light_pos(const GLfloat *value) {
    glUniform3fv(light_pos, 1, value);
}

void BasicShader::set_ambient_strength(float value) {
    glUniform1f(ambient_strength, value);
}

void BasicShader::set_model(const GLfloat *value) {
    glUniformMatrix4fv(model, 1, GL_FALSE, value);
}

void BasicShader::set_model_view_projection(const GLfloat *value) {
    glUniformMatrix4fv(model_view_projection, 1, GL_FALSE, value);
}

void BasicShader::set_model_transparency(float value) {
    glUniform1f(model_transparency, value);
}

void Shader::use()
{
    glUseProgram(shader_id);
}

void BasicShader::set_view_pos(const GLfloat *value) {
	glUniform3fv(view_pos, 1, value);

}

void BasicShader::set_external_camera(bool value)
{
    glUniform1i(external_camera, value);
}

void BasicShader::set_more_light(float value) {
    glUniform1f(more_light,value);
}


FontShader::FontShader(const char *vertex_file_path, const char *fragment_file_path)
        : Shader(vertex_file_path, fragment_file_path)
{
    uniform_tex = glGetUniformLocation(shader_id,"tex");
    uniform_color = glGetUniformLocation(shader_id,"color");

    if( uniformCheck(uniform_tex, "tex")
        || uniformCheck(uniform_color, "textcolor")
            ) {
        exit(1);
    }
}

void FontShader::set_font_tex(int value)
{
    glUniform1i(uniform_tex, value);
}

void FontShader::set_font_color(const GLfloat *value)
{
    glUniform4fv(uniform_color, 1, value);
}