#ifndef PGK_BOX_H
#define PGK_BOX_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include "Camera.h"

#define AQUARIUM_LENGTH 16.0f
#define AQUARIUM_WIDTH 6.0f
#define AQUARIUM_HEIGHT 3.0f

class Aquarium;

class Box {
public:
    Box();
    ~Box();

    void init();
    void draw();
    void clean();
    void draw_aquarium(Camera *view, Player *player, Aquarium *aquarium);

    glm::vec3 pos;
private:
    GLuint BoxVA;
    GLuint BoxVerticesVB;
    GLuint BoxColorVB;
    GLuint BoxNormalsVB;

    glm::vec3 scale;
};


#endif //PGK_BOX_H
