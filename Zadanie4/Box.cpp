#include "Box.h"
#include "Shader.h"
#include "Player.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

extern BasicShader* global_shader;

static const GLfloat g_vertex_buffer_data[] = {
                // left
                -1.0f,-1.0f,-1.0f,
                -1.0f,1.0f,1.0f,
                -1.0f,-1.0f,1.0f,
                // back
                1.0f,1.0f,-1.0f,
                -1.0f,1.0f,-1.0f,
                -1.0f,-1.0f,-1.0f,
                // down
                1.0f,-1.0f,1.0f,
                1.0f,-1.0f,-1.0f,
                -1.0f,-1.0f,-1.0f,
                // back
                1.0f, 1.0f,-1.0f,
                -1.0f,-1.0f,-1.0f,
                1.0f, -1.0f,-1.0f,
                // left
                -1.0f,-1.0f,-1.0f,
                -1.0f, 1.0f,-1.0f,
                -1.0f, 1.0f,1.0f,

                1.0f, -1.0f,1.0f,
                -1.0f,-1.0f,-1.0f,
                -1.0f,-1.0f,1.0f,

                -1.0f, 1.0f,1.0f,
                 1.0f,-1.0f,1.0f,
                -1.0f,-1.0f,1.0f,

                1.0f,1.0f,1.0f,
                1.0f,1.0f,-1.0f,
                1.0f,-1.0f,-1.0f,

                1.0f,-1.0f,-1.0f,
                1.0f,-1.0f,1.0f,
                1.0f,1.0f,1.0f,

                1.0f,1.0f,1.0f,
                -1.0f,1.0f,-1.0f,
                1.0f,1.0f,-1.0f,

                1.0f,1.0f,1.0f,
                -1.0f,1.0f,1.0f,
                -1.0f,1.0f,-1.0f,

                1.0f,1.0f,1.0f,
                1.0f,-1.0f,1.0f,
                -1.0f,1.0f,1.0f
        };

static const GLfloat g_color_buffer_data[] = {
        0.200f,  0.300f,  0.700f,
        0.300f,  0.400f,  0.800f,
        0.200f,  0.300f,  0.700f,

        0.700f,  0.400f,  0.700f,
        0.700f,  0.400f,  0.700f,
        0.700f,  0.400f,  0.700f,

        0.200f,  0.200f,  0.200f,
        0.200f,  0.200f,  0.200f,
        0.200f,  0.200f,  0.200f,

        0.700f,  0.400f,  0.700f,
        0.700f,  0.400f,  0.700f,
        0.700f,  0.400f,  0.700f,

        0.200f,  0.300f,  0.700f,
        0.300f,  0.400f,  0.800f,
        0.300f,  0.400f,  0.800f,

        0.200f,  0.200f,  0.200f,
        0.200f,  0.200f,  0.200f,
        0.200f,  0.200f,  0.200f,

        0.200f,  0.500f,  0.200f,
        0.200f,  0.500f,  0.200f,
        0.200f,  0.500f,  0.200f,

        0.200f,  0.500f,  0.200f,
        0.200f,  0.500f,  0.200f,
        0.200f,  0.500f,  0.200f,

        0.200f,  0.500f,  0.200f,
        0.200f,  0.500f,  0.200f,
        0.200f,  0.500f,  0.200f,

        0.200f,  0.700f,  0.700f,
        0.200f,  0.700f,  0.700f,
        0.200f,  0.700f,  0.700f,

        0.200f,  0.700f,  0.700f,
        0.200f,  0.700f,  0.700f,
        0.200f,  0.700f,  0.700f,

        0.200f,  0.200f,  0.200f,
        0.200f,  0.200f,  0.200f,
        0.200f,  0.200f,  0.200f
};

static const GLfloat g_normals_buffer_data[] = {
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,

        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,

        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,

        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,

        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,

        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,

        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f
};

Box::Box()
{

}

void Box::init()
{
    glGenVertexArrays(1, &BoxVA);
    glBindVertexArray(BoxVA);

    glGenBuffers(1, &BoxVerticesVB);
    glBindBuffer(GL_ARRAY_BUFFER, BoxVerticesVB);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(1, &BoxColorVB);
    glBindBuffer(GL_ARRAY_BUFFER, BoxColorVB);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);


    glGenBuffers(1, &BoxNormalsVB);
    glBindBuffer(GL_ARRAY_BUFFER, BoxNormalsVB);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_normals_buffer_data), g_normals_buffer_data, GL_STATIC_DRAW);

}

Box::~Box()
{
}

void Box::clean()
{
    glDeleteBuffers(1, &BoxVerticesVB);
    glDeleteBuffers(1, &BoxColorVB);

    glDeleteVertexArrays(1, &BoxVA);
}

void Box::draw()
{
    glBindVertexArray(BoxVA);

    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, BoxVerticesVB);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, BoxColorVB);
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, BoxNormalsVB);
    glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glDrawArrays(GL_TRIANGLES, 0, 12*3); // 12*3 indices starting at 0 -> 12 triangles

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}

void Box::draw_aquarium(Camera *view, Player *player, Aquarium *aquarium)
{
    glm::vec3 object_color = glm::vec3(1.0f,1.0f,1.0f);
    glm::vec3 light_color =  glm::vec3(1.0f,1.0f,1.0f);
    glm::vec3 light_pos =    player->getPosition();

    glm::mat4 Projection = view->getProjectionMatrix();
    glm::mat4 View       = view->getViewMatrix();
    glm::mat4 Model      = glm::scale(glm::vec3(aquarium->getWidth(), aquarium->getHeight(), aquarium->getLength()));
    glm::mat4 MVP        = Projection * View * Model; // Remember, matrix multiplication is the other way around

    global_shader->use();
    global_shader->set_model_view_projection(&MVP[0][0]);
    global_shader->set_model_transparency(1.0f);
    global_shader->set_model(&Model[0][0]);
    global_shader->set_object_pigment(&object_color[0]);

    global_shader->set_ambient_strength(0.2f);
    global_shader->set_light_color(&light_color[0]);
    global_shader->set_light_pos(&light_pos[0]);
    global_shader->set_view_pos(&(view->getPosition())[0]);

    global_shader->set_external_camera(view->isExternal());
    global_shader->set_more_light(1.0f);

    draw();
}