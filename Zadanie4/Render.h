#ifndef RENDER_H
#define RENDER_H

#include <algorithm>
#include <functional>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/vec2.hpp>
#include <vector>
#include "Box.h"

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768

class Render
{
private:
	Render() = delete; // static class

	// Used for scroll handling
	static void ScrollCallback(GLFWwindow*, double, double);

    static float 	pxsizex, pxsizey;
public:
	static int 		Init ();
	static void 	DrawFrame(Camera *camera);
	static void 	CleanUp ();

	static double 	GetTime ();
	static bool 	IsKeyPressed (int key);
	static bool 	IsWindowClosed ();


    static void         centerMouse();
    static glm::vec2    updateMouse();
	// Scroll callback
	static std::function<void(double)> scroll_callback;



public:

    static void centerMouseOnStart();
    static void changeForExternalCamera();
	static void changeForPlayerCamera();
};


#endif //RENDER_H
