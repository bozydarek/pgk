#ifndef PGK_PLAYER_H
#define PGK_PLAYER_H

#include "Camera.h"
#include "Shape.h"

class Player: public Camera {
public:
    Player();

    void    moveForward(double time);
    void    moveBackward(double time);
    void    moveRight(double time);
    void    moveLeft(double time);
    void    moveUp(double time);
    void    moveDown(double time);

    glm::vec3 getRight();
    glm::mat4 getViewMatrix() override;
    glm::vec3 getPosition() override;
    glm::vec3 getCameraPosition() override;

    bool    isColliding(Bubble *b);

private:
    float   player_size = 0.3f;
    const static glm::vec3 start_pos;
public:
    float   getPlayer_size() const;
    void    setPlayer_size(float player_size);

    void    setPlayerOnStart();

};


#endif //PGK_PLAYER_H
