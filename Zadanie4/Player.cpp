#include "Player.h"
#include "Box.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

extern cameraMode camMode;

const glm::vec3 Player::start_pos = glm::vec3(0.0f, 0.0f, AQUARIUM_LENGTH-1.0f);

Player::Player()
    : Camera(start_pos)
{
    pitch = PI;
    type = cameraMode::FIRST_PERSON;
}

void Player::moveForward(double time)
{
    position += getDirection() * (float)time * speed;
}

void Player::moveBackward(double time)
{
    position -= getDirection() * (float)time * speed;
}

void Player::moveRight(double time)
{
    position += getRight() * (float)time * speed;
}

void Player::moveLeft(double time)
{
    position -= getRight() * (float)time * speed;
}

void Player::moveUp(double time)
{
    glm::vec3 up = glm::cross( getRight(), getDirection() );
    position += up * (float)time  * speed;
}

void Player::moveDown(double time)
{
    glm::vec3 up = glm::cross( getRight(), getDirection() );
    position -= up * (float)time  * speed;
}

glm::vec3 Player::getRight() {
    return glm::vec3( sin(pitch - 3.14f/2.0f), 0, cos(pitch - 3.14f/2.0f) );
}


glm::mat4 Player::getViewMatrix()
{
    if(camMode==cameraMode::FIRST_PERSON)
        return glm::lookAt(
                position,                // Camera is here
                position + getDirection(), // and looks here : at the same position, plus "direction"
                glm::vec3(0,1,0)    // Head is up (set to 0,-1,0 to look upside-down)
        );
    else
        return glm::lookAt(
                position - getDirection()*2.0f,                // Camera is here
                position, // and looks here : at the same position, plus "direction"
                glm::vec3(0,1,0)    // Head is up (set to 0,-1,0 to look upside-down)
        );

}

glm::vec3 Player::getPosition() {
        return position;
}

glm::vec3 Player::getCameraPosition() {
    if(camMode==cameraMode::THIRD_PERSON)
        return position - getDirection()*2.0f;
    else
        return position;
}

bool Player::isColliding(Bubble *b){
    return b->distance_from(position) < player_size + b->getRadius();
}

float Player::getPlayer_size() const {
    return player_size;
}

void Player::setPlayer_size(float player_size) {
    Player::player_size = player_size;
}

void Player::setPlayerOnStart() {
    position = start_pos;
}
