#version 330 core

// Interpolated values from the vertex shaders
in vec3 fragmentColor;
in vec3 fragmentPos;
in vec3 normals;

uniform float alpha;
uniform float ambient_strength;
uniform vec3 light_pos;
uniform vec3 light_color;
uniform vec3 view_pos;
uniform vec3 object_pigment;
uniform bool external_camera;
uniform float more_light = 1.0f;

// Ouput data
out vec4 color;

void main(){


        vec3 normalized_normal = normalize(normals);
        vec3 view_vector = view_pos - fragmentPos;
        vec3 view_dir = normalize(view_vector);

        vec3 light_vector = light_pos - fragmentPos;
        vec3 light_dir = normalize(light_vector);
        float diff = max(dot(normalized_normal, light_dir), 0.0);

        float should_reflect = float(dot(normalized_normal, light_dir) > 0.1);
        vec3 reflect_dir = reflect(-light_dir, normalized_normal);
        float spec = pow(max(dot(view_dir, reflect_dir),0.0f), 32) * should_reflect;

        vec3 ambient = ambient_strength * light_color;
        vec3 diffuse = 0.8f * diff * light_color;
        vec3 specular = 0.9f * light_color * spec;

        vec3 result = more_light * ( ambient + diffuse + specular) * fragmentColor * object_pigment;

        // gamma correction
        //vec3 gamma = vec3(1.0/2.2);
        //result = pow(result, gamma);

        float distance = length(view_pos - fragmentPos);
        result *= exp (-0.005f * distance);

        float aalpha = alpha;
        if( external_camera == false )
        {
            float ldst = length(light_pos - fragmentPos);
            result *= 1/(1+0.3*ldst);//+0.009*ldistance_sqr);

            aalpha /= max((distance*0.5f), 1.0f);
        }

        color = vec4(result, aalpha);
}
