#ifndef PGK_SHAPE_H
#define PGK_SHAPE_H


#include <glm/glm.hpp>

class Shape {
    glm::vec3   position;
    float       distance_from_sth;
public:
    Shape(glm::vec3 pos);

    void    setPosition(const glm::vec3 &position);
    const   glm::vec3 &getPosition() const;

    float   distance_from(glm::vec3 point);
    void    update_distance(glm::vec3 point);

    bool operator<(const Shape &rhs) const;


    //for debug
    float getDistance_from_sth() const;
};

class Bubble : public Shape{
public:
    enum types{
        Normal,
        Freezer,
        Bomb
    };
private:
    float   radius;
    float   move_speed;
    float   growth_speed;

    float   alpha;
    glm::vec3 color;

    types   type;
public:
    Bubble(float rad, glm::vec3 pos, float speed = 1.0f, float growth = 0.1f);

    void    moveUp(double time);

    float   getRadius() const;
    void    setRadius(float radius);
    float   getSpeed() const;
    void    setSpeed(float speed);

    const glm::vec3 &getColor() const;
    void    setColor(const glm::vec3 &color);
    float   getAlpha() const;
    void    setAlpha(float alpha);

    types getType() const;

    void setType(types type);

};

class Aquarium : public Shape{
    float width;
    float height;
    float length;
public:
    Aquarium();
    float getHeight() const;
    float getWidth() const;
    float getLength() const;
};


#endif //PGK_SHAPE_H
