#include <iostream>
#include <unistd.h>

#include "Render.h"
#include "Player.h"
#include "Game.h"
#include "Tools.h"
#include "Text.h"
#include "Fonts.h"

extern Camera *external;
extern Player *player;
Camera* actualCam;
cameraMode camMode;

#define MANUAL_LINES 4

std::shared_ptr<Text> text_level;
std::shared_ptr<Text> text_score;
std::shared_ptr<Text> text_fps;
std::shared_ptr<Text> text_Pause;
std::shared_ptr<Text> text_manual[MANUAL_LINES];
std::shared_ptr<Text> text_control;
std::shared_ptr<Text> text_aditional_control;

std::vector<std::string> manual ={
        "Try to get to the other side without touching the bubbles.",
        "Touching the bubble ends the game. Except the blue and red bubbles.",
        "Blue one stop bubbles from floating for 5 sec, but it cost you 25 points from score.",
        "Red one explode and destroy all bubbles in range, but it cost 10 points per every destroyed bubble."
};

using namespace std;

// uncomment to have fps counter in terminal
// imho useless when vsync is activated
// unless you have more then 8k bubbles on screen
#define FPS_COUNTER


void ScrollCallback(double x)
{
    float fov = actualCam->GetFOVdg();
    if(camMode == cameraMode::EXTERNAL)
        fov = glm::min(glm::max(fov - 5.0f*(float)x,50.0f),150.0f);
    else
        fov = glm::min(glm::max(fov - 5.0f*(float)x,30.0f),90.0f);

    actualCam->SetFOVdg(fov);

    //std::cout << "Scroll; fov = " << fov << "\n";
}

void UpdateTexts(){
    text_level->SetText("Level: " + std::to_string(Game::level));
    text_score->SetText("Score: " + std::to_string(Game::score));
}

void UpdateFPS(int fps){
    text_fps->SetText("FPS: " + std::to_string(fps));
}

int main()
{
    std::cout << "Starting preparing the game\n";
    srand((unsigned int) time(NULL));

    int n = Render::Init();
    if( n ) return n;
    Render::scroll_callback = ScrollCallback;


    double lastFrameTime = Render::GetTime();


    bool P_key_down = false, TAB_key_down = false, L_key_down = false;
    bool pause = true;

    Render::centerMouseOnStart();

    external = new Camera();
    player = new Player();

    camMode = cameraMode :: THIRD_PERSON;
    actualCam = player;

#ifdef FPS_COUNTER
    double lastTimeForFps = Render::GetTime();
    int nbFrames = 0;
#endif

    Game::init();
    Game::prepare_level();

    vector<Bubble*> to_delete;

    init_font();

    text_level = Text::Create("", glm::vec2(5,50), 48, glm::vec3(0.0,1.0,0.7));
    text_score = Text::Create("", glm::vec2(10,80), 24, glm::vec3(1.0,0.6,0.6));
    text_fps   = Text::Create("", glm::vec2(WINDOW_WIDTH-90,25));

    text_Pause = Text::Create("Press P to start game", glm::vec2(WINDOW_WIDTH/2-250,WINDOW_HEIGHT/2+10), 48, glm::vec3(0.7,1.0,0.7));
    text_control = Text::Create("For moving use: W/A/S/D and mouse", glm::vec2(WINDOW_WIDTH/2-220,WINDOW_HEIGHT/2+70), 24, glm::vec3(0.3,0.8,0.3));
    text_aditional_control = Text::Create("You can use TAB to change camera", glm::vec2(WINDOW_WIDTH/2-210,WINDOW_HEIGHT/2+90), 24, glm::vec3(0.3,0.8,0.3));

    text_manual[0] = Text::Create(manual[0], glm::vec2(20, WINDOW_HEIGHT - 80), 30, glm::vec3(1.0, 0.5, 0.5));
    for (int i = 1; i < MANUAL_LINES; ++i) {
        text_manual[i] = Text::Create(manual[i], glm::vec2(20, WINDOW_HEIGHT - 70 + 20 * i), 20,
                                      glm::vec3(1.0, 0.7, 0.7));
    }


    bool first_run = true;

    UpdateTexts();
    UpdateFPS(0);

    std::cout << "Game is ready\n";

    do
    {
        double newtime = Render::GetTime();

        // Calculate time for physic calculations
        double time_delta = newtime-lastFrameTime;
        lastFrameTime = newtime;


        glm::vec2 mpos = Render::updateMouse();
        actualCam->movePitch(mpos.x);
        actualCam->moveYaw(mpos.y);

    #ifdef FPS_COUNTER
        //FPS counter
        nbFrames++;
        if ( newtime - lastTimeForFps >= 1.0 )
        {
            //cout << "FPS: " << nbFrames << "\n";
            //cout << 1000.0/double(nbFrames) << " ms/frame\n";

            UpdateFPS(nbFrames);
            nbFrames = 0;
            lastTimeForFps += 1.0;
        }
    #endif

        if( not Render::IsKeyPressed(GLFW_KEY_P)) P_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_P)  == GLFW_PRESS && not P_key_down){
            P_key_down = true;
            pause = !pause;
            text_Pause->active = pause;
            if(first_run)
            {
                first_run = false;
                for (int i = 0; i < MANUAL_LINES; ++i) {
                    text_manual[i]->active = false;
                }
                text_control->active = false;
                text_aditional_control->active = false;
            }
        }

        if( not Render::IsKeyPressed(GLFW_KEY_L)) L_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_L)  == GLFW_PRESS && not L_key_down){
            L_key_down = true;
            ++Game::level;
            Game::prepare_level();
            UpdateTexts();
        }

        if( not Render::IsKeyPressed(GLFW_KEY_TAB)) TAB_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_TAB)  == GLFW_PRESS && not TAB_key_down){
            TAB_key_down = true;
            string mode = "";

            switch (camMode){
                case cameraMode::EXTERNAL:
                    camMode = cameraMode::THIRD_PERSON;
                    Render::changeForPlayerCamera();
                    actualCam = player;
                    mode = "Third person";
                    break;
                case cameraMode::THIRD_PERSON:
                    camMode = cameraMode::FIRST_PERSON;
                    mode = "First person";
                    break;
                case cameraMode::FIRST_PERSON:
                    camMode = cameraMode::EXTERNAL;
                    Render::changeForExternalCamera();
                    actualCam = external;
                    mode = "External";
                    break;
            }

            std::cout << "Switch to " << mode << " mode\n";
        }

        // If camera isn't in External mode, we can move;
        if( camMode != cameraMode::EXTERNAL && not pause){
            if (Render::IsKeyPressed(GLFW_KEY_W) == GLFW_PRESS){
                if(not Game::isPlayerColidingWithWall(MoveDir::FORWARD, time_delta))
                    player->moveForward(time_delta);
            }
            if (Render::IsKeyPressed(GLFW_KEY_S) == GLFW_PRESS){
                if(not Game::isPlayerColidingWithWall(MoveDir::BACKWARD, time_delta))
                    player->moveBackward(time_delta);
            }
            if (Render::IsKeyPressed(GLFW_KEY_D) == GLFW_PRESS){
                if(not Game::isPlayerColidingWithWall(MoveDir::RIGHT, time_delta))
                    player->moveRight(time_delta);
            }
            if (Render::IsKeyPressed(GLFW_KEY_A) == GLFW_PRESS){
                if(not Game::isPlayerColidingWithWall(MoveDir::LEFT, time_delta))
                    player->moveLeft(time_delta);
            }
            if (Render::IsKeyPressed(GLFW_KEY_SPACE) == GLFW_PRESS){
                if(not Game::isPlayerColidingWithWall(MoveDir::UP, time_delta))
                    player->moveUp(time_delta);
            }
            if (Render::IsKeyPressed(GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS){
                if(not Game::isPlayerColidingWithWall(MoveDir::DOWN, time_delta))
                    player->moveDown(time_delta);
            }
        }

        if( Game::isItEnd() )
        {
            ++Game::level;
            Game::prepare_level();
            UpdateTexts();
        }

        if( not pause ) {
            if(Game::freezTime <= 0 )
                Game::iteration(time_delta);
            else{
                Game::playerIteration();
                Game::freezTime -= time_delta;
                }

            for( auto b : Game::bubbles )
            {
                if(player->isColliding(b) )
                {
                    //std::cout << "Bum\n";
                    switch (b->getType())
                    {
                        case Bubble::Freezer:
                            Game::freezTime=FREEZ_TIME;
                            Game::score -= 5*FREEZ_TIME;
                            break;

                        case Bubble::Bomb:
                            Game::handleBombExplode(b);
                            break;

                        case Bubble::Normal:
                            std::cout << "Game Over!\n";
                            Game::level = 0;
                            Game::prepare_level();
                            break;
                    }
                    to_delete.push_back(b);
                    UpdateTexts();
                }
            }

            if(to_delete.size())
            {
                for(auto b : to_delete)
                {
                    auto it = std::find( Game::bubbles.begin(), Game::bubbles.end(), b);
                    if (it != Game::bubbles.end()) {
                        Game::bubbles.erase(it);
                    }

                }

                std::for_each(to_delete.begin(), to_delete.end(), delete_pointed_to<Bubble>);
                to_delete.clear();
            }

        }

        // Draw a single frame
        Render::DrawFrame(actualCam);

    }while( not ( Render::IsKeyPressed(GLFW_KEY_ESCAPE) || Render::IsWindowClosed() ) );

    Render::CleanUp();
    Game::CleanUp();

    delete external;
    delete player;

    return 0;
}

