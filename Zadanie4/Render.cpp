#include "Render.h"
#include "Shader.h"
#include "Camera.h"
#include "Player.h"
#include "Sphere.h"
#include "Shape.h"
#include "Game.h"
#include "Fonts.h"
#include "Text.h"

#include <iostream>
#include <zconf.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <memory>

#define RGB(r,g,b) r/255.0f, g/255.0f, b/255.0f

#define MOUSE_SPEED 0.005f

GLFWwindow* window;
Camera *external;
Player *player;

extern Shader* global_shader;

// Static members of Render class
float Render::pxsizex, Render::pxsizey;

std::function<void(double)> Render::scroll_callback;

double Render::GetTime()
{
	return glfwGetTime();
}

bool Render::IsKeyPressed(int key)
{
	return glfwGetKey(window, key) == GLFW_PRESS;
}

bool Render::IsWindowClosed()
{
	return glfwWindowShouldClose(window) == 1;
}


// GLFW initialization
int Render::Init()
{
	if( !glfwInit() )
	{
		std::cerr << "Failed to initialize GLFW." << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	window = glfwCreateWindow( WINDOW_WIDTH, WINDOW_HEIGHT, "Aquarium", NULL, NULL);
	pxsizex = 2.0f/WINDOW_WIDTH;
	pxsizey = 2.0f/WINDOW_HEIGHT;

	if( window == NULL )
	{
		std::cerr << "Failed to open GLFW window." << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glewExperimental = (GLboolean) true;
	if (glewInit() != GLEW_OK)
	{
        std::cerr << "Failed to initialize GLEW\n";
		glfwTerminate();
		return -1;
	}

	centerMouse();

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited movement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwSetScrollCallback(window, ScrollCallback);

	changeForPlayerCamera();

    global_shader = new BasicShader("vertex.glsl", "fragment.glsl");

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    //glFrontFace(GL_CW);
    glEnable(GL_CULL_FACE);

	// Enable alpha effects
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}


void Render::DrawFrame(Camera *camera)
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    //draw sth
    glFrontFace(GL_CCW);
    Game::drawAquarium(camera);

    //Game::drawBubbles(camera);
    glFrontFace(GL_CW);
    Game::drawPlayer(camera);
    Game::drawBubbles(camera);


	for(auto t : Text::texts){
		if(!t->active) continue;
		glm::vec2 off = t->px_offset;
		glm::vec3 color = t->color;

		render_text(t->text.c_str(), -1.0f + off.x * pxsizex, 1.0f - off.y * pxsizey, color.r, color.g, color.b, t->size, pxsizex, pxsizey);
	}

	glUseProgram(0);


	glfwSwapBuffers(window);
	glfwPollEvents();
}

void Render::CleanUp()
{
    delete global_shader;
	glfwTerminate();
}

void Render::centerMouse()
{
    //set mouse cursor in the center of window
    glfwSetCursorPos(window, WINDOW_WIDTH/2, WINDOW_HEIGHT/2);
}

glm::vec2 Render::updateMouse() {

    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    centerMouse();
    return MOUSE_SPEED * glm::vec2(float(WINDOW_WIDTH/2 - xpos ), float( WINDOW_HEIGHT/2 - ypos ));
}

void Render::ScrollCallback(GLFWwindow *, double, double x)
{
    if(scroll_callback)
        scroll_callback(x);
}

void Render::centerMouseOnStart() {
    glfwSwapBuffers(window);
    glfwPollEvents();
    centerMouse();
}

void Render::changeForExternalCamera() {
    glClearColor(RGB(250,250,250), 0.0f);
}

void Render::changeForPlayerCamera() {
    glClearColor(RGB(50, 150, 255), 0.0f);

}



