#ifndef SHADER_H
#define SHADER_H

class Shader{

protected:
    GLuint shader_id;
    bool load(const char * vertex_file_path, const char * fragment_file_path);

public:
    Shader(const char * vertex_file_path, const char * fragment_file_path);

    void use();
};


class BasicShader : public Shader{
    // uniform locations
    GLint model_view_projection;
    GLint model_transparency;
    GLint model;
    GLint ambient_strength;
    GLint light_pos;
    GLint light_color;
    GLint object_pigment;
    GLint view_pos;
    GLint external_camera;

    GLint more_light;

public:
    BasicShader(const char * vertex_file_path, const char * fragment_file_path);

    void set_light_color(const GLfloat *value);
    void set_object_pigment(const GLfloat *value);
    void set_light_pos(const GLfloat *value);
    void set_ambient_strength(float value);
    void set_model(const GLfloat *value);
    void set_model_view_projection(const GLfloat *value);
    void set_model_transparency(float value);
    void set_view_pos(const GLfloat *value);
    void set_external_camera(bool value);
    void set_more_light(float value);
};

class FontShader : public Shader{
private:
    GLint uniform_tex;
    GLint uniform_color;
public:
    FontShader(const char * vertex_file_path, const char * fragment_file_path);

    void set_font_tex(int value);

    void set_font_color(const GLfloat *value);
};

#endif
