#ifndef PGK_TOOLS_H
#define PGK_TOOLS_H

template <typename T>
void delete_pointed_to(T* const ptr)
{
    delete ptr;
}


#endif //PGK_TOOLS_H
