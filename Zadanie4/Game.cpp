#include "Game.h"
#include "Tools.h"
#include <algorithm>
#include <stdlib.h>
#include <iostream>
#include "Player.h"

std::vector<Bubble*> Game::bubbles;
Sphere*     Game::bubble_model;
Bubble*     Game::playerBubble;
Box*        Game::box_model;
Aquarium*   Game::aquarium;

#define RGB(r,g,b) r/255.0f, g/255.0f, b/255.0f

extern Player* player;

unsigned int    Game::max_bubbles_for_level;
const float     Game::place_for_player = 2.0f;

float           Game::hgt, Game::wdt, Game::dst;
double          Game::freezTime;
unsigned int    Game::level;
int             Game::score;

void Game::prepare_level( )
{
    if(level == 0)
    {
        score = 0;
    } else{
        score += level * 50;
    }

    player->setPlayerOnStart();
    freezTime = 0;

    bubbles.clear();

    max_bubbles_for_level = 40 + 10*level;

    dst = (2.0f*aquarium->getLength() - place_for_player)/ max_bubbles_for_level;
    hgt = 2.0f*aquarium->getHeight() / 100.0f;
    wdt = 2.0f*aquarium->getWidth() / 100.0f;

    for( unsigned int i=0; i < max_bubbles_for_level; ++i)
    {
        float radius = rand() % 10 / 20.0f + 0.2f;
        float z = -aquarium->getLength() + ( i + rand() % 10) * dst;
        float y = -aquarium->getHeight() + rand() % 100 * hgt;
        float x = -aquarium->getWidth() + rand() % 100 * wdt;

        if(z > aquarium->getLength()-place_for_player) z -= 4.0f + rand() % 4 * dst;
        if(z < -aquarium->getLength()) z += 1.0f;

        bubbles.push_back( new Bubble(radius, glm::vec3(x,y,z)) );
        //std::cout << x << " " << y << " " << z << "\n";
    }


    dst = (2.0f*aquarium->getLength() - place_for_player)/ 100.0f;
}

void Game::drawBubbles(Camera *camera)
{
    glm::vec3 pos = camera->getCameraPosition();
    for( auto b : bubbles )
    {
        b->update_distance(pos);
    }

    std::sort(bubbles.begin(), bubbles.end(), [](Bubble* a, Bubble* b) {
        return *b < *a;
    });

    //std::cout << "New frame\n";
    bubble_model->prepare_for_drawing();
    for( auto b : bubbles )
    {
        bubble_model->draw(camera, player, b);
        //std::cout << b->getPosition().x << " " << b->getPosition().y << " " << b->getPosition().z  << " -> dst = " << b->distance_from_sth <<"\n" ;
    }
    bubble_model->clean_after_drawing();
}

void Game::drawPlayer(Camera *camera)
{
    bubble_model->prepare_for_drawing();
    bubble_model->drawPlayer(camera, player, playerBubble);
    bubble_model->clean_after_drawing();
}

void Game::init()
{
    bubble_model = new Sphere(10); // no. of vertices is about ( 4 * x )^2
    playerBubble = new Bubble(player->getPlayer_size(), player->getPosition() , 0.0f, 0.0f);
    playerBubble->setAlpha(1.0f);
    playerBubble->setColor(glm::vec3(1.0f, 0.0f, 0.0f));

    box_model = new Box();
    box_model->init();

    aquarium = new Aquarium();

    score = 0;
}

void Game::playerIteration()
{
    playerBubble->setPosition(player->getPosition());
}

void Game::iteration(double time)
{
    playerIteration();

    std::vector<Bubble*> to_remove;
    for( auto b : bubbles )
    {
        b->moveUp(time);
        if(b->getPosition().y > aquarium->getHeight() + 1.5f)
            to_remove.push_back(b);
    }

    for (auto e : to_remove)
    {
        auto it = std::find(bubbles.begin(), bubbles.end(), e);
        if (it != bubbles.end()) {
            bubbles.erase(it);
        }

        // generat new bubble for every removed
        float radius = rand() % 10 / 20.0f + 0.1f;

        float z = -aquarium->getLength() + rand() % 100 * dst;
        float y = -3.0f*aquarium->getHeight() + rand() % 100 * hgt;
        float x = -aquarium->getWidth() + rand() % 100 * wdt;

        int rnd = std::rand() % 100;
        if( rnd < (2 * log(level)) )
        {
            //std::cout << "Freezer spawned\n";
            Bubble *b = new Bubble(radius, glm::vec3(x, y, z), 0.7f, 0.04f);
            b->setColor(glm::vec3(RGB(50, 50, 250)));
            b->setType(Bubble::types::Freezer);
            b->setAlpha(0.8f);
            bubbles.push_back(b);
        }
        else if( rnd < (3 * log(level))){
            //std::cout << "Bomb spawned\n";
            Bubble *b = new Bubble(radius, glm::vec3(x, y, z), 1.8f, 0.4f);
            b->setColor(glm::vec3(RGB(250, 50, 70)));
            b->setType(Bubble::types::Bomb);
            b->setAlpha(0.8f);
            bubbles.push_back(b);
        }
        else
        {
            float speed = 0.9f + 0.01f * (rand()%50) + 0.05 * level;
            bubbles.push_back(new Bubble(radius, glm::vec3(x, y, z), speed));
        }
    }

    std::for_each(to_remove.begin(), to_remove.end(), delete_pointed_to<Bubble>);
}

void Game::CleanUp() {
    delete bubble_model;
    delete box_model;
}

void Game::drawAquarium(Camera *pCamera) {
    box_model->draw_aquarium(pCamera, player, aquarium);
}

bool Game::isPlayerColidingWithWall(MoveDir dir, double delta) {
    glm::vec3 pos = player->getPosition();
    glm::vec3 newPos = pos;

    switch (dir)
    {
        case MoveDir::FORWARD:
            newPos += player->getDirection() * (float)delta * player->getSpeed();
            break;

        case MoveDir::BACKWARD:
            newPos -= player->getDirection() * (float)delta * player->getSpeed();
            break;

        case LEFT:
            newPos -= player->getRight() * (float)delta * player->getSpeed();
            break;

        case RIGHT:
            newPos += player->getRight() * (float)delta * player->getSpeed();
            break;
        case UP:{
                glm::vec3 up = glm::cross( player->getRight(), player->getDirection() );
                newPos += up * (float)delta * player->getSpeed();
            }
            break;

        case DOWN:{
            glm::vec3 up = glm::cross( player->getRight(), player->getDirection() );
            newPos -= up * (float)delta * player->getSpeed();
            }
            break;
    }

    float rad = player->getPlayer_size();

    if(newPos.x + rad > aquarium->getWidth()) return true;
    if(newPos.x - rad < -aquarium->getWidth()) return true;

    if(newPos.y + rad > aquarium->getHeight()) return true;
    if(newPos.y - rad < -aquarium->getHeight()) return true;

    if(newPos.z + rad > aquarium->getLength()) return true;

    return false;
}

bool Game::isItEnd() {
    return player->getPosition().z < -aquarium->getLength();
}

void Game::handleBombExplode(Bubble *pBubble) {
    glm::vec3 pos = pBubble->getPosition();
    std::vector<Bubble*> to_remove;

    for( auto b : bubbles )
    {
        b->update_distance(pos);
        if(b->getDistance_from_sth() < b->getRadius()+BOMB_RANGE)
        {
            to_remove.push_back(b);
            score -= 10;
        }
    }

    for (auto e : to_remove)
    {
        auto it = std::find(bubbles.begin(), bubbles.end(), e);
        if (it != bubbles.end()) {
            bubbles.erase(it);
        }
    }

    //std::for_each(to_remove.begin(), to_remove.end(), delete_pointed_to<Bubble>);

}

