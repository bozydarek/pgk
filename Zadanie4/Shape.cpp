#include <iostream>
#include "Shape.h"

#include <glm/gtx/string_cast.hpp>
#include "Box.h"

#define RGB(r,g,b) r/255.0f, g/255.0f, b/255.0f

Shape::Shape(glm::vec3 pos)
    : position( pos )
{

}

const glm::vec3 &Shape::getPosition() const {
    return position;
}

void Shape::setPosition(const glm::vec3 &position) {
    Shape::position = position;
}

float Shape::distance_from(glm::vec3 point) {
    return glm::distance(point, position);
}

void Shape::update_distance(glm::vec3 point) {
    distance_from_sth = distance_from(point);
}

bool Shape::operator<(const Shape &rhs) const {
    return distance_from_sth < rhs.distance_from_sth;
}

float Shape::getDistance_from_sth() const {
    return distance_from_sth;
}

Bubble::Bubble(float rad, glm::vec3 pos, float speed, float growth)
    : Shape(pos)
    , radius( rad )
    , move_speed( speed )
    , growth_speed( growth )
    , alpha( 0.65f )
    , color( glm::vec3( RGB(250,250,250) ) )
    , type( types::Normal )
{

}

void Bubble::moveUp(double time)
{
    setPosition(getPosition() + glm::vec3(0,1,0) * move_speed * (float)time);
    setRadius(radius + growth_speed * (float)time);
}

float Bubble::getRadius() const {
    return radius;
}

void Bubble::setRadius(float radius) {
    Bubble::radius = radius;
}

float Bubble::getSpeed() const {
    return move_speed;
}

void Bubble::setSpeed(float speed) {
    Bubble::move_speed = speed;
}

const glm::vec3 &Bubble::getColor() const {
    return color;
}

void Bubble::setColor(const glm::vec3 &color) {
    Bubble::color = color;
}

float Bubble::getAlpha() const {
    return alpha;
}

void Bubble::setAlpha(float alpha) {
    Bubble::alpha = alpha;
}

Bubble::types Bubble::getType() const {
    return type;
}

void Bubble::setType(Bubble::types type) {
    Bubble::type = type;
}

Aquarium::Aquarium()
        : Shape(glm::vec3(0))
        , width(AQUARIUM_WIDTH)
        , height(AQUARIUM_HEIGHT)
        , length(AQUARIUM_LENGTH)
{

}

float Aquarium::getHeight() const {
    return height;
}

float Aquarium::getWidth() const {
    return width;
}

float Aquarium::getLength() const {
    return length;
}
