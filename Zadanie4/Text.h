#ifndef PGK_TEXT_H
#define PGK_TEXT_H

#include <string>
#include <glm/glm.hpp>
#include <vector>
#include <functional>
#include <memory>

class Text {
public:
    std::string     text;
    glm::vec3       color;
    unsigned int    size;
    bool            active = true;
    glm::vec2       px_offset;

public:
    Text(std::string text, glm::vec2 px_offset, unsigned int size = 22, glm::vec3 color = glm::vec3(0.0,0.0,0.0));
    ~Text();

    static std::shared_ptr<Text> Create(std::string text, glm::vec2 px_offset, unsigned int size = 22, glm::vec3 color = glm::vec3(0.0,0.0,0.0));
    void SetText(std::string t);
private:
    friend class Render;

    // The list of all texts for easier iteration in render.cpp
    static std::vector<Text *> texts;

};


#endif //PGK_TEXT_H
