#ifndef PGK_CAMERA_H
#define PGK_CAMERA_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include <math.h>

#define PI 3.14159265f

class Player;

enum cameraMode{
    EXTERNAL,
    THIRD_PERSON,
    FIRST_PERSON,
};

class Camera {
protected:
    float       pitch = PI/2;
    float       yaw = -0.2f;
    glm::vec3   position;

    cameraMode  type = cameraMode::EXTERNAL;

    float       speed = 3.0f;
    float       FoV = 1.57f; //90 deg
public:
    //friend Player;

    Camera( glm::vec3 pos = glm::vec3(0) );
    virtual ~Camera();

    void                movePitch(float delta);
    void                moveYaw(float delta);

    glm::vec3           getDirection();
    glm::mat4           getProjectionMatrix();

    virtual glm::mat4   getViewMatrix();
    virtual glm::vec3   getPosition();
    virtual glm::vec3   getCameraPosition();

    void                SetFOV(float f) { FoV = f; }
    void                SetFOVdg(float f) { FoV = f*2.0f*3.1415926f/360.0f; }
    float               GetFOV() const { return FoV; }
    float               GetFOVdg() const { return FoV*360.0f/(2.0f*3.1415926f); }


    bool                isExternal(){ return type == cameraMode ::EXTERNAL; }


    float               getSpeed() const;
    void                setSpeed(float speed);

};


#endif //PGK_CAMERA_H
