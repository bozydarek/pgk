#ifndef PGK_GAME_H
#define PGK_GAME_H


#include <vector>
#include "Shape.h"
#include "Sphere.h"
#include "Box.h"

#define FREEZ_TIME 5.0
#define BOMB_RANGE 3.0


enum MoveDir{
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    UP,
    DOWN,
};


class Game {
    static unsigned int    max_bubbles_for_level;
    static Bubble*         playerBubble;
    const static float     place_for_player;

    static float           dst, hgt, wdt;
public:
    Game() = delete;
    static void CleanUp();

    static void init();
    static void prepare_level( );
    static void drawBubbles(Camera *camera);
    static void iteration(double time);
    static void drawPlayer(Camera *camera);
    static void drawAquarium(Camera *pCamera);

public:
    static std::vector<Bubble*> bubbles;
    static Sphere*      bubble_model;
    static Box*         box_model;
    static Aquarium*    aquarium;

    static double           freezTime;
    static unsigned int     level;
    static int score;


    static bool isPlayerColidingWithWall(MoveDir dir, double delta);
    static bool isItEnd();
    static void playerIteration();
    static void handleBombExplode(Bubble *pBubble);

};


#endif //PGK_GAME_H
