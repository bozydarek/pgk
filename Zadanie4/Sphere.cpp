#include "Sphere.h"
#include "Shader.h"
#include "Tools.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

extern BasicShader* global_shader;

#include <glm/glm.hpp>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include "Player.h"


Sphere::Sphere(unsigned short accuracy)
{
    create_bubble_vertices(accuracy * 4, accuracy * 4);

    assert( accuracy * 4 * accuracy * 4 < USHRT_MAX );

    glGenVertexArrays(1, &vertex_array_id);
    glBindVertexArray(vertex_array_id);

    glGenBuffers(1, &vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sphere_vertices.size() * sizeof(glm::vec3), sphere_vertices.data(), GL_STATIC_DRAW);

    glGenBuffers(1, &color_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
    glBufferData(GL_ARRAY_BUFFER, sphere_colores.size() * sizeof(glm::vec3), sphere_colores.data(), GL_STATIC_DRAW);

    glGenBuffers(1, &normal_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
    glBufferData(GL_ARRAY_BUFFER, sphere_normals.size() * sizeof(glm::vec3), sphere_normals.data(), GL_STATIC_DRAW);


    glGenBuffers(1, &element_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sphere_indices.size()*sizeof(unsigned short), sphere_indices.data() , GL_STATIC_DRAW);
}

Sphere::~Sphere()
{
    glDeleteBuffers(1, &vertex_buffer);
    glDeleteBuffers(1, &normal_buffer);
    glDeleteVertexArrays(1, &vertex_array_id);
}

void Sphere::create_bubble_vertices(unsigned short layers, unsigned short vertices_per_layer)
{
    if (!sphere_vertices.empty())
        return;

    float delta_theta = (float) PI / (layers - 1);
    float delta_phi = 2.0f * (float) PI / vertices_per_layer;


    sphere_vertices.push_back(glm::vec3(0.0f, -1.0f, 0.0f));
    sphere_normals.push_back(glm::normalize(glm::vec3(0.0f, -1.0f, 0.0f)));

    float scolor = 15.0f/(layers+15.0f);
    sphere_colores.push_back(glm::vec3( scolor, scolor, scolor));

    for (unsigned short layer = 1; layer < layers - 1; ++layer)
    {
        float theta = delta_theta * layer - (float) PI / 2.0f;
        for (unsigned short vertex = 0; vertex < vertices_per_layer; ++vertex)
        {
            float phi = vertex * delta_phi;
            float x = cos(theta) * cos(phi);
            float y = sin(theta);
            float z = cos(theta) * sin(phi);

            float color = (layer+15.0f)/(layers+15.0f);

            sphere_vertices.push_back(glm::vec3(x, y, z));
            sphere_normals.push_back(glm::normalize(glm::vec3(x, y, z)));
            sphere_colores.push_back(glm::vec3( color, color, color));
            //sphere_colores.push_back(glm::vec3( (float)((rand() % 100) / 100.0f), (float)((rand() % 100) / 100.0f), (float)((rand() % 100) / 100.0f) ));

        }
    }

//    for( auto i : sphere_colores )
//    {
//        std::cout << i.x << " " << i.y << " " << i.z << " \n";
//    }

    sphere_vertices.push_back( glm::vec3(0.0f, 1.0f, 0.0f) );
    sphere_normals.push_back( glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f)) );
    sphere_colores.push_back( glm::vec3( 1.0f, 1.0f ,1.0f) );

    // up
    for (unsigned short j = 0; j < vertices_per_layer; ++j) {
        sphere_indices.push_back( j+1 );
        sphere_indices.push_back( 0 );
        sphere_indices.push_back( (j+1) % (vertices_per_layer+1) + 1 );
    }

    // middle

    for (unsigned short i = 0; i < layers - 3; ++i) {
        // 1 case
        for (unsigned short j = 0; j < vertices_per_layer; ++j) {
            sphere_indices.push_back( j + 1 + i*vertices_per_layer );
            sphere_indices.push_back( (j + 1) % vertices_per_layer + 1 + i*vertices_per_layer );
            sphere_indices.push_back( j + 1 + vertices_per_layer + i*vertices_per_layer );

        }

        // 2 case
        for (unsigned short j = 0; j < vertices_per_layer; ++j) {
            sphere_indices.push_back( (j + 1) % vertices_per_layer + 1 + (i+1)*vertices_per_layer);
            sphere_indices.push_back( j + 1 + (i+1)*vertices_per_layer);
            sphere_indices.push_back( (j + 1) % vertices_per_layer + 1 + i*vertices_per_layer  );
        }

    }

    unsigned short pre_last = layers - 3;
    unsigned short lastPointPos = sphere_vertices.size() - 1;

    // down
    for (unsigned short j = 0; j < vertices_per_layer; ++j) {
        sphere_indices.push_back( j + 1 + (pre_last)*vertices_per_layer );
        sphere_indices.push_back( (j + 1) % vertices_per_layer + 1 + (pre_last)*vertices_per_layer );
        sphere_indices.push_back( lastPointPos );
    }
}

void Sphere::prepare_for_drawing() {
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, nullptr );

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, nullptr );

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
    glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, nullptr );

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer);
}

void Sphere::draw_single_bubble()
{
    glDrawElements(GL_TRIANGLES, (GLsizei) sphere_indices.size() , GL_UNSIGNED_SHORT, nullptr);
}

void Sphere::draw(Camera *view, Player *player, const glm::vec3 &position, float radius)
{
    glm::vec3 object_color = glm::vec3(1.0f,1.0f,0.0f);
    glm::vec3 light_color = glm::vec3(1.0f,1.0f,1.0f);
    glm::vec3 light_pos =  player->getPosition();

    glm::mat4 translation_mat = glm::translate(position);
    glm::mat4 scale_mat = glm::scale(glm::vec3(radius,radius,radius));
    glm::mat4 model_mat = translation_mat * scale_mat;

    glm::mat4 mvp = view->getProjectionMatrix() * view->getViewMatrix() * model_mat;

    global_shader->use();
    global_shader->set_model_view_projection(&mvp[0][0]);
    global_shader->set_model_transparency(0.65f);
    global_shader->set_model(&model_mat[0][0]);
    global_shader->set_object_pigment(&object_color[0]);

    global_shader->set_ambient_strength(0.2f);
    global_shader->set_light_color(&light_color[0]);

    global_shader->set_light_pos(&light_pos[0]);
    global_shader->set_view_pos(&(view->getPosition())[0]);

    draw_single_bubble();
}

void Sphere::draw(Camera *pCamera, Player *pPlayer, Bubble *pBubble)
{

    glm::vec3 object_color = pBubble->getColor();
    glm::vec3 light_color = glm::vec3(1.0f,0.5f,0.5f);
    glm::vec3 light_pos =  pPlayer->getPosition();

    glm::mat4 translation_mat = glm::translate( pBubble->getPosition());
    glm::mat4 scale_mat = glm::scale(glm::vec3(pBubble->getRadius(),pBubble->getRadius(),pBubble->getRadius()));
    glm::mat4 model_mat = translation_mat * scale_mat;

    glm::mat4 mvp = pCamera->getProjectionMatrix() * pCamera->getViewMatrix() * model_mat;

    global_shader->use();
    global_shader->set_model_view_projection(&mvp[0][0]);
    global_shader->set_model_transparency(pBubble->getAlpha());
    global_shader->set_model(&model_mat[0][0]);
    global_shader->set_object_pigment(&object_color[0]);

    global_shader->set_ambient_strength(0.2f);
    global_shader->set_light_color(&light_color[0]);
    global_shader->set_light_pos(&light_pos[0]);
    global_shader->set_view_pos(&(pCamera->getPosition())[0]);

    global_shader->set_external_camera(pCamera->isExternal());
    global_shader->set_more_light(1.0f);

    draw_single_bubble();
}

void Sphere::drawPlayer(Camera *pCamera, Player *pPlayer, Bubble *pBubble)
{

    glm::vec3 object_color = pBubble->getColor();
    glm::vec3 light_color = glm::vec3(1.0f,1.0f,1.0f);
    glm::vec3 light_pos =  pPlayer->getCameraPosition();

    glm::mat4 translation_mat = glm::translate( pBubble->getPosition());
    glm::mat4 scale_mat = glm::scale(glm::vec3(pBubble->getRadius(),pBubble->getRadius(),pBubble->getRadius()));
    glm::mat4 model_mat = translation_mat * scale_mat;

    glm::mat4 mvp = pCamera->getProjectionMatrix() * pCamera->getViewMatrix() * model_mat;

    global_shader->use();
    global_shader->set_model_view_projection(&mvp[0][0]);
    global_shader->set_model_transparency(pBubble->getAlpha());
    global_shader->set_model(&model_mat[0][0]);
    global_shader->set_object_pigment(&object_color[0]);

    global_shader->set_ambient_strength(0.2f);
    global_shader->set_light_color(&light_color[0]);
    global_shader->set_light_pos(&light_pos[0]);
    global_shader->set_view_pos(&(pCamera->getPosition())[0]);

    global_shader->set_external_camera(pCamera->isExternal());
    global_shader->set_more_light(3.0f);

    draw_single_bubble();
}

void Sphere::clean_after_drawing()
{
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}
