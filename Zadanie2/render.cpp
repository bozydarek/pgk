#include "render.h"
#include "shader.h"

#include <iostream>
#include "game.h"
#include "cardModels.cpp"

#define WIDTH 1024
#define HEIGHT 768

#define BACKGROUND_COLOR RGB (0, 0, 40)

extern unsigned short selected_x, selected_y;
GLFWwindow* window;

// Static members of Render class
float Render::px_size_x, Render::px_size_y, Render::card_height, Render::card_width;
GLuint Render::symbols_buffer[9], Render::card_cover_buffer, Render::color_cover_buffer, Render::frame_buffer;
GLint Render::uniform_scale, Render::uniform_translate, Render::uniform_color;
GLuint Render::VertexArrayID;
GLuint Render::shader_program_id;


double Render::GetTime()
{
	return glfwGetTime();
}

bool Render::IsKeyPressed(int key)
{
	return glfwGetKey(window, key) == GLFW_PRESS;
}

bool Render::IsWindowClosed()
{
	return glfwWindowShouldClose(window) == 1;
}

int Render::Init()
{
	if( !glfwInit() )
	{
		std::cerr << "Failed to initialize GLFW." << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	window = glfwCreateWindow( WIDTH, HEIGHT, "Memory", NULL, NULL);

	if( window == NULL )
	{
		std::cerr << "Failed to open GLFW window." << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glewExperimental = (GLboolean) true;
	if (glewInit() != GLEW_OK)
	{
        std::cerr << "Failed to initialize GLEW\n";
		glfwTerminate();
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glClearColor(BACKGROUND_COLOR, 0.0f);

    shader_program_id = LoadShaders( "pos.vertexshader", "col.fragmentshader" );
    glUseProgram(shader_program_id);

    uniform_scale = glGetUniformLocation(shader_program_id, "scale");
    uniform_translate = glGetUniformLocation(shader_program_id, "translate");
    uniform_color = glGetUniformLocation(shader_program_id, "color");

    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    if (uniform_scale == -1) {
        std::cerr << "A SCALE uniform is missing from the shader." << std::endl;
        glfwTerminate();
        return -1;
    } else if (uniform_translate == -1) {
        std::cerr << "A TRANSLATE uniform is missing from the shader." << std::endl;
        glfwTerminate();
        return -1;
    }

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_ALWAYS);
//---------------------------------------------------------------
    // TEST
    float lineWidth[2];
    glGetFloatv(GL_LINE_WIDTH_RANGE, lineWidth);
    std::cout << "Line Width range " << lineWidth[0] << "-" << lineWidth[1] << "\n";
    // http://stackoverflow.com/a/34867791
    // "[...] drawing wide lines is a deprecated feature, and will not be supported anymore if you move to a newer (core profile) version of OpenGL."

//---------------------------------------------------------------
    px_size_x = 2.0 / WIDTH;
    px_size_y = 2.0 / HEIGHT;

    card_width = 1.0f / Game::board_width;
    card_height = 1.0f / Game::board_height;
   
    //std::cout << "!CW " << Game::board_width << " " << card_width  <<"\n";
 
    glUniform2f(uniform_scale, card_width, card_height);

    glGenBuffers(1, &card_cover_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, card_cover_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(card_cover), card_cover, GL_STATIC_DRAW);

    glGenBuffers(1, &frame_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, frame_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(frame_buffer_data), frame_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(9, symbols_buffer);
    // Preparing data in buffers
    for(int i = 0; i < SYMBOLS; i++)
    {
        glBindBuffer(GL_ARRAY_BUFFER, symbols_buffer[i]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * symbols_buffer_data_size[i] * 2, symbols_buffer_data[i], GL_STATIC_DRAW);
    }

	return 0;
}

void Render::DrawFrame()
{
	glClear( GL_COLOR_BUFFER_BIT );

//    glEnableVertexAttribArray(1);

    glUseProgram(shader_program_id);

	for( unsigned short i=0; i<Game::board_width; ++i )
    {
		for( unsigned short j=0; j<Game::board_height; ++j )
		{
//            std::cout << i << " " << j << " - " << selected_x <<"--"<< ( i == selected_x && j == selected_y ) << "\n";

            // Frame for selected card
            if( i == selected_x && j == selected_y )
            {
                DrawCardFrame(i, j);
            }

            Card* card = &Game::cards[Game::CardNumber(i, j)];
            if( card->uncovered )
            {
                Render::DrawCoveredCard(i, j);
            }
            else if( not card->removed )
            {
                Render::DrawUncoveredCard(i, j, card->model);
            } 
            else
            {
                //nothing
            }

	    }
    }

//    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

	glUseProgram(0);

	glfwSwapBuffers(window);
	glfwPollEvents();
}

void Render::DrawCoveredCard(unsigned short x, unsigned short y)
{
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, card_cover_buffer);
    //glDepthRange (0.0, 0.9);


    glUniform2f(uniform_translate, x*2.0f-(Game::board_width-1.0f), y*2.0f-(Game::board_height-1.0f));

    glUniform3f(uniform_color, COLOR_D_RED);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glDisableVertexAttribArray(0);

}

void Render::DrawUncoveredCard(unsigned short x, unsigned short y, int model)
{
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, card_cover_buffer);

    glUniform2f(uniform_translate, x*2.0f-(Game::board_width-1.0f), y*2.0f-(Game::board_height-1.0f));
    glUniform3fv(uniform_color, 1, &colors[ 3*(model%Game::colors_on_board) ]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glDisableVertexAttribArray(0);

    DrawSymbolOnCard(x, y, model);
}

void Render::DrawCardFrame(unsigned short x, unsigned short y)
{
    glEnableVertexAttribArray(0);

    //draw frame
    glBindBuffer(GL_ARRAY_BUFFER, frame_buffer);

    glUniform2f(uniform_translate, x*2.0f-(Game::board_width-1.0f), y*2.0f-(Game::board_height-1.0f));
    glUniform3f(uniform_color, 0.8f, 0.7f, 0.2f);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glUniform3f(uniform_color, BACKGROUND_COLOR);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);

    glDisableVertexAttribArray(0);
}

void Render::DrawSymbolOnCard(unsigned short x, unsigned short y, int model)
{
    glEnableVertexAttribArray(0);

    //draw frame
    int symbol_index = (model/Game::colors_on_board) % Game::symbols_on_board;

    glBindBuffer(GL_ARRAY_BUFFER, symbols_buffer[symbol_index]);

    glUniform2f(uniform_translate, x*2.0f-(Game::board_width-1.0f), y*2.0f-(Game::board_height-1.0f));
    glUniform3f(uniform_color, COLOR_D_BLACK);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, symbols_buffer_data_size[symbol_index]);


    glDisableVertexAttribArray(0);
}

void Render::CleanUp()
{
	glDeleteBuffers(9, symbols_buffer);
	glDeleteBuffers(1, &color_cover_buffer);
	glDeleteBuffers(1, &frame_buffer);
	glDeleteVertexArrays(1, &VertexArrayID);
	glDeleteProgram(shader_program_id);

	glfwTerminate();
}



