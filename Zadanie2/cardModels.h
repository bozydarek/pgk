#ifndef CARDMODELS_H
#define CARDMODELS_H

#include <GL/gl.h>

#define COLORS 8
#define SYMBOLS 5

#define RGB(r,g,b) r/255.0f, g/255.0f, b/255.0f

#define COLOR_D_BLACK	RGB ( 0, 0, 0 )
#define COLOR_D_RED		RGB ( 170, 0, 0 )
#define COLOR_D_GREEN	RGB ( 0, 170, 0 )
#define COLOR_D_BROWN	RGB ( 170, 85, 0 )
#define COLOR_D_BLUE	RGB ( 0, 0, 170 )
#define COLOR_D_MAGENTA	RGB ( 170, 0, 170 )
#define COLOR_D_CYAN	RGB ( 0, 170, 170 )
#define COLOR_D_GRAY	RGB ( 170, 170, 170 )

#define COLOR_L_GRAY	RGB ( 128, 128, 128 )
#define COLOR_L_RED		RGB ( 255, 0, 0 )
#define COLOR_L_GREEN	RGB ( 0, 255, 0 )
#define COLOR_L_YELLOW	RGB ( 255, 255, 0 )
#define COLOR_L_BLUE	RGB ( 20, 20, 255 )
#define COLOR_L_MAGENTA	RGB ( 255, 0, 255 )
#define COLOR_L_CYAN	RGB ( 0, 255, 255 )
#define COLOR_L_WHITE	RGB ( 255, 255, 255 )

#endif //CARDMODELS_H
