#include "cardModels.h"

static const GLfloat card_cover[] = {
        -.8f, -.8f,
        -.8f,  .8f,
        .8f,  .8f,
        .8f, -.8f
};

static const GLfloat frame_buffer_data[] = {
        -.9f, -.9f,
        -.93f,  .93f,
        .9f,  .9f,
        .93f, -.93f,

        -.88f, -.88f,
        -.84f,  .84f,
        .88f,  .88f,
        .84f, -.84f
};


static const int symbols_buffer_data_size[SYMBOLS+1] = {4, 4, 3, 14, 8};

static const GLfloat symbols_buffer_data[SYMBOLS+1][40] = {
        {//vertical line
                -.1f, -.6f,
                -.1f,  .6f,
                 .1f,  .6f,
                 .1f, -.6f
        },{//horizontal line
                -.6f, -.2f,
                -.6f,  .2f,
                 .6f,  .2f,
                 .6f, -.2f
        },{//triangle
                0.0f,  .6f,
                -.6f, -.6f,
                 .6f, -.6f
        },{//cross
                0.0f,  0.0f,
                .15f,   .15f,
                 .6f,   .15f,
                 .6f,  -.15f,
                .15f,  -.15f,
                .15f,  -.6f,
                -.15f, -.6f,
                -.15f, -.15f,
                 -.6f, -.15f,
                 -.6f,  .15f,
                -.15f,  .15f,
                -.15f,  .6f,
                 .15f,  .6f,
                 .15f,  .15f
        },{//hex
                0.0f, 0.0f,
                 .3f,  .6f,
                 .6f, 0.0f,
                 .3f, -.6f,
                -.3f, -.6f,
                -.6f, 0.0f,
                -.3f,  .6f,
                 .3f,  .6f
        }
};

static const GLfloat colors[] = {
        COLOR_L_GRAY,
        COLOR_L_RED,
        COLOR_L_GREEN,
        COLOR_L_YELLOW,
        COLOR_L_BLUE,
        COLOR_L_MAGENTA,
        COLOR_L_CYAN,
        COLOR_L_WHITE
};