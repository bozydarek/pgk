#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include <assert.h>

#include "game.h"
#include "render.h"
#include "cardModels.h"

using namespace std;

unsigned short selected_x, selected_y;

void man();

int main(int argc, char* argv[])
{
    Game::board_height = 4;
    Game::board_width = 4;
    Game::colors_on_board = COLORS;
    Game::symbols_on_board = SYMBOLS;

    if( argc == 3 || argc == 5 )
    {
        Game::board_height = (unsigned short) atoi(argv[1]);
        Game::board_width = (unsigned short) atoi(argv[2]);

        if(Game::board_height > 10 || Game::board_width > 20)
        {
            cout << "\033[1;31mError:\033[0m Incorrect parameters - Board is too height or too width\n";
            return 1;
        }
        
        if(argc == 5)
        {
            Game::colors_on_board = (unsigned short) atoi(argv[3]);
            Game::symbols_on_board = (unsigned short) atoi(argv[4]);

            assert( Game::colors_on_board  <= COLORS && "Colors number is too big" );
            assert( Game::symbols_on_board <= SYMBOLS && "Symbols number is too big" );
        }
    }
    else if( argc != 1)
    {
        cout << "\033[1;31mError:\033[0m Incorrect use\n";
        man();
        return 2;
    }

    Game::cards_ammount = Game::board_height * Game::board_width;
    assert( Game::cards_ammount % 2 == 0 && "Cards amount is not an even number!" );
    assert( Game::cards_ammount / 2 <= Game::colors_on_board * Game::symbols_on_board && "Cards amount is bigger then possible number of combinations!" );

    selected_x = selected_y = 0;
	
	srand(time(NULL));
	keys gameKey;

    Game::StartNew();
	
	int n = Render::Init();
	if( n )	return n;

	do
	{
		Render::DrawFrame();
		
		if( not Render::IsKeyPressed(GLFW_KEY_UP) )
        {
            gameKey.up = false;
		}
        else if( gameKey.up == false && selected_y < Game::board_height - 1 )
        {
            ++selected_y;
            gameKey.up = true;
        }

        if( not Render::IsKeyPressed(GLFW_KEY_DOWN) )
        {
            gameKey.down = false;
        }
        else if( gameKey.down == false && selected_y > 0 )
        {
            --selected_y;
            gameKey.down  = true;
        }

		if( not Render::IsKeyPressed(GLFW_KEY_LEFT) )
        {
            gameKey.left  = false;
        }
        else if( gameKey.left == false && selected_x > 0 )
        {
            --selected_x;
            gameKey.left  = true;
        }

        if( not Render::IsKeyPressed(GLFW_KEY_RIGHT) )
        {
            gameKey.right = false;
        }
        else if( gameKey.right == false && selected_x < Game::board_width - 1 )
        {
            ++selected_x;
            gameKey.right = true;
        }

		if( not Render::IsKeyPressed(GLFW_KEY_SPACE) )
        {
            gameKey.space = false;
        }
        else if( gameKey.space == false )
        {
            gameKey.space = true;

            if(Game::pairs_left == 0)
            {
                Game::StartNew();
            }
            else
            {
                if( Game::game_state == Game::READY_TO_START || Game::game_state == Game::IN_PROGRESS )
                {
                    Game::CardTest(Game::CardNumber(selected_x, selected_y));
                 }
                else if( Game::game_state == Game::FINISHED )
                {
                    Game::StartNew();
                }
            }
        }
		
		
	}while( not ( Render::IsKeyPressed(GLFW_KEY_ESCAPE ) || Render::IsWindowClosed() ) );

	Render::CleanUp();
	
	return 0;
}

void man()
{
    cout << "\033[1;36mMemory\033[0m - simple game made in OpenGL\n";
    cout << "\n\033[1mUsage:\033[0m\n";
    cout << "\t./Memory - for start program with default configuration\n";
    cout << "\t./Memory H W - to specific cards ammount in game befor start\n";
    cout << "\t./Memory H W C S - for start program with specific configuration\n";
    cout << "\n\033[1mFor example:\033[0m\n";
    cout << "\t./Memory 4 4 6 3\n";
}
