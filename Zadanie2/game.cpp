#include "game.h"

#include <iostream>
#include <algorithm>

#include "render.h"

Game::GameState Game::game_state = Game::NONE;
unsigned short Game::board_width, Game::board_height, Game::cards_ammount, Game::round, Game::pairs_left, Game::colors_on_board, Game::symbols_on_board;
std::vector<Card> Game::cards;
Card *Game::A = NULL, *Game::B = NULL;

void Game::StartNew ()
{
    cards.clear();

    pairs_left = (unsigned short) (cards_ammount / 2);
    for( unsigned int i = 0; i < pairs_left; ++i )
    {
        cards.push_back( Card(i) );
        cards.push_back( Card(i) );
    }
    std::random_shuffle(cards.begin(), cards.end());

    round = 0;
    game_state = READY_TO_START;

    std::cout << "Starting new game\n";
}

static bool RemoveTest(Card* &A, Card* &B)
{
    bool remove = false;

    if (A->model == B->model)
    {
        A->removed = true;
        B->removed = true;
        remove = true;
    }
    else
    {
        A->uncovered = true;
        B->uncovered = true;
    }

    A = B = NULL;

    return remove;
}

void Game::CardTest (int n)
{

    //std::cout << n << "-" <<cards[n].model << "\n";
    Card* card = &cards[n];
    if( card->removed )
    {
        if( A != NULL && B != NULL )
        {
            if(RemoveTest(A,B))
            {
                --pairs_left;
            }
        }

        return;
    }

    if( A == NULL )
    {
        A = card;
        A->uncovered = false;
        ++round;
        std::cout << "Round: " << round << "\n";

        //std::cout << "A:" << card->model << "\n";
    } 
    else if( B == NULL)
    {
        if( A == card )
        {
            return;
        }
        else
        {
            B = card;
            B->uncovered = false;
            ++round;
            std::cout << "Round: " << round << "\n";

            //std::cout << "B:" << card->model << "\n";
        }
    }
    else
    {
        if(RemoveTest(A,B))
        {
            --pairs_left;
        }

        CardTest(n);
    }
}

unsigned int Game::CardNumber(unsigned short x, unsigned short y)
{
    return y*board_width + x;
}
