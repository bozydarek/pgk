#ifndef RENDER_H
#define RENDER_H

#include <algorithm>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Render
{
private:
	Render() = delete; // static class
	static float px_size_x, px_size_y;
    static float card_width, card_height;

    static GLuint symbols_buffer[9], card_cover_buffer, color_cover_buffer, frame_buffer;
    static GLint uniform_scale, uniform_translate, uniform_color;

public:
	static int 		Init ();
	static void 	DrawFrame ();
	static void 	CleanUp ();

	static double 	GetTime ();
	static bool 	IsKeyPressed (int key);
	static bool 	IsWindowClosed ();

    static GLuint   VertexArrayID;
    static GLuint   shader_program_id;

    static void     DrawCoveredCard (unsigned short x, unsigned short y);
    static void     DrawUncoveredCard (unsigned short x, unsigned short y, int model);

    static void     DrawCardFrame (unsigned short x, unsigned short y);
    static void     DrawSymbolOnCard (unsigned short x, unsigned short y, int model);
};


#endif //RENDER_H
