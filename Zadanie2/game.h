#ifndef GAME_H
#define GAME_H

#include <vector>

struct keys{

	bool    up;
	bool    down;
	bool    left;
	bool    right;
	bool    space;
	
	keys() { up = down = left = right = space = false; };
};

class Card{
public:
    Card(int m) : model(m) {}
    int model;

    bool uncovered  = true;
    bool removed    = false;
};


class Game{
	Game() = delete;
public:
	typedef enum{
		NONE,
		READY_TO_START,
		IN_PROGRESS,
		FINISHED
	} GameState;

	static GameState 	    game_state;

    static unsigned short   board_width;
    static unsigned short   board_height;
	static unsigned short   colors_on_board;
	static unsigned short   symbols_on_board;

	static unsigned short   cards_ammount;

    static unsigned short   round;
    static unsigned short   pairs_left;

	static void 	        StartNew ();
	static void 	        CardTest (int n);

    static std::vector<Card> cards;
    
    static Card*            A;
    static Card*            B;

	static unsigned int		CardNumber(unsigned short x, unsigned short y);

};

#endif //GAME_H

