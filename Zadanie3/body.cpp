#include <glm/glm.hpp>
#include <iostream>
#include "body.h"

Body::Body(int size, const float *vert, float scala, glm::vec2 *pos)
    :position(pos)
{
    assert(size>1);

    glm::vec3 upVector(0.0f, 0.0f, 1.0f);

    if(size == 2)
    {
        glm::vec3 v1(vert[0],vert[1],0.0f);
        glm::vec3 v2(vert[2],vert[3],0.0f);
        glm::vec3 normal = glm::normalize(glm::cross(upVector,v1-v2));
        collision_side_vectors.push_back( std::make_pair( glm::vec2(v1)*scala, glm::vec2(normal) ) );
        //std::cout << normal.x << " " << normal.y << "\n";
    }
    else
    {
        for (int i = 0; i < size; ++i)
        {
            int next = (i+1) % size;
            glm::vec3 v1(vert[2*i],vert[2*i+1],0.0f);
            glm::vec3 v2(vert[2*next],vert[2*next+1],0.0f);
            glm::vec3 normal = glm::normalize(glm::cross(upVector,v1-v2));
            collision_side_vectors.push_back( std::make_pair( glm::vec2(v1)*scala, glm::vec2(normal) ) );
            //std::cout << normal.x << " " << normal.y << "\n";
        }
    }
}

Body::Body(std::initializer_list<float> vert, float scala, glm::vec2 *pos)
        :position(pos)
{
    glm::vec3 upVector(0.0f, 0.0f, 1.0f);
    auto vert_iter = vert.begin();
    //std::cout << "Start: " << *vert_iter <<"\n";

    if(vert.size() == 2)
    {
        // We have to split glm::vec3 v1( *(vert_iter++), *(vert_iter), 0.0f );
        // for this because mixing ++ with assignment is undefined behaviour.
        float tmp_v1 = *(vert_iter++);
        float tmp_v2 = *(vert_iter);
        glm::vec3 v1( tmp_v1, tmp_v2, 0.0f );

        // We have to take, first two again
        vert_iter = vert.begin();
        tmp_v1 = *(vert_iter++);
        tmp_v2 = *(vert_iter);
        glm::vec3 v2( tmp_v1, tmp_v2, 0.0f );

        glm::vec3 normal = glm::normalize(glm::cross(upVector,v1-v2));
        collision_side_vectors.push_back( std::make_pair( glm::vec2(v1)*scala, glm::vec2(normal) ) );
        //std::cout << "Vec:" << normal.x << " " << normal.y << "\n";
    }
    else
    {
        //std::cout << " ----- \n";
        for (unsigned int i = 0; i < vert.size()/2-1 ; ++i)
        {
            float tmp_v1 = *(vert_iter++);
            float tmp_v2 = *(vert_iter++);
            glm::vec3 v1( tmp_v1, tmp_v2, 0.0f );

            tmp_v1 = *(vert_iter++);
            tmp_v2 = *(vert_iter++);
            glm::vec3 v2( tmp_v1, tmp_v2, 0.0f );
            //std::cout  << "Vec:"  << v1.x << " " << v1.y << "\n";

            //We have to step back
            (*vert_iter--);
            (*vert_iter--);

            glm::vec3 normal = glm::normalize( glm::cross(upVector,v1-v2) );
            collision_side_vectors.push_back( std::make_pair( glm::vec2(v1)*scala, glm::vec2(normal) ) );
            //std::cout << normal.x << " " << normal.y << "\n";
        }

        float tmp_v1 = *(vert_iter++);
        float tmp_v2 = *vert_iter;
        glm::vec3 v1( tmp_v1, tmp_v2, 0.0f );

        // We have to take, first two again
        vert_iter = vert.begin();
        tmp_v1 = *(vert_iter++);
        tmp_v2 = *vert_iter;
        glm::vec3 v2( tmp_v1, tmp_v2, 0.0f );

        glm::vec3 normal = glm::normalize( glm::cross(upVector,v1-v2) );
        collision_side_vectors.push_back( std::make_pair( glm::vec2(v1)*scala, glm::vec2(normal) ) );
        //std::cout << normal.x << " " << normal.y << "\n";
    }
}

Body::Body(glm::vec2 vert1, glm::vec2 vert2, float scala, glm::vec2 *pos)
        :position(pos)
{
    glm::vec3 upVector(0.0f, 0.0f, 1.0f);

    glm::vec3 v1(vert1,0.0f);
    glm::vec3 v2(vert2,0.0f);
    glm::vec3 normal = glm::normalize(glm::cross(upVector,v2-v1));
    collision_side_vectors.push_back( std::make_pair( glm::vec2(v1)*scala, glm::vec2(normal) ) );

    std::cout << "New line for physic: " << normal.x << " " << normal.y << "\n";
}