#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <glm/glm.hpp>
#include <vector>
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>


#define rgb(x,y,z)	x/255.0, y/255.0, z/255.0, 1.0
#define SQRT3 1.73205080757

class Model;

// Class for every object that can be draw on screen
class Drawable
{
public:

    Drawable(std::string id, Model* model_vbo, float model_scale = 1.0, glm::vec2 pos = glm::vec2(0.0,0.0), float model_angle = 0.0, unsigned short variant = 0);
    ~Drawable();

    //name of object (useful for debug)
    std::string     object_id;

    void            Draw();
    void            PlayAnimation(double len);

    // Get'ers for object properties
    glm::vec2       getPosition() { return  position; }
    float           getScale() { return  scale; }
    float           getAngle() { return  angle; }

    glm::vec2       getVerticle(int i);

    // Set'ers for object properties
    void            setPosition( glm::vec2 pos ) { position = pos; }
    void            setXPosition( float xpos ) { position.x = xpos; }
    void            setScale( float scal ) { scale = scal; }
    void            setAngle( float ang ) { angle = ang; }

    void            move( glm::vec2 pos ) { position += pos; }

    // Hack, I have no time to make it beter
    glm::vec2*       getPtrToPosition() { return  &position; }
private:
    friend class    Render;

	// Pointer to the model
    Model*          model;
    
    // Add pointer to all drawable elements to this vector for easy access to them
    static std::vector<Drawable*> listOfAll;

    float           scale;
    glm::vec2       position;
    float           angle;
    
    unsigned short  color_version;
	
    // Animation state for disappears blocks
    unsigned int    animate_mode = 0;
    double          animate_start;
    double          animate_length;
    
};


#endif //DRAWABLE_H
