#include "render.h"
#include "shader.h"

#include <iostream>
#include <zconf.h>
#include "game.h"
#include "drawable.h"


#define RGB(r,g,b) r/255.0f, g/255.0f, b/255.0f

#define WIDTH 1024
#define HEIGHT 1024

#define BACKGROUND_COLOR RGB (225, 225, 225)

GLFWwindow* window;

// Static members of Render class
GLint Render::uniform_scale, Render::uniform_translate, Render::uniform_angle, Render::uniform_animation_mode,  Render::uniform_animation_phase;
GLuint Render::VertexArrayID;
GLuint Render::shader_program_id;


double Render::GetTime()
{
	return glfwGetTime();
}

bool Render::IsKeyPressed(int key)
{
	return glfwGetKey(window, key) == GLFW_PRESS;
}

bool Render::IsWindowClosed()
{
	return glfwWindowShouldClose(window) == 1;
}


int static uniformCheck(GLint uniform, std::string name) {

    if (uniform == -1)
    {
        std::cerr << "A " << name << " uniform is missing from the shader." << std::endl;
        glfwTerminate();
        return 1;
    }

    return 0;
}

// GLFW initialization
int Render::Init()
{
	if( !glfwInit() )
	{
		std::cerr << "Failed to initialize GLFW." << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	window = glfwCreateWindow( WIDTH, HEIGHT, "Arkanoid", NULL, NULL);

	if( window == NULL )
	{
		std::cerr << "Failed to open GLFW window." << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glewExperimental = (GLboolean) true;
	if (glewInit() != GLEW_OK)
	{
        std::cerr << "Failed to initialize GLEW\n";
		glfwTerminate();
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glClearColor(BACKGROUND_COLOR, 0.0f);

    shader_program_id = LoadShaders( "vert.vertexshader", "color.fragmentshader" );
    glUseProgram(shader_program_id);

//------------------------| Uniforms |------------------------

    uniform_scale = glGetUniformLocation(shader_program_id, "scale");
    uniform_translate = glGetUniformLocation(shader_program_id, "translate");
	uniform_angle = glGetUniformLocation(shader_program_id,"angle");
	uniform_animation_mode = glGetUniformLocation(shader_program_id,"animation_mode");
	uniform_animation_phase = glGetUniformLocation(shader_program_id,"animation_phase");

    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    if( uniformCheck(uniform_scale, "SCALE") ||
        uniformCheck(uniform_translate, "TRANSLATE") ||
        uniformCheck(uniform_angle, "ANGLE") ||
        uniformCheck(uniform_animation_mode, "animation_mode") ||
        uniformCheck(uniform_animation_phase, "animation_phase") )
        return -1;

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_ALWAYS);

	// Enable alpha effects
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//---------------------------------------------------------------

    //card_width = 1.0f / Game::board_width;
    //card_height = 1.0f / Game::board_height;
   
    //std::cout << "!CW " << Game::board_width << " " << card_width  <<"\n";
 
    //glUniform2f(uniform_scale, card_width, card_height);

	return 0;
}

std::vector<Drawable*> to_erease;

void Render::DrawFrame()
{
	glClear( GL_COLOR_BUFFER_BIT );

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

    glUseProgram(shader_program_id);


	for(auto d : Drawable::listOfAll)
	{
		//std::cout << d->object_id << "\n";

		glUniform2f( uniform_translate, d->getPosition().x, d->getPosition().y );
		glUniform1f( uniform_scale, d->getScale() );
		glUniform1f( uniform_angle, d->getAngle()*2.0f*(float)M_PI );
		glUniform1i( uniform_animation_mode, d->animate_mode );

        //std::cout << d->getPosition().x << " " << d->getPosition().y << "\n";
        //std::cout << d->getScale() << " " << d->getAngle()*2.0*M_PI << "\n";

		if(d->animate_mode != 0)
		{
			float anim_phase = (float)((GetTime() - d->animate_start)/d->animate_length);

			//std::cout << "Anim: " <<anim_phase << "\n";

			glUniform1f(uniform_animation_phase, anim_phase);
			if(anim_phase >= 1.0)
			{
				d->animate_mode = 0;
				to_erease.push_back(d);
				continue; // we don't want to add new if before d->Draw()  to check every time
			}
		}

		d->Draw();

	}

	if(to_erease.size())
	{// we wouldn't do it often
		for (auto e : to_erease)
		{
			//std::cout << "Delete block\n";
			auto it = std::find(Drawable::listOfAll.begin(), Drawable::listOfAll.end(), e);
			if (it != Drawable::listOfAll.end()) {
				Drawable::listOfAll.erase(it);
			}
		}
		to_erease.clear();
	}

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

	glUseProgram(0);

	glfwSwapBuffers(window);
	glfwPollEvents();
}

void Render::CleanUp()
{
	glDeleteVertexArrays(1, &VertexArrayID);
	glDeleteProgram(shader_program_id);

	glfwTerminate();
}



