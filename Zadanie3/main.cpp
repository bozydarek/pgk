#include <iostream>
#include <unistd.h>
#include <assert.h>

#include "render.h"
#include "game.h"
#include "background.h"
#include "board.h"
#include "paddle.h"
#include "ball.h"
#include "bricks.h"

using namespace std;

// uncomment to have fps counter in terminal
// imho useless when vsync is activated
//#define FPS_COUNTER

int main()
{
    std::cout << "Starting preparing the game\n";
    srand((unsigned int) time(NULL));

    int n = Render::Init();
    if( n ) return n;

    Background bg(8,12);
    BoardFrame board;
    Paddle paddle;
    float paddle_speed = .5f;
    Ball ball(glm::vec2(0.0f, -.3), .5f);

    Bricks bricks(8, 6);

	keys gameKeys;

    Game::New();

    double lastFrameTime = Render::GetTime();

#ifdef FPS_COUNTER
    double lastTimeForFps = Render::GetTime();
    int nbFrames = 0;
#endif

    float ang = 0.0;
    std::vector<std::shared_ptr<Body>> to_erease;

    std::cout << "Game is ready\n";

    do
    {
        double newtime = Render::GetTime();

        // Calculate time for physic calculations
        double time_delta = newtime-lastFrameTime;
        lastFrameTime = newtime;

    #ifdef FPS_COUNTER
        //FPS counter
        nbFrames++;
        if ( newtime - lastTimeForFps >= 1.0 )
        {
            cout << "FPS: " << nbFrames << "\n";
            cout << 1000.0/double(nbFrames) << " ms/frame\n";
            nbFrames = 0;
            lastTimeForFps += 1.0;
        }
    #endif

        if(Game::state == Game::IN_PROGRESS)
        {
            // for demo
            ball.ball_draw->setAngle(ang);
            ang+=(0.5* time_delta);

            // Calculate physics
            //PhysicsEngine::makeIteration(time_delta);

            ball.update((float) time_delta);

            float delta_x = 0.0f;
            if(Render::IsKeyPressed(GLFW_KEY_RIGHT))
                delta_x = 1.0f;

            if(Render::IsKeyPressed(GLFW_KEY_LEFT))
                delta_x = -1.0f;

            float newpos = paddle.paddle_draw->getPosition().x + delta_x * paddle_speed * (float)time_delta;
            newpos = glm::clamp(newpos, -1.0f+paddle.getLength(), 1.0f-paddle.getLength());
            paddle.paddle_draw->setXPosition(newpos);


            if(Game::collision(ball, *paddle.paddle_physic_body))
                Game::paddleCollision(ball, paddle);

            for(auto br : bricks.all_bricks_physic_bodies)
            {
                if(Game::collision(ball, *br))
                {
                    bricks.removeBrick(*(br->position));
                    to_erease.push_back(br);
                }
            }

            if(to_erease.size())
            {
                for (auto e : to_erease)
                {
                    //std::cout << "Delete physic block\n";
                    auto it = std::find(bricks.all_bricks_physic_bodies.begin(), bricks.all_bricks_physic_bodies.end(), e);
                    if (it != bricks.all_bricks_physic_bodies.end())
                    {
                        //std::cout << "Pos " <<e->position->x << " " <<e->position->y << " "<< "\n";
                        bricks.all_bricks_physic_bodies.erase(it);
                    }
                }
                to_erease.clear();
            }


            for(auto wall : board.all_walls_physic_bodies)
            {
                Game::collision(ball, *wall);
            }

            if(Game::collision(ball, *board.dead_zone))
            {
                Game::lifeLoss(ball);
            }

            if(Render::IsKeyPressed(GLFW_KEY_SPACE) && not gameKeys.space)
            {
                Game::Pause();
                gameKeys.space = true;
            }
            else if (not Render::IsKeyPressed(GLFW_KEY_SPACE))
            {
                gameKeys.space = false;
            }
            if(Render::IsKeyPressed(GLFW_KEY_N)){} // with out this, when press N while playing, game will reset after pause

        }
        else if(Game::state == Game::READY_TO_START)
        {
            // Use arrows or space to start the game
            if(Render::IsKeyPressed(GLFW_KEY_RIGHT) || Render::IsKeyPressed(GLFW_KEY_LEFT) || Render::IsKeyPressed(GLFW_KEY_DOWN) || Render::IsKeyPressed(GLFW_KEY_UP) || Render::IsKeyPressed(GLFW_KEY_SPACE))
                Game::Start();
        }
        else // Game is not in state READY_TO_START or IN_PROGRESS -> game state is PAUSE or FINISHED
        {
            if(Render::IsKeyPressed(GLFW_KEY_N))
            {
                Game::New();
                ball.reset();
                bricks.reset();
            }
            
            if(Render::IsKeyPressed(GLFW_KEY_SPACE) && not gameKeys.space)
            {
                Game::Start();
                gameKeys.space = true;
            }
            else if (not Render::IsKeyPressed(GLFW_KEY_SPACE))
            {
                gameKeys.space = false;
            }
        }

        // Draw a single frame
        Render::DrawFrame();

    }while( not ( Render::IsKeyPressed(GLFW_KEY_ESCAPE) || Render::IsWindowClosed() ) );

    Render::CleanUp();

    return 0;
}
