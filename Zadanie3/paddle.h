#ifndef PADDLE_H
#define PADDLE_H

#include <memory>
#include "drawable.h"

class Body;

class Paddle {
public:
    Paddle();

private:

    const float length = .6;
    const float height = .1;
    // Class responsible for a single board element
    class PaddleDraw: public Drawable
    {
    public:
        //BoardDraw(Model* vbo_model, float scale, glm::vec2 pos);
        PaddleDraw(Model *vbo_model, glm::vec2 pos);
    };

public:
    std::shared_ptr<Body>       paddle_physic_body;
    std::shared_ptr<PaddleDraw> paddle_draw;

    float getLength(){ return length; }
    float getHeight(){ return height; }
};


#endif //PADDLE_H
