#include <iostream>
#include "game.h"
#include "body.h"
#include "ball.h"

//static members of class Game
Game::GameState Game::state;
unsigned short int Game::lifes = 3;
std::vector<std::shared_ptr<Ball>> Game::lives_pic;

void Game::Pause()
{
    state = Game::PAUSE;
}

void Game::New()
{
    lives_pic.clear();
    for (int i=0; i<3; ++i)
    {
        auto one = std::make_shared<Ball>(glm::vec2(-.85f + i*.2f, .85f));
        one->ball_draw->setAngle(0.2);
        lives_pic.push_back(one);
    }

    lifes = 3;
    state = Game::READY_TO_START;
}

void Game::Start()
{
    if( lifes < 1 ) New();

    state = Game::IN_PROGRESS;
}

std::ostream & operator<<(std::ostream & out,const  glm::vec2 & v)
{
    return out<<"["<<v.x<<","<<v.y<<"]";
}

bool Game::collision(Ball &ball, Body &body)
{
    glm::vec2 body_pos = *body.position;
    glm::vec2 ball_pos = ball.ball_draw->getPosition();
    float ball_radius = ball.ball_scale;

    glm::vec2 center = ball_pos - body_pos;

    float separation = glm::dot(body.collision_side_vectors[0].second, (center - body.collision_side_vectors[0].first));

    float tmp_separation = separation;
    glm::vec2 normal_vec(body.collision_side_vectors[0].second);

    //std::cout << "\t\nNew\n"<< separation << "\n";

    if(separation > ball_radius)
    {
        return false;
    }
    
    if(body.collision_side_vectors.size() == 1)
    {
        //std::cout<<"Collision with wall\n";
        ball.newVelocity(normal_vec);
        return true;
    }
    int tmp = 0; 
    for (unsigned  int i = 1; i < body.collision_side_vectors.size(); ++i)
    {
        separation = glm::dot(body.collision_side_vectors[i].second, (center - body.collision_side_vectors[i].first));

        //std::cout << body.collision_side_vectors[i].second << " " << separation << "\n";

        if(separation > ball_radius)
        {
            return false;
        }

        if(separation > tmp_separation)
        {
            tmp_separation = separation;
            normal_vec = body.collision_side_vectors[i].second;
            tmp = i;
        }
    }

    //std::cout << "Kontakt\n";
    if(tmp_separation < 0)
    {
        ball.newVelocity(normal_vec, not body.isPaddle);
        return true;
    }
    
    glm::vec2 v1 = body.collision_side_vectors[tmp].first;
    glm::vec2 v2 = body.collision_side_vectors[(tmp+1)%body.collision_side_vectors.size()].first;

    //std::cout<<"v1: "<<v1<<"\n";
    //std::cout<<"v2: "<<v2<<"\n";

    float dot1 = glm::dot(center - v1, v1 - v2);
    float dot2 = glm::dot(center - v2, v2 - v1);

    if( dot1 > 0 || dot2 > 0 )
    {
        glm::vec2 closest_vec = dot1 > 0 ? v1 : v2;
        if( glm::distance(center, closest_vec) > ball_radius )
        {
            return false;
        }          
        normal_vec = glm::normalize (center - closest_vec);
    }
    
    ball.newVelocity(normal_vec, not body.isPaddle);
    return true;
}

// for make game more interesting
void Game::paddleCollision(Ball &ball, Paddle &paddle)
{
    glm::vec2 ball_pos = ball.ball_draw->getPosition();
    glm::vec2 padd_pos = paddle.paddle_draw->getPosition();

    if(ball_pos.y > padd_pos.y)
    {
        //std::cout << ball_pos.x - padd_pos.x << "\n";
        ball.changeVelocityX(3.0f*(ball_pos.x - padd_pos.x));
    }
}

void Game::lifeLoss(Ball &ball)
{
    assert(lifes > 0);

    if((--lifes) == 0)
    {
        state = Game::FINISHED;
    }
    else
    {
        state = Game::READY_TO_START;
    }
    lives_pic.erase(lives_pic.end() - 1);
    ball.reset();

}
