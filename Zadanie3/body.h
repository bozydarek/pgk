#ifndef BODY_H
#define BODY_H

#include <memory>
#include <glm/vec2.hpp>
#include <vector>

class Body
{
public:
    Body(int size, const float * vert, float scala, glm::vec2* pos);
    Body(std::initializer_list<float> vert, float scala, glm::vec2* pos);
    Body(glm::vec2 vert1, glm::vec2 vert2, float scala, glm::vec2 *pos);

    std::vector< std::pair<glm::vec2, glm::vec2> > collision_side_vectors;
    glm::vec2* position;

    bool isPaddle = false;
    void setIsPaddle(bool is) { isPaddle = is; }
};


#endif //BODY_H
