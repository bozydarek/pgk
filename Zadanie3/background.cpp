#include <cstdlib>
#include <iostream>
#include "background.h"
#include "render.h"
#include "model.h"


std::vector<float> prepare_vertices(){
    std::vector<float> points(2*(7+1));//1 - in center and 7 - on the perimeter (one is doubled)

    points[0] = points[1] = 0.0;

    for(int i = 1; i <= 7; ++i)
    {
        points[i*2 + 0] = (float)cos(i*2.0*M_PI/(float)6);
        points[i*2 + 1] = (float)sin(i * 2.0 * M_PI / (float)6);
    }
    return points;
}

const float center_colors[4] = { rgb(60, 130, 250) };

const float triangle_colors[8] =
{
        rgb(50, 90, 220),
        rgb(20, 40, 80),
};

std::vector<float> prepare_colors()
{
    std::vector<float> colors(4*(7+1));//1 - in center and 7 - on the perimeter (one is doubled)

    for (int i = 0; i < 4; ++i)
    {
        colors[i] = center_colors[i];
    }


    for(int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            colors[4+i*8 + j] = triangle_colors[j];
        }
    }

    for (int j = 0; j < 4; j++)
    {
        colors[4+3*8 + j] = triangle_colors[j];
    }

    return colors;
}


std::vector<float> prepare_vertices_for_line(){
    std::vector<float> points(2*6);

    for(int i = 0; i < 6; ++i)
    {
        points[i*2] = (float)cos(i*2.0*M_PI/(float)6);
        points[i*2 + 1] = (float)sin(i * 2.0 * M_PI / (float)6);
    }
    return points;
}

float line_colors[4] = {rgb(0, 10, 60) };

std::vector<float> prepare_colors_for_line()
{
    std::vector<float> colors(4*6);

    for(int i = 0; i < 6; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            colors[i*4+j] = line_colors[j];
        }
    }
    return colors;
}

Background::Background(int w, int h)
{
    const float scale = 0.1;

    Model *v_mod = new Model(GL_TRIANGLE_FAN, 8 , prepare_vertices().data(), prepare_colors().data());
    Model *v_mod_line = new Model(GL_LINE_LOOP, 6, prepare_vertices_for_line().data(), prepare_colors_for_line().data());

    for (int y = -h; y < h; ++y)
    {
        int par = y & 1;
        float x_offset = scale*par*1.5f;

        for (int x = -w / 2; x <= w / 2 - par; ++x)
        {
            // x_offset is responsible for the scattering sideways
            // 3 because width of one hex is 2 and we need to add half on next hex

            // height of the equilateral triangle (side length 1) is sqrt3/2 so height of hex is SQRT3,
            // but we need only half of that because next hex is find out about half the height at the top
            glm::vec2 pos(3.0f*x*scale + x_offset, y*SQRT3*0.5f*scale);

            auto piece = std::make_shared<BackgroundDraw>(v_mod, scale, pos);
            bg_pieces.push_back(piece);

            auto lines = std::make_shared<BackgroundDraw>(v_mod_line, scale, pos);
            bg_pieces.push_back(lines);
        }
    }
}

Background::BackgroundDraw::BackgroundDraw(Model* vbo_model, float scale, glm::vec2 pos)
        : Drawable("Background", vbo_model, scale, pos)
{

}
