#include <iostream>
#include "drawable.h"
#include "render.h"
#include "model.h"

std::vector<Drawable*> Drawable::listOfAll;

Drawable::Drawable(std::string id, Model* model_vbo, float model_scale, glm::vec2 pos, float model_angle, unsigned short v)
        : object_id(id)
        , model(model_vbo)
        , scale(model_scale)
        , position(pos)
        , angle(model_angle)
        , color_version(v)
        , animate_mode(0)

{
    //std::cout << "New object - " << id << "\n" << scale << " " <<  pos.x << " " <<  pos.y << "\n" ;
    Drawable::listOfAll.emplace_back(this);
}


Drawable::~Drawable()
{
    auto p = std::find(listOfAll.begin(), listOfAll.end(), this);
    if (p != listOfAll.end())
    {
        listOfAll.erase(p);
    }
    //std::cout << "Delete Drawabble object\n";
}

void Drawable::PlayAnimation(double len)
{
    animate_mode = 1;
    animate_length = len;
    animate_start = Render::GetTime();
}

void Drawable::Draw()
{
    glBindBuffer(GL_ARRAY_BUFFER, model->vertex_buffer);
    glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0 );
    glBindBuffer(GL_ARRAY_BUFFER, model->color_buffer[color_version]);
    glVertexAttribPointer( 1, 4, GL_FLOAT, GL_FALSE, 0, (void*)0 );

    glDrawArrays(model->getMode(), 0, model->getVertices());
}


glm::vec2 Drawable::getVerticle(int i)
{
    return  model->getVert(i);
}