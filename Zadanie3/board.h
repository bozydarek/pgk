#ifndef BOARD_H
#define BOARD_H


#include <memory>
#include "drawable.h"

class Body;

// Class for this irritating brick_frame on the screen.
class BoardFrame
{
public:
    BoardFrame();

    // Class responsible for a single board element
    class BoardDraw: public Drawable
    {
    public:
        //BoardDraw(Model* vbo_model, float scale, glm::vec2 pos);
        BoardDraw(Model *vbo_model, glm::vec2 pos);
    };

    std::shared_ptr<BoardDraw>              board_pieces;
    std::vector< std::shared_ptr<Body> >    all_walls_physic_bodies;
    std::shared_ptr<Body>                   dead_zone;
};


#endif //BOARD_H
