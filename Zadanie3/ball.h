#ifndef BALL_H
#define BALL_H

#include <memory>
#include "drawable.h"

//Class for Ball
class Ball
{
    float       ball_speed;
    float       start_speed;
    glm::vec2   start_position;
	glm::vec2   velocity = glm::vec2(0.099f,-1.0f);

public:
    Ball(glm::vec2 start_pos, float speed = 0.0f);
    // Because before scaling ball radius is 1.0f, so after is equal to ball_scale
    static float ball_scale;

    // Class responsible for draw a ball
    class BallDraw: public Drawable
    {
    public:
        BallDraw(Model *vbo_model, glm::vec2 pos);
    };

	void update(float delta);
    void newVelocity(glm::vec2 collision_vector, bool random = true);
    void changeVelocityX(float x_change);

    std::shared_ptr<BallDraw> ball_draw;

    void reset();
};




#endif //BALL_H
