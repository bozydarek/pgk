#ifndef GAME_H
#define GAME_H

#include "ball.h"
#include "paddle.h"
#include "body.h"

struct keys{

//	bool    up;
//	bool    down;
//	bool    left;
//	bool    right;
	bool    space;
	
	keys() { /*up = down = left = right =*/ space = false; };
};

class Game{
    Game() = delete;

	static std::vector <std::shared_ptr<Ball>> lives_pic;
public:
    typedef enum{
        NONE,
        READY_TO_START,
        IN_PROGRESS,
        PAUSE,
        FINISHED
    } GameState;

    static GameState 	    state;


	static unsigned short   lifes;
    static unsigned short   bricks_amount;

    static void 	        New();
    static void             Start();
    static void             Pause();
	static bool 			collision(Ball &ball, Body &body);
	static void 			paddleCollision(Ball &ball, Paddle &paddle);

	static void 			lifeLoss(Ball &ball);
};

#endif //GAME_H
