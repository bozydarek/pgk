#ifndef RENDER_H
#define RENDER_H

#include <algorithm>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Render
{
private:
	Render() = delete; // static class
    static GLint uniform_scale, uniform_translate, uniform_angle, uniform_animation_mode, uniform_animation_phase;

public:
	static int 		Init ();
	static void 	DrawFrame ();
	static void 	CleanUp ();

	static double 	GetTime ();
	static bool 	IsKeyPressed (int key);
	static bool 	IsWindowClosed ();

    static GLuint   VertexArrayID;
    static GLuint   shader_program_id;

};


#endif //RENDER_H
