#ifndef BRICK_H
#define BRICK_H

#include <memory>
#include "drawable.h"
#include "body.h"

class Bricks {
    int width;
    int height;
public:
    Bricks(int w, int h);
    static const float brick_scale;
    static const float y_shift;

    // Class responsible for a single brick
    class BrickDraw: public Drawable
    {
    public:
        BrickDraw(Model *vbo_model, float scale, glm::vec2 pos, unsigned short version);
    };

    // Vector of all bricks
    std::vector <std::shared_ptr<BrickDraw>> all_bricks;
    std::vector <std::shared_ptr<Body>>      all_bricks_physic_bodies;

    void AddBrick(Model *frame_model, Model *model, int i, int j, int shift = 0);
    void removeBrick(glm::vec2 pos);
    void reset();
    void setStandardBricks(int w);
};


#endif //BRICK_H
