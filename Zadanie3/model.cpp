#include <iostream>
#include "model.h"

Model::Model(GLenum mode_, unsigned int size, std::initializer_list<float> vert,
             std::initializer_list<std::initializer_list<float>> color)
    : mode(mode_)
    , vertices(size)
{
    all_color_versions = color.size();

    vert_data = new float[vertices*2];
    colors_data = new float*[all_color_versions];
    for(unsigned int i = 0; i < all_color_versions; ++i)
        colors_data[i] = new float[vertices*4];


    auto vert_iter = vert.begin();
    auto color_iter = color.begin();

    for(unsigned int i = 0; i < vertices*2; i++)
        vert_data[i] = *(vert_iter++);

    for(unsigned int v = 0; v < all_color_versions; v++)
    {
        auto color_iter2 = (*(color_iter++)).begin();
        for(unsigned int i = 0; i < vertices*4; i++)
            colors_data[v][i] = *(color_iter2++);
    }
    
    buffers_initialization();
}

Model::Model(GLenum mode_, unsigned int size, std::initializer_list<float> vert, std::initializer_list<float> color)
        : mode(mode_)
        , vertices(size)
{
    all_color_versions = 1;

    vert_data = new float[vertices*2];
    colors_data = new float*[1];
    colors_data[0] = new float[vertices*4];

    auto vert_iter = vert.begin();
    auto color_iter = color.begin();

    for(unsigned int i = 0; i < vertices*2; i++)
        vert_data[i] = *(vert_iter++);

    for(unsigned int i = 0; i < vertices*4; i++)
        colors_data[0][i] = *(color_iter++);
        
    buffers_initialization();
}

Model::Model(GLenum mode_, unsigned int size, const float *vert, const float *color)
        : mode(mode_)
        , vertices(size)
{
    all_color_versions = 1;

    vert_data = new float[vertices*2];
    colors_data = new float*[1];
    colors_data[0] = new float[vertices*4];

    for(unsigned int i = 0; i < vertices*2; i++)
        vert_data[i] = vert[i];

    for(unsigned int i = 0; i < vertices*4; i++)
        colors_data[0][i] = color[i];
        
    buffers_initialization();
}


Model::~Model()
{
    glDeleteBuffers(1, &vertex_buffer);
    glDeleteBuffers(all_color_versions, color_buffer);
}

void Model::buffers_initialization()
{
    color_buffer = new GLuint[all_color_versions];
    glGenBuffers(1, &vertex_buffer);
    glGenBuffers(all_color_versions, color_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertices*2, vert_data, GL_STATIC_DRAW);

    for(unsigned int i = 0; i < all_color_versions; i++)
    {
        glBindBuffer(GL_ARRAY_BUFFER, color_buffer[i]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertices*4, colors_data[i], GL_STATIC_DRAW);
    }
}

glm::vec2 Model::getVert(int i)
{
    assert(i>=0 && i<(int)vertices);
    return glm::vec2(vert_data[2*i], vert_data[2*i+1]);
}
