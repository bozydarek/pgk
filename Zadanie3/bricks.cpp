#include "bricks.h"

#include <iostream>
#include "render.h"
#include "model.h"

std::initializer_list<float> vertices_for_brick_frame
{
        1.0f,  .5f,
        -1.0f, .5f,
        -1.0f, -.5f,
        1.0f,  -.5f,
};

std::initializer_list<std::initializer_list<float>> colors_for_bricks_frame
{
        {   rgb( 80, 80, 80),
            rgb( 80, 80, 80),
            rgb( 80, 80, 80),
            rgb( 80, 80, 80)},
        {   rgb( 160, 80, 80),
            rgb( 160, 80, 80),
            rgb( 160, 80, 80),
            rgb( 160, 80, 80)},
        {   rgb( 80, 160, 80),
            rgb( 80, 160, 80),
            rgb( 80, 160, 80),
            rgb( 80, 160, 80)},
        {   rgb( 80, 80, 160),
            rgb( 80, 80, 160),
            rgb( 80, 80, 160),
            rgb( 80, 80, 160)},
        {   rgb( 160, 160, 80),
            rgb( 160, 160, 80),
            rgb( 160, 160, 80),
            rgb( 160, 160, 80)},
        {   rgb( 160, 80, 160),
            rgb( 160, 80, 160),
            rgb( 160, 80, 160),
            rgb( 160, 80, 160)}
};

std::initializer_list<float> vertices_for_brick
{
        0.0f, 0.0f,
         .9f,  .4f,
        -.9f,  .4f,
        -.9f, -.4f,
         .9f, -.4f,
         .9f,  .4f,
};

std::initializer_list<std::initializer_list<float>> colors_for_bricks
{
        {   rgb( 160, 160, 160),
            rgb( 100, 100, 100),
            rgb( 100, 100, 100),
            rgb( 100, 100, 100),
            rgb( 100, 100, 100),
            rgb( 100, 100, 100)},

        {   rgb( 200, 200, 200),
            rgb( 200, 100, 100),
            rgb( 200, 100, 100),
            rgb( 200, 100, 100),
            rgb( 200, 100, 100),
            rgb( 200, 100, 100)},

        {   rgb( 200, 200, 200),
            rgb( 100, 200, 100),
            rgb( 100, 200, 100),
            rgb( 100, 200, 100),
            rgb( 100, 200, 100),
            rgb( 100, 200, 100)},

        {   rgb( 200, 200, 200),
            rgb( 100, 100, 200),
            rgb( 100, 100, 200),
            rgb( 100, 100, 200),
            rgb( 100, 100, 200),
            rgb( 100, 100, 200)},

        {   rgb( 250, 250, 200),
            rgb( 200, 200, 100),
            rgb( 200, 200, 100),
            rgb( 200, 200, 100),
            rgb( 200, 200, 100),
            rgb( 200, 200, 100)},

        {   rgb( 250, 200, 250),
            rgb( 200, 100, 200),
            rgb( 200, 100, 200),
            rgb( 200, 100, 200),
            rgb( 200, 100, 200),
            rgb( 200, 100, 200)}
};

const float Bricks::brick_scale = 0.1;
const float Bricks::y_shift = 0.1;

Bricks::Bricks(int w, int h)
    :width(w)
    ,height(h)
{

    if(width<height) // it not necessary
    {
        height = width;
    }

    setStandardBricks(width);
}

void Bricks::setStandardBricks(int w)
{

    Model *frame_model = new Model(GL_TRIANGLE_FAN, 4, vertices_for_brick_frame, colors_for_bricks_frame);
    Model *model = new Model(GL_TRIANGLE_FAN, 6, vertices_for_brick, colors_for_bricks);

    for (int j = 0; j < height; ++j)
    {
        if(w%2)
        {
            for (int i = -w / 2; i <= w / 2; ++i)
            {
                AddBrick(frame_model, model, i, j);
            }
        }
        else
        {
            for (int i = -w / 2; i < w / 2; ++i)
            {
                AddBrick(frame_model, model, i, j, 1);
            }
        }
        --w;
    }
}

void Bricks::AddBrick(Model *frame_model, Model *model, int i, int j, int shift)
{
    short color = (short) (random() % 6); // or we can use (short) (j % 6);
    glm::vec2 pos(-(2 * brick_scale) * i - brick_scale * shift, j * (brick_scale) + y_shift);
    
    auto testf = std::make_shared<BrickDraw>(frame_model, brick_scale, pos, color);
    all_bricks.push_back(testf);
    
    auto test = std::make_shared<BrickDraw>(model, brick_scale, pos, color);
    all_bricks.push_back(test);

    auto brick = std::make_shared<Body>(vertices_for_brick_frame, brick_scale, testf->getPtrToPosition());
    all_bricks_physic_bodies.push_back(brick);
}

Bricks::BrickDraw::BrickDraw(Model *vbo_model, float scale, glm::vec2 pos, unsigned short version)
    :Drawable("Brick", vbo_model, scale, pos, 0.0, version)
{

}


void Bricks::removeBrick(glm::vec2 pos)
{
    // block and brick_frame - that's two elements
    double h = (pos.y - y_shift) * 1/brick_scale; // that should always be integer
    int hh = (int)h;
    unsigned int sum = 0;
    //skip bricks on previous levels
    for(int i=0;hh>0;--hh,++i)
    {
        sum+=width-i;
    }

    for (unsigned int i = sum; i < all_bricks.size(); ++i)
    {
        if(all_bricks[2*i]->getPosition() == pos)
        {
            all_bricks[2 * i]->PlayAnimation(2.0);
            all_bricks[2 * i + 1]->PlayAnimation(2.0);
            return;
        }
    }

    assert(false);
}

void Bricks::reset()
{
    all_bricks.clear();
    all_bricks_physic_bodies.clear();
    setStandardBricks(width);
}
