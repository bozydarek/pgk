#include "ball.h"

#include <iostream>
#include "render.h"
#include "model.h"

#define BALL_ITER 25

std::vector<float> prepare_vertices_for_ball()
{
    std::vector<float> points(2*(BALL_ITER+1+1));//1 - for center, BALL_ITER+1 for every vertices in BALL_ITER-side (1 doubled)

    points[0] = points[1] = 0.0f;

    for(int i = 1; i <= BALL_ITER+1; ++i)
    {
        points[i*2 + 0] = (float)cos(i*2.0*M_PI/(float)BALL_ITER);
        points[i*2 + 1] = (float)sin(i * 2.0 * M_PI / (float)BALL_ITER);
    }
    return points;
}

float ball_colors[4] = {rgb(100, 10, 10) };
float center_ball_colors[4] = {rgb(200, 110, 110) };
float ball_ptr[4] = { rgb(140, 0, 70) };

std::vector<float> prepare_colors_for_ball()
{
    std::vector<float> colors(4*(BALL_ITER+2));

    // bright color for the center of ball to make it visually convex
    for (int j = 0; j < 4; j++)
    {
        colors[j] = center_ball_colors[j];
    }

    for(int i = 1; i < BALL_ITER-1; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            colors[i*4+j] = ball_colors[j];
        }
    }

    //color to show that ball really rotate
    for(int i = -1; i < 1; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            colors[(BALL_ITER+i)*4+j] = ball_ptr[j];
        }
    }

    //last color to keep pretty pattern
    for (int j = 0; j < 4; j++)
    {
        colors[(BALL_ITER+1)*4+j] = ball_colors[j];
    }

    return colors;
}

float Ball::ball_scale = 0.05f;

Ball::Ball(glm::vec2 start_pos, float speed)
    : ball_speed(speed)
    , start_speed(speed)
    , start_position(start_pos)
{
    Model *model = new Model(GL_TRIANGLE_FAN, BALL_ITER+2, prepare_vertices_for_ball().data(), prepare_colors_for_ball().data());
    ball_draw = std::make_shared<BallDraw>(model, start_pos);
}

void Ball::update(float delta)
{
    ball_draw->move(velocity * ball_speed * delta);
}

void Ball::newVelocity(glm::vec2 collision_vector, bool random) {
//    std::cout << "Dot product = " <<collision_vector<<" "<< glm::dot(collision_vector, velocity) << "\n";
    //std::cout<<"\t"<<collision_vector.x << " "<<collision_vector.y<<"\n";
    if (glm::dot(collision_vector, velocity) < 0)
        velocity = velocity - 2 * glm::dot(collision_vector, velocity) * collision_vector;
    //std::cout << velocity.x << " " << velocity.y << "\n";

    if (random)
    {
        //random disorder after every rebound
        int rnd = rand() % 3 - 1;
        velocity.x += (0.05 * rnd);

        rnd = rand() % 3 - 1;
        velocity.y += (0.05 * rnd);
    }
    //increase speed for more fun
    ball_speed *= 1.005;
}

void Ball::reset()
{
    ball_draw->setPosition(start_position);
    int rnd = rand() % 3 - 1;
    velocity = glm::vec2(0.099f*rnd,-1.0f);
    ball_speed = start_speed;
}

void Ball::changeVelocityX(float x_change)
{
    velocity.x+=x_change;
}


Ball::BallDraw::BallDraw(Model *vbo_model, glm::vec2 pos)
    :Drawable("Ball", vbo_model, ball_scale, pos)
{

}
