#include "paddle.h"

#include <iostream>
#include "render.h"
#include "model.h"
#include "body.h"

std::vector<float> prepare_paddle_points(float length, float height)
{
    std::vector<float> points(2*4);

    points[0] = length;
    points[2] = -length;

    points[1] = points[3] = height;
    points[5] = points[7] = -height;

    // make paddle more interesting
    points[4] = -length + .05f;
    points[6] = length - .05f;

    return points;
}

float paddle_colors[4*4] =
        {
                rgb( 120, 120, 120),
                rgb( 120, 120, 120),
                rgb( 160, 160, 160),
                rgb( 160, 160, 160)
        };

Paddle::Paddle()
{
    Model *model = new Model(GL_TRIANGLE_FAN, 4, prepare_paddle_points(length, height).data(), paddle_colors);

    glm::vec2 pos(0.0, -SQRT3/2.0);
    paddle_draw = std::make_shared<PaddleDraw>(model, pos);
    paddle_physic_body = std::make_shared<Body>(4, prepare_paddle_points(length, height).data(), 0.25, paddle_draw->getPtrToPosition());
    paddle_physic_body->setIsPaddle(true);
}

Paddle::PaddleDraw::PaddleDraw(Model *vbo_model, glm::vec2 pos)
    :Drawable("Paddle", vbo_model, 0.25, pos)
{

}
