#include <cstdlib>
#include <iostream>
#include "board.h"
#include "render.h"
#include "model.h"
#include "body.h"

std::vector<float> prepare_hex_for_board()
{
    std::vector<float> hex_points(2 * 6);
    // Generate 6 points of hex
    for (int i = 0; i < 6; ++i) {
        hex_points[i * 2 + 0] = (float) cos(i * 2.0 * M_PI / (float) 6);
        hex_points[i * 2 + 1] = (float) sin(i * 2.0 * M_PI / (float) 6);
    }

    return hex_points;
}

std::vector<float> prepare_vertices_of_hex_for_board(){
    std::vector<float> hex_points = prepare_hex_for_board();
    // Generate 6 points of hex
    for(int i = 0; i < 6; ++i)
    {
        hex_points[i*2 + 0] = (float)cos(i * 2.0*M_PI/(float)6);
        hex_points[i*2 + 1] = (float)sin(i * 2.0*M_PI/(float)6);
    }

    //We have to make 4 triangles + 1 rectangle on top (2 triangles) => 6 triangles * 3 vertices * 2 coordinate
    std::vector<float> board_points(6*3*2);

    int v=0;
    //Copy first two
    for(; v < 4; ++v)
        board_points[v] = hex_points[v];

    board_points[v++] = hex_points[0]; // x from point 0
    board_points[v++] = hex_points[3]; // y from point 1


    //Copy second two
    for(; v < 10; ++v)
        board_points[v] = hex_points[v-2];

    board_points[v++] = hex_points[6]; // x from point 3
    board_points[v++] = hex_points[5]; // y from point 2


    //Copy 3 i 4
    for(; v < 16; ++v)
        board_points[v] = hex_points[v-6];

    board_points[v++] = hex_points[6]; // x from point 3
    board_points[v++] = hex_points[9]; // y from point 4

    //Copy 5 and 0
    board_points[v++] = hex_points[10];
    board_points[v++] = hex_points[11];
    board_points[v++] = hex_points[0];
    board_points[v++] = hex_points[1];

    board_points[v++] = hex_points[0]; // x from point 0
    board_points[v++] = hex_points[11]; // y from point 5

    assert(v==24);

    //add rectangle
    float top = 2.0f-board_points[5];

    board_points[v++] = board_points[4];
    board_points[v++] = board_points[5];
    board_points[v++] = board_points[10];
    board_points[v++] = board_points[11];
    board_points[v++] = board_points[10];
    board_points[v++] = top;

    board_points[v++] = board_points[4];
    board_points[v++] = board_points[5];
    board_points[v++] = board_points[4];
    board_points[v++] = top;
    board_points[v++] = board_points[10];
    board_points[v++] = top;

    assert(v==36);

    return board_points;
}

float board_colors[4] = {rgb(0, 20, 70) };

std::vector<float> prepare_colors_for_board()
{
    std::vector<float> colors(4*3*6); // color (4 variables) * 6 triangles * 3 vertices

    for(int i = 0; i < 3*6; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            colors[i*4+j] = board_colors[j];
        }
    }

    //make boardFrame more pretty
    for (int j = 0; j < 3; j++)
    {
        colors[4*8+j] = 0.0f;
        colors[4*11+j] = 0.0f;
    }

    return colors;
}

BoardFrame::BoardFrame()
{
    std::vector<float> board_points = prepare_vertices_of_hex_for_board();
    Model *model = new Model(GL_TRIANGLES, 6*3, board_points.data(), prepare_colors_for_board().data());

    //move board to down of the window
    glm::vec2 pos(0.0, -1.0-board_points[23]);
    board_pieces = std::make_shared<BoardDraw>(model, pos);


    std::vector<float> hex_points = prepare_hex_for_board();

    for (int i = 0; i < 4; ++i) // Right UP, UP, Left UP, Left DOWN
    {
        auto wall = std::make_shared<Body>( glm::vec2(hex_points[2*i] , hex_points[2*i+1]), glm::vec2(hex_points[2*i+2], hex_points[2*i+3]), 1.0f, board_pieces->getPtrToPosition());
        all_walls_physic_bodies.push_back(wall);
    }
    auto wall = std::make_shared<Body>( glm::vec2(hex_points[10] , hex_points[11]), glm::vec2(hex_points[0], hex_points[1]), 1.0f, board_pieces->getPtrToPosition());
    all_walls_physic_bodies.push_back(wall); // Right DOWN

    float safty_zone = 0.1;
    dead_zone = std::make_shared<Body> (glm::vec2(hex_points[8] , hex_points[9]-safty_zone), glm::vec2(hex_points[10], hex_points[11]-safty_zone), 1.0f, board_pieces->getPtrToPosition());
}

BoardFrame::BoardDraw::BoardDraw(Model *vbo_model, glm::vec2 pos)
        : Drawable("BoardFrame", vbo_model, 1.0f, pos)
{

}
