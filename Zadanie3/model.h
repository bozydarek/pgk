#ifndef MODEL_H
#define MODEL_H

#include <glm/glm.hpp>
#include <vector>
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Model
{
public:
    Model(GLenum mode_, unsigned int size,
          std::initializer_list<float> vert,
          std::initializer_list<std::initializer_list<float>> color);


    Model(GLenum mode_, unsigned int size,
          std::initializer_list<float> vert,
          std::initializer_list<float> color);


    Model(GLenum mode_, unsigned int size,
          const float *vert,
          const float *color);

	~Model();

    unsigned short  all_color_versions;

    unsigned int    getVertices(){ return  vertices; }
    GLenum          getMode(){ return mode; }

    float           *vert_data = nullptr;
    float           **colors_data = nullptr;
    
    GLuint          vertex_buffer;
    GLuint*         color_buffer = nullptr;

	glm::vec2		getVert(int i);

private:
    GLenum          mode;
    unsigned int    vertices;
    
	void 			buffers_initialization();
};


#endif //MODEL_H
