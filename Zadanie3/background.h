#ifndef BACKGROUND_H
#define BACKGROUND_H


#include <memory>
#include "drawable.h"

// Class for create the background
class Background
{
    public:
        Background(int, int);
	
	// Class responsible for a single background element
    class BackgroundDraw: public Drawable
    {
        public:
            BackgroundDraw(Model* vbo_model, float scale, glm::vec2 pos);
    };
	
	// Vector of all background elements
    std::vector <std::shared_ptr<BackgroundDraw>> bg_pieces;
};


#endif //BACKGROUND_H
