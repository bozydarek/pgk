# CMake entry point
cmake_minimum_required(VERSION 2.8)
project (PGK)

find_package(OpenGL REQUIRED)

# Compile external dependencies
add_subdirectory (external)

if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
	message(STATUS "Using clang.")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
endif()

add_definitions(
	-DTW_STATIC
	-DTW_NO_LIB_PRAGMA
	-DTW_NO_DIRECT3D
	-DGLEW_STATIC
	-D_CRT_SECURE_NO_WARNINGS
	-std=c++11
	-g
	-Wall
	-Wextra
	-pedantic
    #-O2
	#-Werror
)

include_directories(
	${GLFW_GLEW_INCLUDE_DIRS}
	#${ASSIMP_INCLUDE_DIRS}
	external/assimp-3.3.1/include/
	external/glm-0.9.7.1/
	#	${GLM_INCLUDE_DIRS}
	${FREETYPE_INCLUDE_DIRS}
)

if   (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
	#set(OPENGL_LIBRARY ${OPENGL_LIBRARY}  -lGL -lGLU -lXrandr -lXext -lX11 -lrt)
	#Sometimes, on somes distros, you need more flags. Don't ask me why ;/
	set(OPENGL_LIBRARY ${OPENGL_LIBRARY}  -lm -ldl -lXinerama -lXcursor -lglfw3 -lGL -lGLU -lGLEW -lASSIMP -lX11 -lXxf86vm -lpthread -lXrandr -lXi)
endif(${CMAKE_SYSTEM_NAME} MATCHES "Linux")

link_directories(
	${FREETYPE_CUSTOM_LINK_DIR} # Used if freetype was compiled manually
	${GLFW_GLEW_LIBRARY_DIRS}
	${ASSIMP_LIBRARY_DIRS}
)

#Add colors for linux ;)
if(NOT WIN32)
  string(ASCII 27 Esc)
  set(ColourReset "${Esc}[m")
  set(ColourBold  "${Esc}[1m")
  set(Red         "${Esc}[31m")
  set(Green       "${Esc}[32m")
  set(Yellow      "${Esc}[33m")
  set(Blue        "${Esc}[34m")
  set(Magenta     "${Esc}[35m")
  set(Cyan        "${Esc}[36m")
  set(White       "${Esc}[37m")
  set(BoldRed     "${Esc}[1;31m")
  set(BoldGreen   "${Esc}[1;32m")
  set(BoldYellow  "${Esc}[1;33m")
  set(BoldBlue    "${Esc}[1;34m")
  set(BoldMagenta "${Esc}[1;35m")
  set(BoldCyan    "${Esc}[1;36m")
  set(BoldWhite   "${Esc}[1;37m")
endif()

#Zadanie3 - Arkanoid
if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie3")
	add_executable(Arkanoid
		Zadanie3/main.cpp
		Zadanie3/render.cpp
		Zadanie3/render.h
		Zadanie3/shader.cpp
		Zadanie3/shader.h
		Zadanie3/game.cpp
		Zadanie3/game.h
		Zadanie3/background.cpp 
		Zadanie3/background.h 
		Zadanie3/drawable.cpp 
		Zadanie3/drawable.h
        Zadanie3/bricks.cpp
		Zadanie3/bricks.h
		Zadanie3/ball.cpp 
		Zadanie3/ball.h 
		Zadanie3/paddle.cpp 
		Zadanie3/paddle.h
        Zadanie3/model.cpp
        Zadanie3/model.h
        Zadanie3/board.cpp
        Zadanie3/board.h
        Zadanie3/body.cpp
        Zadanie3/body.h
        
        
		Zadanie3/color.fragmentshader
		Zadanie3/vert.vertexshader)
    
    # Files needed by Zadanie3 on runtime.
	set( ZAD3_RUNTIME_FILES
		Zadanie3/color.fragmentshader
		Zadanie3/vert.vertexshader
	)
	
	if(GLFW_DEPS)
		add_dependencies(Arkanoid GLEW_1130 GLFW_312 GLFW GLEW)
	endif()
	if(GLM_DEPS)
		add_dependencies(Arkanoid GLM)
	endif()


	target_link_libraries(Arkanoid
		${GLFW_GLEW_LIBRARIES}
		${OPENGL_LIBRARY}
		${CMAKE_DL_LIBS}
	)

	# Copy the binary to Zadanie3 folder
	add_custom_command(
		TARGET Arkanoid POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/Arkanoid${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie3/"
	)

	# Copy runtime files
	foreach( file ${ZAD3_RUNTIME_FILES})
	    add_custom_command(
		    TARGET Arkanoid POST_BUILD
		    COMMAND ${CMAKE_COMMAND}
		    ARGS -E copy "${CMAKE_CURRENT_SOURCE_DIR}/${file}" "${CMAKE_CURRENT_BINARY_DIR}"
	)
	message("${BoldWhite}${file}${ColourReset} - added to the copy")
	endforeach( file )
endif (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie3")

#Zadanie4 - Aquarium
if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie4")


    set(PROJECT_NAME Aquarium)

	add_executable(${PROJECT_NAME}
			Zadanie4/main.cpp
            Zadanie4/Camera.cpp
            Zadanie4/Camera.h
			Zadanie4/Render.cpp
			Zadanie4/Render.h
            Zadanie4/Shader.cpp
            Zadanie4/Shader.h
            Zadanie4/Sphere.cpp
            Zadanie4/Sphere.h
			Zadanie4/Box.cpp
			Zadanie4/Box.h
			Zadanie4/Player.cpp
			Zadanie4/Player.h
			Zadanie4/Shape.cpp
			Zadanie4/Shape.h
		    Zadanie4/Game.cpp 
		    Zadanie4/Game.h 
		    Zadanie4/Tools.h
			Zadanie4/Fonts.cpp
			Zadanie4/Fonts.h
            Zadanie4/Text.cpp
            Zadanie4/Text.h)
    
    # Files needed by Zadanie4 on runtime.
	set( ZAD4_RUNTIME_FILES
		Zadanie4/fragment.glsl
		Zadanie4/vertex.glsl
		Zadanie4/textFragment.glsl
		Zadanie4/textVertex.glsl
        Zadanie4/Ubuntu.ttf)
	
	if(GLFW_DEPS)
		add_dependencies(${PROJECT_NAME} GLEW_1130 GLFW_312 GLFW GLEW)
	endif()
	if(GLM_DEPS)
		add_dependencies(${PROJECT_NAME} GLM)
	endif()
	if(FREETYPE_DEPS)
	    add_dependencies(${PROJECT_NAME} Freetype)
	endif()


	target_link_libraries(${PROJECT_NAME}
		${GLFW_GLEW_LIBRARIES}
		${FREETYPE_LIBRARIES}
		${OPENGL_LIBRARY}
		${CMAKE_DL_LIBS}
	)

	# Copy the binary to Zadanie4 folder
	add_custom_command(
		TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/${PROJECT_NAME}${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie4/"
	)

	# Copy runtime files
	foreach( file ${ZAD4_RUNTIME_FILES})
	    add_custom_command(
		    TARGET ${PROJECT_NAME} POST_BUILD
		    COMMAND ${CMAKE_COMMAND}
		    ARGS -E copy "${CMAKE_CURRENT_SOURCE_DIR}/${file}" "${CMAKE_CURRENT_BINARY_DIR}"
	)
	message("${BoldWhite}${file}${ColourReset} - added to the copy")
	endforeach( file )
endif (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie4")

#Zadanie5 - WorldMap
if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie5")


    set(PROJECT_NAME WorldMap)

	add_executable(${PROJECT_NAME}
			Zadanie5/main.cpp
			Zadanie5/Render.cpp
			Zadanie5/Render.h
			Zadanie5/Shader.cpp
			Zadanie5/Shader.h
			Zadanie5/Fonts.cpp
			Zadanie5/Fonts.h
			Zadanie5/Text.cpp
			Zadanie5/Text.h
            Zadanie5/Tile.cpp
			Zadanie5/Tile.h
			Zadanie5/Camera.cpp
			Zadanie5/Camera.h
			Zadanie5/Grid.cpp
			Zadanie5/Grid.h
			)
    
    # Files needed by Zadanie5 on runtime.
	set( ZAD5_RUNTIME_FILES
		Zadanie5/mapFragment.glsl
		Zadanie5/mapVertex.glsl
		Zadanie5/gridFragment.glsl
		Zadanie5/gridVertex.glsl
		Zadanie5/textFragment.glsl
		Zadanie5/textVertex.glsl
        Zadanie5/Ubuntu.ttf)
	
	if(GLFW_DEPS)
		add_dependencies(${PROJECT_NAME} GLEW_1130 GLFW_312 GLFW GLEW)
	endif()
	if(GLM_DEPS)
		add_dependencies(${PROJECT_NAME} GLM)
	endif()
	if(FREETYPE_DEPS)
	    add_dependencies(${PROJECT_NAME} Freetype)
	endif()


	target_link_libraries(${PROJECT_NAME}
		${GLFW_GLEW_LIBRARIES}
		${FREETYPE_LIBRARIES}
		${OPENGL_LIBRARY}
		${CMAKE_DL_LIBS}
	)

	# Copy the binary to Zadanie5 folder
	add_custom_command(
		TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/${PROJECT_NAME}${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie5/"
	)

	# Copy runtime files
	foreach( file ${ZAD5_RUNTIME_FILES})
	    add_custom_command(
		    TARGET ${PROJECT_NAME} POST_BUILD
		    COMMAND ${CMAKE_COMMAND}
		    ARGS -E copy "${CMAKE_CURRENT_SOURCE_DIR}/${file}" "${CMAKE_CURRENT_BINARY_DIR}"
	)
	message("${BoldWhite}${file}${ColourReset} - added to the copy")
	endforeach( file )
endif (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie5")

#Zadanie6 - OBJ viewer
if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie6")


    set(PROJECT_NAME OBJViewer)

	add_executable(${PROJECT_NAME}
			Zadanie6/main.cpp
			Zadanie6/Camera.h
			Zadanie6/Camera.cpp
            Zadanie6/Render.h
            Zadanie6/Render.cpp
            Zadanie6/Text.h
            Zadanie6/Text.cpp
            Zadanie6/Fonts.h
            Zadanie6/Fonts.cpp
            Zadanie6/Shader.h
            Zadanie6/Shader.cpp
            Zadanie6/ObjParser.cpp
            Zadanie6/ObjParser.h
			Zadanie6/Texture.h
			Zadanie6/Texture.cpp
			)
    
    # Files needed by Zadanie6 on runtime.
	set( ZAD6_RUNTIME_FILES
        	Zadanie6/textFragment.glsl
			Zadanie6/textVertex.glsl
			Zadanie6/StandardFragmentShader.glsl
			Zadanie6/StandardVertexShader.glsl
			Zadanie6/Noise1FragmentShader.glsl
			Zadanie6/Ubuntu.ttf

			Zadanie6/SampleOBJFiles/suzanne.obj
			Zadanie6/SampleOBJFiles/kostka.obj
			Zadanie6/SampleOBJFiles/kostka2.obj
			Zadanie6/SampleOBJFiles/Realistic_Female.obj
            Zadanie6/SampleOBJFiles/Female.obj
			Zadanie6/SampleOBJFiles/Eye_Mesh.obj
			Zadanie6/SampleOBJFiles/Teeth_Mesh.obj
            Zadanie6/SampleOBJFiles/Cirno.obj

			Zadanie6/textures/uvsuzanna.DDS
			Zadanie6/textures/uvtemplate.bmp
			Zadanie6/textures/uvtemplate.DDS
			)

	if(GLFW_DEPS)
		add_dependencies(${PROJECT_NAME} GLEW_1130 GLFW_312 GLFW GLEW)
	endif()
	if(GLM_DEPS)
		add_dependencies(${PROJECT_NAME} GLM)
	endif()
	if(FREETYPE_DEPS)
	    add_dependencies(${PROJECT_NAME} Freetype)
	endif()
	if(ASSIMP_DEPS)
		add_dependencies(${PROJECT_NAME} ASSIMP)
	endif()


	target_link_libraries(${PROJECT_NAME}
		${GLFW_GLEW_LIBRARIES}
        ${ASSIMP_LIBRARIES}
        ${FREETYPE_LIBRARIES}
        ${OPENGL_LIBRARY}
        ${CMAKE_DL_LIBS}
	)

	# Copy the binary to Zadanie6 folder
	add_custom_command(
		TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/${PROJECT_NAME}${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie6/"
	)

	# Copy runtime files
	foreach( file ${ZAD6_RUNTIME_FILES})
	    add_custom_command(
		    TARGET ${PROJECT_NAME} POST_BUILD
		    COMMAND ${CMAKE_COMMAND}
		    ARGS -E copy "${CMAKE_CURRENT_SOURCE_DIR}/${file}" "${CMAKE_CURRENT_BINARY_DIR}"
	)
	message("${BoldWhite}${file}${ColourReset} - added to the copy")
	endforeach( file )
endif (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Zadanie6")

