Building
===
To build the application, use CMake. You will need at libgl-dev and xorg-dev.
Other dependencies, including glew and glfw are either provided
with the source and will be build by cmake.

```
#!bash

mkdir build
cd build
cmake ..
make
```

To speed up the process, use: `make -jN`
where N is twice the number of your CPU cores.