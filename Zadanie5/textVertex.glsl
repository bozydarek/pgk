#version 330

layout(location = 0) in vec4 coord;
out vec2 texcoord;

//https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Text_Rendering_01#Shaders

void main(void) {
    // Use first two floats as vertex coordinates, and
    // the other two as texture coordinates.
    gl_Position = vec4(coord.xy, 0, 1);
    texcoord = coord.zw;
}
