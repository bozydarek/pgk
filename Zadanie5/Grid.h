#ifndef PGK_GRID_H
#define PGK_GRID_H

#include <GL/glew.h>
#include <memory>
#include "Shader.h"

#define GRIDS 3

class Grid {
    Grid() = delete; // static class
    static unsigned long size[GRIDS];
    static std::shared_ptr<GridShader> shader;
    static GLuint gridsbuffer[GRIDS];
    static int actualGrid;

public:
    static void Init(int width, int height);
    static void Render(bool is3D, float xscale, glm::mat4 view_trans);

    static void incActualGrid();
    static void decActualGrid();
};


#endif //PGK_GRID_H
