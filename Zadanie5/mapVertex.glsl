#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in uint vertexPosition;
layout(location = 1) in int vertexData;

uniform vec2 pos;
uniform float xscale;

uniform bool sphere = false;
uniform int terrainscale;

uniform mat4 view_transform;

// out color for vertex
out vec4 fragmentColor;

#define EARTH_R 6371000
#define DEG2RAD(x) (x*0.0174532925f)

vec3 getcolor(float ht){
	vec3 color;
	if	  (ht < 0  )     color = vec3(0.0,	    0.0,		    1.0); //blue
	else if (ht < 10  )  color = vec3(0.0,	    0.5*ht/10,	    1.0-ht/10 ); //blue->dgreen
	else if (ht < 500)   color = vec3(0.0,	    ht/1000 + 0.5,  0.0	   ); //dgreen->lgreen
	else if (ht < 1000)  color = vec3(ht/500-1, 1.,		        0.0	   ); //lgreen->yellow
	else if (ht < 2000)  color = vec3(1.0,	    2.-ht/1000,     0.0	   ); //yellow->dred
	else if (ht < 3000)  color = vec3(1.0,	    ht/1000-2,	    ht/1000-2); //dred->lred
	else				 color = vec3(1.0,      1.0,            1.0); //white
	return color;
}

vec3 transformcoords(vec3 pos, float height){
	float q = cos(DEG2RAD(pos.y));
	float h = (EARTH_R + height*terrainscale)/EARTH_R;
	return vec3( sin(DEG2RAD(pos.x))*q,
	            -cos(DEG2RAD(pos.x))*q,
	             sin(DEG2RAD(pos.y))
	            ) * h;
}

void main(){

        float a = 1.0;
        if(vertexData == -32768) // when there is no data about this point.
            a = -1e20;   // For more see https://dds.cr.usgs.gov/srtm/version2_1/Documentation/Quickstart.pdf


        fragmentColor = vec4(getcolor(vertexData), a);

        uint encodedXpos = uint(uint(vertexPosition) >> uint(16));
        uint encodedYpos = uint(vertexPosition & uint(0xffff));

        float posX = float(encodedXpos)/1200.0 + pos.x;
        float posY = float(encodedYpos)/-1200.0 + pos.y;

        vec3 realpos = vec3(posX, posY, 0.0);

        if(sphere) realpos = transformcoords(realpos, vertexData);
        else realpos.x *= xscale;

        gl_Position = view_transform * vec4(realpos, 1.0);
}
