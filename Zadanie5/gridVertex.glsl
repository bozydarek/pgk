#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec2 pointpos;

uniform float xscale;
uniform bool sphere = false;

uniform mat4 view_transform;

#define DEG2RAD(x) (x*0.0174532925f)

vec3 transformcoords(vec3 pos){
	float q = cos( DEG2RAD(pos.y) );
	return vec3( sin(DEG2RAD(pos.x))*q,
	            -cos(DEG2RAD(pos.x))*q,
	             sin(DEG2RAD(pos.y))
	            );
}

void main(){

        vec3 realpos = vec3(pointpos.x,pointpos.y,0.0);

        if(sphere) realpos = transformcoords(realpos);
        else realpos.x *= xscale;

        gl_Position = view_transform * vec4(realpos, 1.0);
}
