#ifndef SHADER_H
#define SHADER_H

#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Shader{

protected:
    GLuint shader_id;
    bool load(const char * vertex_file_path, const char * fragment_file_path);

public:
    Shader(const char * vertex_file_path, const char * fragment_file_path);
    virtual ~Shader();

    void use();
};


class BasicShader : public Shader {
    // uniform locations
    GLint tile_pos;
    GLint xscale;
    GLint view_trans;

    GLint sphere;
    GLint terrainscale;

public:
    BasicShader(const char *vertex_file_path, const char *fragment_file_path);

    void set_tile_pos(int lon, int lat);
    void set_xscale(float value);
    void set_view_transform(const glm::mat4 &value);

    void set_is_sphere_viev(bool value);
    void set_terrainscale(int value);
};

class FontShader : public Shader{
private:
    GLint uniform_tex;
    GLint uniform_color;
public:
    FontShader(const char * vertex_file_path, const char * fragment_file_path);

    void set_font_tex(int value);
    void set_font_color(const GLfloat *value);
};


class GridShader : public Shader {

    GLint sphere;
    GLint xscale;

    GLint view_trans;
public:
    GridShader(const char *vertex_file_path, const char *fragment_file_path);

    void set_is_sphere_viev(bool value);
    void set_xscale(float value);
    void set_view_transform(const glm::mat4 &value);

};

#endif
