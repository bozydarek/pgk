#ifndef PGK_CAMERA_H
#define PGK_CAMERA_H

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define PI 3.14159265f
#define DEG2RAD(x) (x*0.0174532925f)


class Camera {
    glm::vec3   position;
    glm::mat4   lookAtMatrix;

    float       speed = 0.5f;
    float       step = 5.0f;//1.0f;

    // when we're farther from the map we have to make bigger step
    float       FoV = DEG2RAD(40);

    float       pitch = 0.0f;
    float       yaw = -PI/2.0f;

    bool        is3D = false;

public:
    Camera(glm::vec3 pos);

    const glm::vec3 &getPosition() const;
    void    setPosition(const glm::vec3 &position);

    void    setFOV(float f) { FoV = f; }
    void    setFOVdg(float f) { FoV = f*2.0f*3.1415926f/360.0f; }
    float   getFOV() const { return FoV; }
    float   getFOVdg() const { return FoV*360.0f/(2.0f*3.1415926f); }

    // move for 2D camera
    void    moveNorth(float time);
    void    moveSouth(float time);
    void    moveWest(float time);
    void    moveEast(float time);
    // move for 3D camera
    void    moveIn(float time);
    void    moveForward(float time);
    void    moveBackward(float time);
    void    moveLeft(float time);
    void    moveRight(float time);

    glm::mat4 getViewMatrix();
    glm::mat4 getPerspectiveTransform(float ratio);
    glm::mat4 getTransform() const;

    float   getSpeed() const;
    void    setSpeed(float speed);
    float   getStep() const;
    void    setStep(float step);
    void    setIs3D(bool is3D);
    bool    is3Dcam() const;

    void    rotatePitch(float delta);
    void    rotateYaw(float delta);


};


#endif //PGK_CAMERA_H
