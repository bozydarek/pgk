#ifndef RENDER_H
#define RENDER_H

#include <algorithm>
#include <functional>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/vec2.hpp>
#include <vector>
#include <memory>
#include "Camera.h"

class Render
{
private:
	Render() = delete; // static class

	// Used for scroll handling
	static void ScrollCallback(GLFWwindow*, double, double);

private:
    static float 	pxsizex, pxsizey;
    static int      width, height;
	static GLuint   VertexArrayID;
	static float	window_ratio;

public:
	static int 			Init (int height_, int width_);

//	static void 		DrawFrame(Camera *camera);
    static void     	DrawFrame();
	static void 		PrepareToDrawFrame();

	static void 		CleanUp ();

	static double 		GetTime ();
	static bool 		IsKeyPressed (int key);
	static bool 		IsWindowClosed ();


    static void         centerMouse();
    static glm::vec2    updateMouse();
	// Scroll callback
	static std::function<void(double)> scroll_callback;

    static void 		centerMouseOnStart();
	static int 			getWidth();
	static int 			getHeight();
	static float 		getWindowRatio();

	static void FirstPart(std::shared_ptr<Camera> actualCamera, float xscale, int terrain_scale_multipler);
};


#endif //RENDER_H
