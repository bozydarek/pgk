#ifndef PGK_TILE_H
#define PGK_TILE_H

#include <memory>
#include <fstream>

class Tile {
    // one more for RESET INDEX more on: https://www.khronos.org/opengl/wiki/Vertex_Rendering#Primitive_Restart
    std::array<short,1201*1202> data;
    GLuint databuffer;
    static GLuint positionsbuffer;
    static GLuint indicesbuffer[6];

private:
    Tile(){};
    void LoadFromFile(std::ifstream& file, int latitude_, int longitude_);

public:
    int longitude;
    int latitude;

    static std::shared_ptr<Tile> Create(std::string path, int latitude_, int longitude_, bool quiet_mode);

    static inline int   XY2N(int x, int y);
    static inline void  N2XY(int n, int &x, int &y);
    static inline int   XY2Ndivided(int x, int y, float no);

    static void     Init(); //Prepare vertex positions for every level of details (similar for every tile)
    void            SendData2GraphicCard();
    unsigned int    Render(int lod);
};


#endif //PGK_TILE_H
