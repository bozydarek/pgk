#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <deque>

#include "Render.h"
#include "Text.h"
#include "Fonts.h"
#include "Tile.h"
#include "Camera.h"
#include "Shader.h"
#include "Grid.h"

#define BOLD(TEXT) "\033[1m" #TEXT "\033[0m"

extern BasicShader* global_shader;

using namespace std;

void man();
bool extractCords(string cords, int& lat, int& lon);
glm::vec4 findCenter();
void ScrollCallback(double x);

void AutoLODTooHigh(int &lod);
void AutoLODTooLow(int &lod);
int  last_lod_change = 0;

inline bool intersect(float Ax1, float Ay1, float Ax2, float Ay2, float Bx1, float By1, float Bx2, float By2){
    return (Ax1 < Bx2 && Ax2 > Bx1 && Ay1 < By2 && Ay2 > By1);
}

std::vector<std::shared_ptr<Tile>> tiles;
std::shared_ptr<Camera> camera2D;
std::shared_ptr<Camera> camera3D;
std::shared_ptr<Camera> actualCamera;

bool view_3D_mode = false;

int main(int argc, char** argv)
{
    // Parse input data
    if(argc == 1){
        man();
    }
    int height=768, width=1024;
    int lat, lon;
    bool any_map_to_load = false;
    bool quiet_mode = false;

    //first check if some one turn on quiet mode
    for(int i = 1; i < argc; ++i)
    {
        std::string argument(argv[i]);
        if( argument == "-q")
            quiet_mode = true;
    }

    for(int i = 1; i < argc; ++i)
    {
        std::string argument(argv[i]);
        unsigned long k = argument.length();
        if( k > 4 && argument.substr(k-4) == ".hgt" ){
            if( argument.length() < 11 || !extractCords(argument.substr(k-11,7),lat,lon) ){
				cout << "Argument '" << argument << "' does not seem valid." << endl;
                return 1;
			}
            any_map_to_load = true;
            if(not quiet_mode) cout << "Opening file " << argument << "..." << endl;
            std::shared_ptr<Tile> tile = Tile::Create(argument,lat,lon, quiet_mode);
            if(not tile){
                std::cout << "Problem with accessing to file: " << argument << "." << std::endl;
                return 5;
            }
            tiles.push_back(tile);
        }
        else if( argument == "--help"){
            man();
            return 0;
        }
        else if( argument == "-h"){
            if(++i >= argc){
                cout << "You have to pass argument after -h. Type --help for manual." << endl;
                return 1;
            }

            argument = argv[i];
            int h = atoi(argument.c_str());
            if (h <= 0){
                cout << "Argument '" << argument << "' does not seem to be valid for height." << endl;
                return 1;
            }
            else if( h < 400 || h > 1080){
                cout << "Please set windows height bigger then 400 and smaller then 1080" << endl;
                return 1;
            }

            height = h;
        }
        else if( argument == "-w"){
            if(++i >= argc){
                cout << "You have to pass argument after -w. Type --help for manual." << endl;
                return 1;
            }

            argument = argv[i];
            int w = atoi(argument.c_str());
            if (w <= 0){
                cout << "Argument '" << argument << "' does not seem to be valid for width." << endl;
                return 1;
            }
            else if( w < 400 || w > 1920){

                cout << "Please set windows width bigger then 400 and smaller then 1920" << endl;
                return 1;
            }

            width = w;
        }
        else if( argument == "-q") {
            // It has moved above, but this have to stay because validation method is very strict
        }
        else{
            cout << "Argument '" << argument << "' does not seem valid." << endl;
            return 1;
        }
    }

    if (not any_map_to_load){
        cout << "You have to pass any hgt file to this program" << endl;
        return 3;
    }

    //Init openGL things
    if(not quiet_mode) cout << "Initiating Renderer..."<< endl;
        int n = Render::Init(height, width);
        if(n) {
            return n;
        }
    if(not quiet_mode) cout << "...Done" << endl;

    if(not quiet_mode) cout << "Initiating Tiles..."<< endl;
        Tile::Init();
    if(not quiet_mode) cout << "...Sending data to graphic card..."<< endl;
        for(auto tile : tiles){
            tile->SendData2GraphicCard();
        }
    if(not quiet_mode) cout << "...Done" << endl;

    if(not quiet_mode) cout << "Initiating Grid..."<< endl;
        Grid::Init(180, 80);
    if(not quiet_mode) cout << "...Done" << endl;

    if(not quiet_mode) cout << "Initiating Fonts..."<< endl;
    init_font();
    if(not quiet_mode) cout << "...Done" << endl;

    // Preapare UT text labels

    #define MANUAL_LINES 9

    std::shared_ptr<Text> manual[MANUAL_LINES] = {
        std::make_shared<Text>("MANUAL",                       glm::vec2(50,30),  24, glm::vec3(1.0,0.0,0.0)),
        std::make_shared<Text>("TAB: Switch camera mode",      glm::vec2(10,50)),
        std::make_shared<Text>("W/S/A/D: Move camera",         glm::vec2(10,70)),
        std::make_shared<Text>("SCROLL: Go up/down",           glm::vec2(10,90)),
        std::make_shared<Text>("1-6: set LOD, 0: auto",        glm::vec2(10,110)),
        std::make_shared<Text>("SHIFT: hold to move faster",   glm::vec2(10,130)),
        std::make_shared<Text>("G/H: change grid density",     glm::vec2(10,150)),
        std::make_shared<Text>("M: Hide manual",               glm::vec2(10,170)),
        std::make_shared<Text>("Q/E: Change terrain scale (DOWN/UP)", glm::vec2(10,190))
    };

    manual[8]->active = false;

    auto press_mn = std::make_shared<Text>("Press M to show manual.", glm::vec2(10,20),  16, glm::vec3(1.0,0.0,1.0));
    press_mn->active = false;

    #define MARGIN_UL 180
    auto fps_text = std::make_shared<Text>("FPS: 60", glm::vec2(Render::getWidth()-MARGIN_UL, 20));
    auto lod_text = std::make_shared<Text>("LOD: 5",  glm::vec2(Render::getWidth()-MARGIN_UL, 40));
    auto triangles_text = std::make_shared<Text>("Triangles: 0", glm::vec2(Render::getWidth()-MARGIN_UL, 60));
    auto terr_scal_text = std::make_shared<Text>("Terrain scale: Real(1x)", glm::vec2(Render::getWidth()-MARGIN_UL, 80));
    terr_scal_text->active = false;

    // flags and variables
    bool show_manual = true;
    bool M_key_down = false;
    bool TAB_key_down = false;
    bool E_key_down = false;
    bool Q_key_down = false;
    bool G_key_down = false;
    bool H_key_down = false;


    double lastTimeForFps = Render::GetTime();
    double lastTime = Render::GetTime();
    int nbFrames = 0;
    std::deque<int> samples; // to make average from few measurements
    #define SAMPLES_NO 4
    double FPSavg = 30.0;


    int lod = 4; // level of detail
    bool auto_lod = false;

    glm::vec4 center = findCenter();
    float xTransformScale = center.w;
    camera2D = std::make_shared<Camera>( glm::vec3(center.x*xTransformScale, center.y, 1.5) );
    camera3D = std::make_shared<Camera>( glm::vec3(center.x, center.y, 1.15) );
    camera3D->setIs3D(true);
    camera3D->setSpeed(20.0f);

    actualCamera = camera2D;

    int terrain_scale_multipler = 1;

    Render::scroll_callback = ScrollCallback;

    //Main loop
    do{
        double newTime = Render::GetTime();
        double deltaTime = newTime - lastTime;
        lastTime = newTime;

        //FPS counter
        nbFrames++;
        if ( newTime - lastTimeForFps >= 1.0 ) {
            //cout << 1000.0/double(nbFrames) << " ms/frame\n";

            fps_text->SetText("FPS: " + std::to_string(nbFrames));
            samples.push_back(nbFrames);
            FPSavg += nbFrames;
            ++last_lod_change;

            nbFrames = 0;
            lastTimeForFps += 1.0;

            if (samples.size() > SAMPLES_NO) {
                FPSavg -= samples.front();
                samples.pop_front();
            }

            if (auto_lod && last_lod_change > SAMPLES_NO) {
                double avg = FPSavg / samples.size();
                if (avg < 25) AutoLODTooHigh(lod);
                if (avg > 50) AutoLODTooLow(lod);
                lod_text->SetText("LOD: " + std::to_string(lod + 1) + "(Auto)");
            }
        }
        //key handling
        if( not view_3D_mode ) {
            if( not Render::IsKeyPressed(GLFW_KEY_TAB)) TAB_key_down = false;
            if( Render::IsKeyPressed(GLFW_KEY_TAB) && not TAB_key_down )
            {
                TAB_key_down = true;

                view_3D_mode = true;
                actualCamera = camera3D;

                if(show_manual) manual[8]->active = true;
                terr_scal_text->active = true;
            }

            float amount = (float) deltaTime * camera2D->getSpeed() * camera2D->getStep();

            if (Render::IsKeyPressed(GLFW_KEY_LEFT_SHIFT)) amount *= 5;
            if (Render::IsKeyPressed(GLFW_KEY_W)) camera2D->moveNorth(amount);
            if (Render::IsKeyPressed(GLFW_KEY_S)) camera2D->moveSouth(amount);
            if (Render::IsKeyPressed(GLFW_KEY_A)) camera2D->moveEast(amount);
            if (Render::IsKeyPressed(GLFW_KEY_D)) camera2D->moveWest(amount);
        }
        else{
            if( not Render::IsKeyPressed(GLFW_KEY_TAB)) TAB_key_down = false;
            if(Render::IsKeyPressed(GLFW_KEY_TAB) && not TAB_key_down)
            {
                TAB_key_down = true;

                view_3D_mode = false;
                actualCamera = camera2D;

                manual[8]->active = false;
                terr_scal_text->active = false;
            }

            if( not Render::IsKeyPressed(GLFW_KEY_E)) E_key_down = false;
            if( Render::IsKeyPressed(GLFW_KEY_E)  == GLFW_PRESS && not E_key_down) {
                E_key_down = true;
                ++terrain_scale_multipler;
                terr_scal_text->SetText("Terrain scale: " + std::to_string(terrain_scale_multipler) + "x");
            }
            if( not Render::IsKeyPressed(GLFW_KEY_Q)) Q_key_down = false;
            if( Render::IsKeyPressed(GLFW_KEY_Q)  == GLFW_PRESS && not Q_key_down) {
                Q_key_down = true;
                if(terrain_scale_multipler > 1) {
                    --terrain_scale_multipler;
                    terr_scal_text->SetText("Terrain scale: " + std::to_string(terrain_scale_multipler) + "x");
                }
            }

            float amount = float(deltaTime)*camera3D->getSpeed()*(camera3D->getPosition().z-1.0f);
            if(Render::IsKeyPressed(GLFW_KEY_LEFT_SHIFT)) amount *= 5;
            if(Render::IsKeyPressed(GLFW_KEY_W)) camera3D->moveForward(amount);
            if(Render::IsKeyPressed(GLFW_KEY_S)) camera3D->moveBackward(amount);
            if(Render::IsKeyPressed(GLFW_KEY_A)) camera3D->moveRight(amount);
            if(Render::IsKeyPressed(GLFW_KEY_D)) camera3D->moveLeft(amount);

            glm::vec2 mouse = Render::updateMouse();
            camera3D->rotatePitch(mouse.x);
            camera3D->rotateYaw(mouse.y);
        }

        if( not Render::IsKeyPressed(GLFW_KEY_M)) M_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_M)  == GLFW_PRESS && not M_key_down) {
            M_key_down = true;
            show_manual = !show_manual;
            press_mn->active = !show_manual;

            int man_lin = MANUAL_LINES - 1 * int(not view_3D_mode);
            for (int i = 0; i < man_lin; ++i) {
                manual[i]->active = show_manual;
            }
        }

        if(Render::IsKeyPressed(GLFW_KEY_1)) { lod = 0; auto_lod = false; lod_text->SetText("LOD: 1"); }
        if(Render::IsKeyPressed(GLFW_KEY_2)) { lod = 1; auto_lod = false; lod_text->SetText("LOD: 2"); }
        if(Render::IsKeyPressed(GLFW_KEY_3)) { lod = 2; auto_lod = false; lod_text->SetText("LOD: 3"); }
        if(Render::IsKeyPressed(GLFW_KEY_4)) { lod = 3; auto_lod = false; lod_text->SetText("LOD: 4"); }
        if(Render::IsKeyPressed(GLFW_KEY_5)) { lod = 4; auto_lod = false; lod_text->SetText("LOD: 5"); }
        if(Render::IsKeyPressed(GLFW_KEY_6)) { lod = 5; auto_lod = false; lod_text->SetText("LOD: 6"); }
        if(Render::IsKeyPressed(GLFW_KEY_0)) { auto_lod = true; lod_text->SetText("LOD: "+ std::to_string(lod+1)+"(Auto)"); }

        if( not Render::IsKeyPressed(GLFW_KEY_H)) H_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_H)  == GLFW_PRESS && not H_key_down) {
            H_key_down = true;
            Grid::decActualGrid();

        }

        if( not Render::IsKeyPressed(GLFW_KEY_G)) G_key_down = false;
        if( Render::IsKeyPressed(GLFW_KEY_G)  == GLFW_PRESS && not G_key_down) {
            G_key_down = true;
            Grid::incActualGrid();

        }

        Render::PrepareToDrawFrame();
        Render::FirstPart(actualCamera, xTransformScale, terrain_scale_multipler);
            unsigned int triangles = 0;
            glm::vec3 cpos = actualCamera->getPosition();
            float cstep = actualCamera->getStep();

            for(auto tile : tiles){
                if( not view_3D_mode )
                {
                    if( not intersect(cpos.x - cstep* Render::getWindowRatio(), cpos.y - cstep, cpos.x + cstep*Render::getWindowRatio(), cpos.y + cstep,
                                  tile->longitude*xTransformScale, tile->latitude-1.0f, (tile->longitude+1.0f)*xTransformScale, tile->latitude)) continue;
                }
                triangles += tile->Render(lod);
            }
            triangles_text->SetText("Triangles: " + std::to_string(triangles));
        Render::DrawFrame();

    }while( not ( Render::IsKeyPressed(GLFW_KEY_ESCAPE) || Render::IsWindowClosed() ) );

    Render::CleanUp();

    return 0;
}

bool extractCords(string cords, int& lat, int& lon) {
    if( cords.length()!=7 ) {
        return false;
    }

    int latitude = std::stoi(cords.substr(1,2));
    int longitude = std::stoi(cords.substr(4,3));

    if( cords[0] == 'S' || cords[0] == 's' ) {
        latitude *= -1;
    }
    else if( not( cords[0] == 'N' || cords[0] == 'n') ) {
        return false;
    }

    if( cords[3] == 'W' || cords[3] == 'w' ) {
        longitude *= -1;
    }
    else if( not( cords[3] == 'E' || cords[3] == 'e') ) {
        return false;
    }

    lat = latitude;
    lon = longitude;
    
    return true;
}

void man()
{
    cout << "\033[1;36mWorldMap\033[0m - program to visualization data from hgt files" << endl;
    cout << BOLD(Usage:) << endl;
    cout << "WorldMap [-h HEIGHT -w WIDTH -q] HGT-FILES | --help" << endl;
    cout << endl;
    cout << BOLD(Arguments:) << endl;
    std::cout << "  HGT-FILES - list of .hgt files to be loaded." << std::endl;
    std::cout << "  "<<BOLD(-h)<<" HEIGHT - set window height. Default: 768." << std::endl;
    std::cout << "      HEIGHT - must be bigger then 400 and smaller then 1080." << std::endl;
    std::cout << "  "<<BOLD(-w)<<" WIDTH - set window height. Default: 1024." << std::endl;
    std::cout << "      WIDTH - must be bigger then 400 and smaller then 1920." << std::endl;
    std::cout << "  "<<BOLD(-q)<<" quiet mode - don't write info on console." << std::endl;
    std::cout << "      This doesn't apply for error messages." << std::endl;
    cout << endl;
}

glm::vec4 findCenter(){
    float xmax = -1000.0f, xmin = 1000.0f, ymax = -1000.0f, ymin = 1000.0f;
    for(auto t : tiles){
        xmin = std::min(xmin, (float)t->longitude+0.5f);
        xmax = std::max(xmax, (float)t->longitude+0.5f);
        ymin = std::min(ymin, (float)t->latitude-0.5f);
        ymax = std::max(ymax, (float)t->latitude-0.5f);
    }
    float xc = (xmin+xmax)/2;
    float yc = (ymin+ymax)/2;
    float xscale = (float)cos(DEG2RAD(yc));
    float spread = std::max( xscale*(xmax-xmin), ymax-ymin );
    return glm::vec4( xc, yc, (spread+1)/2.0, xscale );
}

void ScrollCallback(double x){
    if(not view_3D_mode) {
        float new_step = glm::clamp(float(actualCamera->getStep() * pow(0.75, x)), 0.015625f, 64.0f); // 1/64 = 0.015625
        actualCamera->setStep(new_step);

        ++last_lod_change; // make lod change faster
    }
    else{
        actualCamera->moveIn(float(x)*(actualCamera->getPosition().z-1.0f)*0.2f);
    }
}

void AutoLODTooHigh(int &lod){
    if(lod == 5) return;
    lod++;
    last_lod_change = 0;
}
void AutoLODTooLow(int &lod){
    if(lod == 0) return;
    lod--;
    last_lod_change = 0;
}
