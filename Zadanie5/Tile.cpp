#include <iostream>
#include <cstdlib>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>

#include "Tile.h"
#include "Shader.h"

extern BasicShader* global_shader;

#define RESTART_INDEX ((unsigned int)-1)
#define LOD_COUNT 6

// The defincion of tile resolutions for each level of detail
int LOD_points[] = {1201, 600, 400, 200, 120, 60};
GLuint Tile::indicesbuffer[6];
GLuint Tile::positionsbuffer;

void Tile::Init(){
    std::vector<unsigned int> positions(1201*1202);
    for(int y = 0; y < 1201; y++)
        for(int x = 0; x < 1201; x++)
            positions[XY2N(x,y)] = (unsigned int) (x<<16) | y; // encode x, y pos - to save memory
                // (but we have to decode it on graphic card for every vertex of every tiles in every frame) -> memory usage ~ speed

    glGenBuffers(1, &positionsbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, positionsbuffer);
    glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(unsigned int), positions.data(), GL_STATIC_DRAW);

    glGenBuffers(LOD_COUNT, indicesbuffer);

    for(int i = 0; i < LOD_COUNT; i++){
        int n = LOD_points[i];
        std::vector<unsigned int> indices;
        indices.reserve((unsigned long)n*(n+1)*2);

        for(int y = 0; y < n; y++){
            for(int x = 0; x < n; x++){
                indices.push_back((unsigned int)XY2Ndivided(x, y,   n));
                indices.push_back((unsigned int)XY2Ndivided(x, y+1, n));
            }
            indices.push_back(RESTART_INDEX);
            indices.push_back(RESTART_INDEX);
        }

        // send data to graphic card - it is similar for every tile so we can do it only one
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesbuffer[i]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);
    }
}

std::shared_ptr<Tile> Tile::Create(std::string path, int latitude_, int longitude_, bool quiet_mode) {
    std::ifstream file(path, std::ios::in|std::ios::binary);
    if( not file.is_open() ) {
        return nullptr;
    }

    std::shared_ptr<Tile> tile = std::shared_ptr<Tile>(new Tile());

    if(not quiet_mode) std::cout << "Loading data for tile " << latitude_ << " " << longitude_ << std::endl;
    tile->LoadFromFile(file, latitude_, longitude_);

    file.close();

    return tile;
}

void Tile::LoadFromFile(std::ifstream& file, int latitude_, int longitude_){

    latitude  = latitude_;
    longitude = longitude_;


    for(int y = 0; y < 1201; ++y){
        for(int x = 0; x < 1201; ++x){
            char buff[2];
            file.read(buff,2);

            short a = buff[0];
            short b = (short)(buff[1] & 0x00ff);
            short result = (a << 8) | b;

            data[XY2N(x,y)] = result;
        }
    }

//    for(int y = 0; y < 1201; y+=100){
//        for(int x = 0; x < 1201; x+=100){
//            if(data[XY2N(x,y)] > 999)
//                std::cout << data[XY2N(x,y)] << " ";
//            else if(data[XY2N(x,y)] > 99)
//                std::cout << data[XY2N(x,y)] << "  ";
//            else
//                std::cout << data[XY2N(x,y)] << "   ";
//        }
//        std::cout << std::endl;
//    }
}

void Tile::SendData2GraphicCard(){
    glGenBuffers(1, &databuffer);
    glBindBuffer(GL_ARRAY_BUFFER, databuffer);
    glBufferData(GL_ARRAY_BUFFER, 1201*1202 * sizeof(short), data.data(), GL_STATIC_DRAW);
}


unsigned int Tile::Render(int lod) {
    global_shader->set_tile_pos(longitude, latitude);

    glBindBuffer(GL_ARRAY_BUFFER, positionsbuffer);
    glVertexAttribIPointer( 0, 1, GL_UNSIGNED_INT, 0, (void*)0 );

    glBindBuffer(GL_ARRAY_BUFFER, databuffer);
    glVertexAttribIPointer( 1, 1, GL_SHORT, 0, (void*)0 );

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesbuffer[lod]);

    // "Primitive restart functionality allows us to tell OpenGL that a particular index value means, not to source a vertex at that index,
    // but to begin a new Primitive of the same type with the next vertex"
    // more on: https://www.khronos.org/opengl/wiki/Vertex_Rendering
    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(RESTART_INDEX);

    glDrawElements(GL_TRIANGLE_STRIP, LOD_points[lod]*LOD_points[lod]*2, GL_UNSIGNED_INT, (void*)0 );

    return LOD_points[lod]*LOD_points[lod]*2;
}

int Tile::XY2N(int x, int y) {
    return 1202*y+x;
}

void Tile::N2XY(int n, int& x, int& y) {
    x = n % 1202;
    y = n / 1202;
}


int Tile::XY2Ndivided(int x, int y, float no) {
    return 1202 * int(1200*y/(no-1)) + int(1200*x/(no-1));
}
