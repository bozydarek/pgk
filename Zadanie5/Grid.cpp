#include "Grid.h"

#include <vector>

unsigned long Grid::size[GRIDS];
std::shared_ptr<GridShader> Grid::shader;
GLuint Grid::gridsbuffer[GRIDS];
int Grid::actualGrid;

int Grid_densities[] = {1, 2, 5};

void Grid::Init(int width, int height) {

    shader = std::make_shared<GridShader>("gridVertex.glsl", "gridFragment.glsl");

    actualGrid = 0;

    glGenBuffers(GRIDS, gridsbuffer);
    for (int i = 0; i < GRIDS; ++i) {

        int density = Grid_densities[i];

        std::vector<float> data;
        // horizontal lines
        for (int y = -height; y <= height; y += density)
            for (int x = -width; x < width; x += density) {
                data.push_back(x);
                data.push_back(y);
                data.push_back(x + density);
                data.push_back(y);
            }
        // vertical lines
        for (int x = -width; x <= width; x += density)
            for (int y = -height; y < height; y += density) {
                data.push_back(x);
                data.push_back(y);
                data.push_back(x);
                data.push_back(y + density);
            }

        glBindBuffer(GL_ARRAY_BUFFER, gridsbuffer[i]);
        glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), data.data(), GL_STATIC_DRAW);
        size[i] = data.size() * 2;
    }
}

void Grid::Render(bool is3D, float xscale, glm::mat4 view_trans) {

    shader->use();
    shader->set_is_sphere_viev(is3D);
    shader->set_xscale(xscale);
    shader->set_view_transform(view_trans);

    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, gridsbuffer[actualGrid]);
    glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0 );
    glDrawArrays(GL_LINES, 0, size[actualGrid]);

    glDisableVertexAttribArray(0);
}

void Grid::incActualGrid() {
    if(actualGrid == GRIDS - 1) return;
    ++actualGrid;
}

void Grid::decActualGrid() {
    if(actualGrid == 0) return;
    --actualGrid;
}
