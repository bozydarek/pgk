#include <glm/geometric.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "Camera.h"

Camera::Camera(glm::vec3 pos):
    position(pos)
{
    lookAtMatrix = glm::lookAt(glm::vec3(0.0), glm::vec3(0.0,1.0,0.0), glm::vec3(0,0,1));
}

const glm::vec3 &Camera::getPosition() const {
    return position;
}

void Camera::setPosition(const glm::vec3 &position) {
    Camera::position = position;
}

static glm::vec3 transformcoords(glm::vec3 pos){
    float q = (float)cos(DEG2RAD(pos.y));
    return glm::vec3( sin(DEG2RAD(pos.x))*q, -cos(DEG2RAD(pos.x))*q, sin(DEG2RAD(pos.y)) ) * pos.z;
}

glm::mat4 Camera::getTransform() const {

    glm::vec3 p = position;
    glm::quat pitch_quat = glm::angleAxis(pitch, glm::vec3(0.0f,0.0f,1.0f));
    glm::quat yaw_quat = glm::angleAxis( yaw, glm::rotate( pitch_quat, glm::vec3(1.0f,0.0f,0.0f) ) );

    if(is3D)
    {
        p = transformcoords(position);
        pitch_quat = glm::angleAxis(float(pitch + DEG2RAD(glm::mod(position.x, 360.0f*2.0f))), glm::vec3(0.0f,0.0f,1.0f) );
        yaw_quat = glm::angleAxis( yaw, glm::rotate( pitch_quat, glm::vec3(1.0f,0.0f,0.0f) ) );

        //glm::rotation -> https://glm.g-truc.net/0.9.8/api/a00220.html#gac4856d356c5c97cec74e9b672ea89240
        auto rotation = glm::rotation(glm::vec3(0.0,0.0,-1.0), glm::normalize(-transformcoords(position)));
        return  glm::translate(glm::mat4(1.0), p) * glm::toMat4(rotation * yaw_quat * pitch_quat);
    }

    return  glm::translate(glm::mat4(1.0), p) * glm::toMat4(yaw_quat * pitch_quat);
}

glm::mat4 Camera::getViewMatrix()
{
    return lookAtMatrix * glm::inverse(getTransform());
}

void Camera::moveNorth(float time){
    position += glm::vec3(0.0,1.0,0.0)*time;
}
void Camera::moveSouth(float time){
    position -= glm::vec3(0.0,1.0,0.0)*time;
}
void Camera::moveWest(float time){
    position += glm::vec3(1.0,0.0,0.0)*time;
}
void Camera::moveEast(float time){
    position -= glm::vec3(1.0,0.0,0.0)*time;
}

void Camera::moveIn(float time) {
    position -= glm::vec3(0.0,0.0,1.0)*time;
    position.z = glm::clamp(position.z, 1.0005f, 6.00f);
}

void Camera::moveForward(float time){
    glm::vec3 pitched_front = glm::rotate(glm::vec3(0.0,1.0,0.0), pitch, glm::vec3(0.0,0.0,1.0));
    float scale = (float)cos(DEG2RAD(position.y));
    pitched_front.x /= scale;
    position += pitched_front*time;
}
void Camera::moveBackward(float time){
    glm::vec3 pitched_front = glm::rotate(glm::vec3(0.0,1.0,0.0), pitch, glm::vec3(0.0,0.0,1.0));
    float scale = (float)cos(DEG2RAD(position.y));
    pitched_front.x /= scale;
    position -= pitched_front*time;
}
void Camera::moveRight(float time){
    glm::vec3 pitched_left = glm::rotate(glm::vec3(1.0,0.0,0.0), pitch, glm::vec3(0.0,0.0,1.0));
    float scale = (float)cos(DEG2RAD(position.y));
    pitched_left.x /= scale;
    position -= pitched_left*time;
}
void Camera::moveLeft(float time){
    glm::vec3 pitched_left = glm::rotate(glm::vec3(1.0,0.0,0.0), pitch, glm::vec3(0.0,0.0,1.0));
    float scale = (float)cos(DEG2RAD(position.y));
    pitched_left.x /= scale;
    position += pitched_left*time;
}

float Camera::getSpeed() const {
    return speed;
}

void Camera::setSpeed(float speed) {
    Camera::speed = speed;
}

float Camera::getStep() const {
    return step;
}

void Camera::setStep(float step) {
    Camera::step = step;
}

void Camera::setIs3D(bool is3D) {
    Camera::is3D = is3D;
}

void Camera::rotatePitch(float delta) {
    pitch += delta;
}

void Camera::rotateYaw(float delta) {
    yaw = glm::clamp(yaw + delta, -PI/2.0f, 0.0f);
}

bool Camera::is3Dcam() const {
    return is3D;
}

glm::mat4 Camera::getPerspectiveTransform(float ratio) {
    if(is3D)
    {
        return glm::perspective(getFOV(), ratio, 0.0005f, 6.0f);
    }

    float s = getStep();
    return glm::ortho(-s* ratio, s*ratio, -s, s, 0.1f, 20.0f);
}

