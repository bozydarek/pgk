#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;

#include <GL/glew.h>

#include "Shader.h"

BasicShader *global_shader;

bool Shader::load(const char * vertex_file_path,const char * fragment_file_path){

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if(VertexShaderStream.is_open()){
		std::string Line = "";
		while(getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}else{
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if(FragmentShaderStream.is_open()){
		std::string Line = "";
		while(getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;


	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}



	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}



	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	
	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);
	
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	shader_id = ProgramID;

	return true;
}

int static uniformCheck(GLint uniform, std::string name) {

    if (uniform == -1)
	{
		std::cerr << "A " << name << " uniform is missing from the shader." << std::endl;
		return 1;
	}

	return 0;
}

Shader::Shader(const char *vertex_file_path, const char *fragment_file_path) {
	if (not load(vertex_file_path, fragment_file_path)) {
		exit(1);
	}
}

void Shader::use()
{
    glUseProgram(shader_id);
}

Shader::~Shader() {
    glDeleteProgram(shader_id);
}

BasicShader::BasicShader(const char *vertex_file_path, const char *fragment_file_path)
	: Shader(vertex_file_path, fragment_file_path)
{
    tile_pos    = glGetUniformLocation(shader_id, "pos");
    xscale      = glGetUniformLocation(shader_id, "xscale");
    view_trans  = glGetUniformLocation(shader_id, "view_transform");
    sphere      = glGetUniformLocation(shader_id, "sphere");
    terrainscale= glGetUniformLocation(shader_id, "terrainscale");

    if( uniformCheck(tile_pos, "Tile position (pos)")
        || uniformCheck(xscale, "xscale")
        || uniformCheck(view_trans, "view_transform")
        || uniformCheck(sphere, "sphere")
        || uniformCheck(terrainscale, "terrainscale")
        ) {
            exit(1);
    }

}

void BasicShader::set_tile_pos(int lon, int lat) {
	glUniform2f(tile_pos, (float)lon, (float)lat);

}

void BasicShader::set_xscale(float value) {
    glUniform1f(xscale, value);
}

void BasicShader::set_view_transform(const glm::mat4 &value) {
    glUniformMatrix4fv(view_trans, 1, GL_FALSE, &value[0][0]);
}



void BasicShader::set_terrainscale(int value) {
    glUniform1i(terrainscale, value);
}

void BasicShader::set_is_sphere_viev(bool value) {
    glUniform1i(sphere, value);

}

//
//void BasicShader::set_model_view_projection(const GLfloat *value) {
//    glUniformMatrix4fv(model_view_projection, 1, GL_FALSE, value);
//}


FontShader::FontShader(const char *vertex_file_path, const char *fragment_file_path)
        : Shader(vertex_file_path, fragment_file_path)
{
    uniform_tex = glGetUniformLocation(shader_id,"tex");
    uniform_color = glGetUniformLocation(shader_id,"color");

    if( uniformCheck(uniform_tex, "tex")
        || uniformCheck(uniform_color, "textcolor") )
    {
        exit(1);
    }
}

void FontShader::set_font_tex(int value)
{
    glUniform1i(uniform_tex, value);
}

void FontShader::set_font_color(const GLfloat *value)
{
    glUniform4fv(uniform_color, 1, value);
}

GridShader::GridShader(const char *vertex_file_path, const char *fragment_file_path)
    : Shader(vertex_file_path, fragment_file_path)
{
    xscale      = glGetUniformLocation(shader_id, "xscale");
    view_trans  = glGetUniformLocation(shader_id, "view_transform");
    sphere      = glGetUniformLocation(shader_id, "sphere");

    if( uniformCheck(xscale, "xscale")
        || uniformCheck(view_trans, "view_transform")
        || uniformCheck(sphere, "sphere") ) {
        exit(1);
    }
}

void GridShader::set_is_sphere_viev(bool value) {
	glUniform1i(sphere, value);
}

void GridShader::set_xscale(float value) {
	glUniform1f(xscale, value);
}

void GridShader::set_view_transform(const glm::mat4 &value) {
	glUniformMatrix4fv(view_trans, 1, GL_FALSE, &value[0][0]);
}
